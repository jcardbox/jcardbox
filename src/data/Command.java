package data;

public interface Command {
	void execute();
}
