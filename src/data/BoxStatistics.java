package data;

/** contains data that is shown in the statistics window */
public class BoxStatistics {
    public int[] cardsInBox;     // number of cards in each box
    public int [][] cardsForDay; // number of cards in each box which will be questioned in 0..n days
    public int finishedCards;    // number of cards that have answerFinished the last box
    public int currentCategory;  // number of cards in the current category
}
