package data;

import org.h2.tools.Csv;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.StringJoiner;

/**
 * Created by anke on 8/9/15.
 */
public class H2Database extends SQLDatabase
{
    static {
        try {
            Class.forName("org.h2.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected String getReplaceStatement() {return "MERGE";}

    protected void loadDump(boolean withoutBlobs) {
        for (Language lang : Globals.getSelectedLanguages()) {
            Globals.setLanguage(lang);

            deleteTables();
            createTables();

            HashMap<String, String> tables = withoutBlobs ? basicTables : allTables;
            for (String tablename : tables.keySet()) {
                String filename = dumpDir + File.separator + tablename + ".csv";
                String columns = tables.get(tablename);
                fillTableFromCSV(tablename, columns, filename);
            }
        }
    }

    public static long readChangeDateFromDump( String dumpDir ) {
        long timestamp = 0;
        try {
            H2Database localTmpDB = (H2Database) SQLDatabase.connect("jdbc:h2:mem:", "", "");
            localTmpDB.setDumpDirectory(dumpDir);

            String tableName = SQLDatabase.PREF_TABLE;
            String filename = dumpDir + File.separator + tableName + ".csv";
            localTmpDB.fillTableFromCSV(tableName, basicTables.get(tableName), filename);
            timestamp = Long.parseLong(localTmpDB.getPrefs("lastDumpTimestamp"));
            localTmpDB.closeConnection();
        } catch (Exception e) {
            System.err.println("Warning: readChangeDateFromDump: previous entry doesn't exist.");
        }
        return timestamp;
    }

    /**
     * save all basicTables to csv. Option to disable saving of blobs is added to avoid larger network traffic.
     * Incremental backup can be used instead.
     * @param withoutBlobs if true, basicTables with blobs (images/sounds) are not saved
     */
    protected void saveDump( boolean withoutBlobs ) {
        ArrayList<String> filenames= new ArrayList<>();
        String tempDir = dumpDir + File.separator + "tmp" + File.separator;
        try {
            Files.createDirectories( Paths.get( tempDir ) );
            //tmpDirName = Files.createTempDirectory("JCardBox_").toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (Language lang : Globals.getSelectedLanguages()) {
            Globals.setLanguage(lang);

            long time = new java.util.Date().getTime();
            setPrefs("lastDumpTimestamp",""+time);
            HashMap<String, String> tables = withoutBlobs ? basicTables : allTables;
            for (String tablename : tables.keySet()) {
                String filename = tempDir + tablename + ".csv";
                saveTableToCSV(tablename, filename);
                filenames.add(filename);
            }
        }
        createZip( filenames, dumpDir + File.separator + "jcardboxDump_"+getDateString()+".zip");

        try {
            File dir = new File(tempDir);
            for (File file : dir.listFiles()) {
                file.delete();
            }
            dir.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getDateString() {
        java.util.Calendar now = java.util.Calendar.getInstance();
        String str = String.format("%d_%02d_%02d", now.get(Calendar.YEAR), now.get(Calendar.MONTH)+1, now.get(Calendar.DAY_OF_MONTH));
        return str;
    }

    private void saveTableToCSV(String table, String csvFileName) {
        File createFile = new File( csvFileName );
        try {
            createFile.createNewFile();
        } catch (IOException e) { System.out.println("Warning saving csv: "+csvFileName+" "+e);}

        try {
            Statement stat = con.createStatement();
            String query = "SELECT * FROM "+table;
            ResultSet rs = stat.executeQuery(query);
            Csv csv = new Csv();
            csv.setWriteColumnHeader(false);
            csv.write(csvFileName, rs, "UTF8");

            ResultSet rs2 = stat.executeQuery(query);
            // while (rs2.next()) System.out.println("table--3----: "+table+" "+rs2.getString(1)+" "+rs2.getString(2));

            stat.close();
        } catch ( SQLException e) {
            e.printStackTrace();
        }
    }

    private void fillTableFromCSV(String table, String columns, String csvFileName) {
        this.deleteTableContents( table );
        try {
            // quick and dirty hack to get column names
            String [] split = columns.split(",");
            StringJoiner joiner = new StringJoiner(",");
            for (int i=0; i<split.length; i++) {
                String col = split[i].trim().split(" ")[0];
                if (col.startsWith("primary")) break;
                joiner.add(col);
            }
            String columnstr = joiner.toString(); // all table columns, separated by comma

            long bytes = new File(csvFileName).length();
            if ( bytes > 0 ) {
                String cmd = "insert into " + table + "(" + columnstr + ") SELECT * FROM CSVREAD('%s', '%s', 'charset=UTF-8');";
                cmd = String.format(cmd, csvFileName, columnstr);
                //System.out.println("st: "+cmd);
                Statement stat = con.createStatement();
                stat.execute(cmd);
                stat.close();
            }
        } catch ( SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * writes the lesson with the given name to the file csvFileName
     * @see this#loadCSV
     */
    public static void saveLessonToCSV(Lesson lesson, String filename) {
        try {
            lesson.load( true );

            H2Database localTmpDB = (H2Database) SQLDatabase.connect("jdbc:h2:mem:", "", "");
            localTmpDB.createTables();
            localTmpDB.addLesson( lesson );
            Statement stat = localTmpDB.con.createStatement();

            String basename = filename.replaceAll("\\.lesson","");
            basename = basename.replaceAll("\\.csv","");
            String csvFileName = basename+".csv";
            String suffix = Globals.getLanguageSuffix();

            String files[] = {csvFileName, basename+"_audio.csv", basename+"_image.csv"};
            ArrayList<String> filenames = new ArrayList<>();
            for (String f:files) filenames.add(f);

            String query = "SELECT %s FROM "+SQLDatabase.CARDS_TABLE;
            ResultSet rs = stat.executeQuery(String.format(query, CARD_COLUMNS));
            new Csv().write(files[0], rs, "UTF8");

            String c_card = "card_id"+suffix;
            String c_cardCard = SQLDatabase.CARDS_TABLE+".card_id"+suffix;
            String c_audio = "audio"+suffix;
            String c_image = "image"+suffix;

            query = "SELECT "+c_cardCard+","+c_audio+" FROM "+SQLDatabase.AUDIO_TABLE+", "+SQLDatabase.CARDS_TABLE+
                    " WHERE "+c_cardCard+"="+SQLDatabase.AUDIO_TABLE+"."+c_card;
            query = "SELECT * from "+SQLDatabase.AUDIO_TABLE;
            rs = stat.executeQuery(query);
            new Csv().write(files[1], rs, "UTF8");

            query = "SELECT "+c_cardCard+","+c_image+" FROM "+SQLDatabase.IMAGE_TABLE+", "+SQLDatabase.CARDS_TABLE+
                    " WHERE "+c_cardCard+"="+SQLDatabase.IMAGE_TABLE+"."+c_card;
            rs = stat.executeQuery(query);
            new Csv().write(files[2], rs, "UTF8");

            stat.close();
            localTmpDB.closeConnection();

            createZip( filenames, basename+".lesson");

            for (String f:files) {
                new File(f).delete();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static ArrayList<Card> importLessonFromCSV(String filename) {
        ArrayList<Card> list = null;
        if (filename.endsWith(".lesson")) {
            try {
                String dir = Files.createTempDirectory("JCardBox_").toString();
                extractZip(filename, dir);
                File file = new File(filename);
                String basename = dir + File.separator + file.getName();
                basename = basename.replaceAll("\\.lesson", "");

                list = loadCSV(basename);
                String[] files = new File(dir).list();
                for ( String tmpFile : files ) {
                    new File(dir+File.separator+tmpFile).delete();
                }
                new File(dir).delete();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            list = loadCSV(filename);
        }
        return list;
    }

    /**
     * loads a lesson from CSV (comma separated values) file with the given file name.
     * @see this#saveLessonToCSV
     * @return a list of all imported cards
     */
    private static ArrayList<Card> loadCSV(String filename) {
        String basename = filename.replaceAll(".csv","");

        try {
            if (filename.endsWith(".lesson")) {
                String dir = Files.createTempDirectory("JCardBox_").toString();
                extractZip(filename, dir);
                File file = new File(filename);
                basename = dir + File.separator + file.getName();
                basename = basename.replaceAll("\\.lesson","");
            }
        } catch (IOException e ) {
            e.printStackTrace();
        }

        ArrayList<Card> cards = new ArrayList<>();
        try {
            // probs with blob ResultSet rs = new Csv().read(filename, null, null);

            // read card data
            String query = String.format("SELECT * FROM CSVREAD('%s', null, 'charset=UTF-8')", basename+".csv");
            H2Database localTmpDB = (H2Database) SQLDatabase.connect("jdbc:h2:mem:", "", "");
            Statement stat = localTmpDB.con.createStatement();
            ResultSet rs = stat.executeQuery(query);
            while (rs.next()) {
                Card card = new Card();
                card.setDatabaseID(rs.getInt(1));
                card.setFront(rs.getString(2));
                card.setBack(rs.getString(3));
                //System.err.println("card: "+card.getFront()+":"+card.getBack());
                cards.add(card);
                //System.err.println("H2Database::read card: " + card.getQuestion() + ":" + card.getAnswer());
            }
            stat.close();

            // read images
            query = String.format("SELECT card_id, image FROM CSVREAD('%s', null, 'charset=UTF-8')", basename + "_image.csv");
            stat = localTmpDB.con.createStatement();
            rs = stat.executeQuery(query);
            while (rs.next()) {
                int cardID = rs.getInt(1);
                Blob blob = rs.getBlob(2);
                if (blob != null) {
                    try {
                        InputStream in = blob.getBinaryStream();
                        BufferedImage bufImg = ImageIO.read(in);
                        in.close();
                        for (Card card : cards) {
                            if (card.getDatabaseID() == cardID) {
                                card.setBufferedImage(bufImg);
                                break;
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } // read images

            // read audio
            query = String.format("SELECT card_id, audio FROM CSVREAD('%s', null, 'charset=UTF-8')", basename + "_audio.csv");
            stat = localTmpDB.con.createStatement();
            rs = stat.executeQuery(query);
            while (rs.next()) {
                int cardID = rs.getInt(1);
                byte[] bytes = rs.getBytes(2);
                for (Card card : cards) {
                    if (card.getDatabaseID() == cardID) {
                        card.setAudio( bytes );
                        break;
                    }
                }
            }

            stat.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cards;
    }
}
