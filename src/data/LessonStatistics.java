package data;

/** contains data that is shown in the statistics window */
public class LessonStatistics {
    public int lessonID;
    public String lessonName;
    public int countInBox[]; // last box is boxNr -1 (=answerFinished)
}
