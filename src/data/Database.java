package data;

import gui.dialogs.DefaultDialogs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.util.Properties;

/**
 gives access to the instance of the currently used database
 */
public class Database {
	private static SQLDatabase db;
	private static ConnectionData connectionData = null;

	public static void connectToDatabase() throws Exception {
		ConnectionData data = new Database.ConnectionData();
		data.readIniFile();
		if (data.internalDatabase) // connect to local database
		{
			closeDatabaseConnection();
			String DATABASE = File.separator + "database" + File.separator + "CardBoxDB";
			String localURL = "jdbc:h2:"+ Globals.getInstallationRoot() + DATABASE; // + ".mv.db";
			connect(localURL, "", "");
		}
		else
		{ // connect to mysql database
			boolean sameConnection = connectionData != null &&
					connectionData.databaseURL.equals(data.databaseURL) &&
					connectionData.databaseUser.equals(data.databaseUser) &&
					connectionData.databasePass.equals(data.databasePass);
			if ( !sameConnection ) {
				closeDatabaseConnection();
				connect(data.databaseURL, data.databaseUser, data.databasePass);
				connectionData = data;
			}
		}
	}

	private static boolean connect(String url, String userid, String password) throws SQLException {
		//System.out.println("Database::connect to "+url+" as user \""+userid+"\" with password \""+password);
		closeDatabaseConnection();
		db = SQLDatabase.connect(url, userid, password);
		System.out.println("db connected");
		db.setDumpDirectory( Globals.getDumpDir() );
		return true;
	}

	public static void closeDatabaseConnection() {
		if (db != null) db.closeConnection();
		db = null;
		connectionData = null;
	}

	public static SQLDatabase getDatabase() {
		if ( db == null )
		{
			Thread.dumpStack();
			DefaultDialogs.showErrorDialog("Fehler","Keine Verbindung zur Datenbank");
		}
		return db;
	}

	public static boolean isValid() {
		return db != null;
	}

	public static class ConnectionData {
		private boolean internalDatabase = true;
		private String databaseURL="";
		private String databaseUser="";
		private String databasePass="";

		public boolean isInternalDatabase() {
			return internalDatabase;
		}

		public void setInternalDatabase(boolean internalDatabase) {
			this.internalDatabase = internalDatabase;
		}

		public String getDatabaseURL() {
			return databaseURL;
		}

		public void setDatabaseURL(String databaseURL) {
			this.databaseURL = databaseURL;
		}

		public String getDatabaseUser() {
			return databaseUser;
		}

		public void setDatabaseUser(String databaseUser) {
			this.databaseUser = databaseUser;
		}

		public String getDatabasePass() {
			return databasePass;
		}

		public void setDatabasePass(String databasePass) {
			this.databasePass = databasePass;
		}

		public void saveIniFile() {
			try {
				String inifile = Globals.getInstallationRoot() + File.separator + "settings.ini";
				FileOutputStream out = new FileOutputStream(inifile);
				Properties p = new Properties();
				p.setProperty("useInternalDatabase", "" + internalDatabase );
				p.setProperty("databaseURL", "" + databaseURL );
				p.setProperty("databaseUser", "" + databaseUser );
				p.setProperty("databasePass", "" + databasePass );

				p.store(out, "");
				out.close();
			} catch (Exception e) {e.printStackTrace();}
		}

		public void readIniFile() {
			String inifile = Globals.getInstallationRoot() + File.separator + "settings.ini";

			if ( ! (new File(inifile)).exists() ) // historical reasons
			{
				String languageDir = Globals.getInstallationRoot() + File.separator + Globals.getLanguageIdentifier();
				String oldIni = languageDir + File.separator + "settings.ini";
				if ( new File(oldIni).exists() )
					inifile = oldIni;
			}

			try {
				Properties p = new Properties();
				FileInputStream in = new FileInputStream(inifile);
				p.load(in);
				in.close();

				internalDatabase = Boolean.parseBoolean(p.getProperty("useInternalDatabase", "true"));
				databaseURL = p.getProperty("databaseURL", "");
				databaseUser = p.getProperty("databaseUser", "");
				databasePass = p.getProperty("databasePass", "");

			} catch (Exception e) { System.out.println(e);}
		}
	}

}