package data;

import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.util.HashMap;

/**
 * Created by anke on 9/6/16.
 */
public class LanguageEnglish extends Language
{
    private boolean prependTo;
    public static final String tenses[] = {"simple past","past participle","past"};

    @Override
    public String getLabel() {
        return "Englisch";
    }

    @Override
    public String getIdentifier() {
        return "english";
    }

    @Override
    public String getDatabaseSuffix() {
        return "";
    }

    /**
     has to be called after database has been opened
     */
    public void loadDatabasePreferences() {
        prependTo = Globals.getBooleanPref( "prependTo", true);
    }

    public boolean answerIsCorrect(String userAnswer, String correctAnswer) {

        // ignore blanks next to dot and comma
        userAnswer =    userAnswer.replaceAll("\\s*\\.\\s*",".");
        correctAnswer = correctAnswer.replaceAll("\\s*\\.\\s*",".");
        userAnswer =    userAnswer.replaceAll("\\s*,\\s*",",");
        correctAnswer = correctAnswer.replaceAll("\\s*,\\s*",",");

        if (userAnswer.equals(correctAnswer)) {
            return true;
        }

        if (prependTo && (("to " + userAnswer).equals(correctAnswer) || userAnswer.equals("to " + correctAnswer)) ) {
            return true;
        }

        /* check if answer is correct if long form is replaced by short form or vice versa */
        String replaceCombinations[][] = {
                {"'re", " are"},
                {"'ve", " have"},
                {"I'm", "I am"},
                {"'s", " is"},
                {"'s", " has"},
                {"n't", " not"}
        };

        for (String[] replace : replaceCombinations) {
            String variation = userAnswer.replace(replace[0], replace[1]);
            if (variation.equals(correctAnswer)) return true;
            variation = userAnswer.replace(replace[1], replace[0]);
            if (variation.equals(correctAnswer)) return true;
        }

        return false;
    }

    //public LanguageKeyListener getKeyListener() {
    //    return keyListener;
    //}
    // private LanguageKeyListener keyListener = new EnglishKeyListener();

    @Override
    public boolean isConfigurable() {
        return true;
    }

    @Override
    public void configure(java.awt.Component parent) {
        new ConfigDialog( parent ).setVisible(true);
    }

    @Override
    public void setInput(TextField textField) {
        textField.addEventFilter(KeyEvent.KEY_TYPED, new EnglishKeyListener());
    }
    class EnglishKeyListener implements EventHandler<KeyEvent> {
        @Override
        public void handle(KeyEvent ke) {
            TextField field= (TextField) ke.getSource();

            HashMap<KeyCode, String > replace = new HashMap<>();
            replace.put(KeyCode.F1, "[past]");
            replace.put(KeyCode.F2, "[simple past]");
            replace.put(KeyCode.F3, "[past participle]");

            String replaceStr = replace.get(ke.getCode());
            //todo System.out.println("replac"+replaceStr+"  "+ke.getCode()+"ke"+ke);
            if (replaceStr != null)
            {
                String text = field.getText();
                for (String str : replace.values() ) {
                    if (text.contains( str ))
                    {
                        int start = text.indexOf( str );
                        text = text.substring(0,start)+text.substring(start+str.length());
                    }
                }
                replaceStr = new String(text + " " + replaceStr).replaceAll("\\s+"," ");
                field.setText(replaceStr);
            }
        }
    }

    class ConfigDialog extends JDialog {
        private JRadioButton r1, r2;
        private JCheckBox check;
        public ConfigDialog( java.awt.Component parent )
        {
            setTitle(LanguageEnglish.this.getLabel());
            prependTo = Globals.getBooleanPref( "prependTo", true);
            r1 = new JRadioButton("\"to\" muß eingegeben werden");
            r2 = new JRadioButton("\"to\" wird automatisch eingefügt");
            r1.setSelected( !prependTo );
            r2.setSelected( prependTo );
            ButtonGroup group = new ButtonGroup();
            group.add(r1);
            group.add(r2);

            check = new JCheckBox("Abkürzungen ersetzen (z.B. I'm <=> I am)");
            check.setSelected( Globals.getBooleanPref("lang_english_shorten", true ) );

            setLayout(new MigLayout("fill, wrap1"));
            JLabel header = new JLabel("Sprachkonfiguration: "+LanguageEnglish.this.getLabel());
            java.awt.Font font = new java.awt.Font(header.getFont().getName(), java.awt.Font.BOLD, (header.getFont().getSize()+2));
            header.setFont( font );

            add(header, "wrap 10pt");
            add(new JLabel("Grundform englischer Verben (to go <=> go)"));
            add(Box.createHorizontalStrut(20), "split 2"); add(r1, "wrap 0pt");
            add(Box.createHorizontalStrut(20), "split 2"); add(r2);
            add(check);

            JButton cancel = new JButton("Abbruch");
            cancel.addActionListener(e -> setVisible(false) );
            JButton ok = new JButton("Ok");
            ok.addActionListener(e -> ok() );
            add(cancel, "split2,sizegroup bttn, align right"); //sizegroups set all members to the size of the biggest member
            add(ok, "sizegroup bttn");

            setLocationRelativeTo(parent);
            pack();
        }

        private void ok()
        {
            prependTo = r2.isSelected();
            Globals.setPref("lang_english_shorten", ""+check.isSelected());
            Globals.setPref("prependTo", ""+prependTo);
            setVisible(false);
        }
    }
}