package data;

import javafx.scene.control.TextField;

import java.awt.Color;
//import javafx.scene.paint.Color;

/**
 * Created by Anke Visser on 9/6/16.
 */


public abstract class Language {
    public enum Gender {MALE, FEMALE, NEUTRAL, UNDEFINED}

    public abstract String getLabel();

    public abstract String getIdentifier();

    public abstract String getDatabaseSuffix();

    public void loadDatabasePreferences() {
    }

    public Gender getGender(String word) {
        return Gender.UNDEFINED;
    }

    public boolean answerIsCorrect(String userAnswer, String correctAnswer) {
        return (userAnswer.equals(correctAnswer));
    }

    public boolean isConfigurable() {
        return false;
    }

    public Color getColor(Gender gender) {
        return null;
    }

    public void configure(java.awt.Component parent) {
    }
    public abstract void setInput(TextField textField);
}
