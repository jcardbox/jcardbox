package data;

/**
 * LogEntry contains the data of one log entry in the database
 */
public class LogEntry {
	public static final char SEP=';';
	public static final int LOG_CRAM=1;
	public static final int LOG_BOX=2;
	public static final int LOG_UNDO=3;
	public static final int LOG_MODIFY_BOX=4;

	private int logType;
	private java.sql.Date date;
	private String message;

	public LogEntry(int logType, String message) {
		this.date = new java.sql.Date(new java.util.Date().getTime());
		this.message = message;
		this.logType = logType;
	}
	public LogEntry(int logType, java.sql.Date date, String message) {
		this.date = date;
		this.message = message;
		this.logType = logType;
	}

	/** creates a log entry for the cram lesson
     * @param lessonName name of the loaded lesson
     * @param lessonSize number of entries in the lesson
     * @param answered number of entries, that have been checkAnswer correctly
     * @param correct number of entries, that have been checkAnswer correctly for the 1rst try
     * @param errors number of errors, duplicates included
     * @param uniqErrors number of errors, duplicates excluded
     */
	public LogEntry(String lessonName, int lessonSize, int answered, int correct, int errors, int uniqErrors) {
		date = new java.sql.Date(new java.util.Date().getTime());
		message = lessonName + SEP + lessonSize + SEP + answered + SEP + correct + SEP + errors + SEP + uniqErrors;
		logType = LogEntry.LOG_CRAM;
        System.out.println(message+"\n jmsg:"+getMessage());
	}

	/** creates a log entry for the box lesson
	 * @param startSize number of elements in the list at the beginning
     * @param listSize number of remaining elements, if lesson has been aborted
     * @param answerCount number of checkAnswer elements, including (multiple) errors
     * @param errorCount number of wrong answers
	 */
	public LogEntry(int startSize, int listSize, int answerCount[], int errorCount[]) {
		date = new java.sql.Date(new java.util.Date().getTime());
		StringBuilder log= new StringBuilder(""+startSize+SEP+listSize+SEP+answerCount.length);
		for (int answer : answerCount) log.append(SEP).append(answer);
		for (int error  : errorCount)  log.append(SEP).append(error);
		message = log.toString();
		logType = LogEntry.LOG_BOX;
	}


	public int getLogType() {
		return logType;
	}

	/** writes this log entry to database */
	public void write() {
		Database.getDatabase().writeLog( logType, getDatabaseString() );
	}

	public java.sql.Date getDate() {
		return date;
	}
	public void setDate(java.sql.Date date) {
		this.date = date;
	}
	public String getDatabaseString() {
		return message;
	}

	public boolean isBoxCramLesson() {
		if (logType == LogEntry.LOG_CRAM) {
			String[] fields = message.split("" + SEP);
			if (fields.length != 6) return false;
			String lessonName = fields[0];
			return lessonName.equals(Lesson.boxLesson.getName());
		}
		return false;
	}

	/** returns { # number of cards not yet questioned, number of uniq errors } */
	public int[] getBoxCramLessonErrors() {
		int count[] = null;
		if (logType == LogEntry.LOG_CRAM) {
			String[] fields = message.split(""+SEP);
			if (fields.length != 6) return null;
			int lessonSize = Integer.parseInt(fields[1]);
			int answered = Integer.parseInt(fields[2]);
			count = new int[2];
			count[ 0 ] = lessonSize - answered;
			count[ 1 ] = Integer.parseInt(fields[5]); // uniq errors
		}
		return count;
	}

	public String getMessage() 
	{
		String[] fields = message.split(""+SEP);
		String ret = message;
		if (logType == LogEntry.LOG_CRAM) 
		{
            if (fields.length != 6) return "";
			//lessonName + SEP + lessonSize + SEP + questionsAsked + SEP + correct + SEP + errors + SEP + uniqErrors;
			String lessonName = fields[0];
			int lessonSize = Integer.parseInt(fields[1]);
			int answered = Integer.parseInt(fields[2]);
			int firstCorrect = Integer.parseInt(fields[3]);
            int errors = Integer.parseInt(fields[4]);
			int uniqErrors = Integer.parseInt(fields[5]);

            if ( answered != lessonSize) {
                ret = lessonName +": Beantwortet wurden " + answered + " von " + lessonSize +
                        " Vokabeln, davon beim 1. Versuch korrekt: " + firstCorrect;
            }
            else {
                if (errors == 0)
                    ret = lessonName + " ("+lessonSize+" Vokabeln): Alle Vokabeln richtig eingegeben!";
                else
                    ret = lessonName + " ("+lessonSize+" Vokabeln): Beim 1. Versuch korrekt: "+firstCorrect+
							" Fehler: "+errors ;
            }
        }
        else if (logType == LogEntry.LOG_BOX)
        {
            //LogEntry logEntry = new LogEntry( startSize, list.size(), boxes, answerCount, errorCount );
            int startSize = Integer.parseInt(fields[0]);
			int questionsLeft = Integer.parseInt(fields[1]);
			int boxes = Integer.parseInt(fields[2]);

			int answered = startSize - questionsLeft;
			int errors = 0;
			int fieldNr = 3 + boxes;
			for (int box=0; box < boxes; box++) {
				errors += Integer.parseInt(fields[ fieldNr++ ]);
			}

			ret = "Vokabelkasten ( " + startSize +" Vokabeln ): Abgefragt wurden "+ answered +
			      " davon korrekt: " + (answered-errors);
		}
		else if (logType == LogEntry.LOG_UNDO)
		{
			int err = fields.length-1;
			int ok  = fields.length-2;
			ret = "Korrektur von Vokabel "+fields[0]+": eingegeben wurde \""+fields[err]+"\" korrekt ist \""+fields[ok]+"\"";
		}

		return ret;
	}

}
