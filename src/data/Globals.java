package data;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.util.*;
import java.awt.Font;

/**
 * global settings
 */
public class Globals {
    public enum UserQuestionType {Text,Choice}

    public final static String version = "1.1"; // version of this program (JCardBox)
    private static String installationRoot;
    private static String dumpDir;
    private static ArrayList<Language> supportedLanguages = new ArrayList<>();
    private static ObservableList<Language> selectedLanguages = FXCollections.observableArrayList();
    private static Language language;

    // --- following values can be set by the user at runtime and will be saved into db ---
    private static String userName = null;
    private static String languageLabel1 = "Deutsch";
    private static String languageLabel2 = null;
    private static String defaultCategory = "Klasse5";
    private static boolean useWikiMedia = true; // use wiki to search for images
    private static boolean useWiktionary = true; // use wiktionary for translations
    private static UserQuestionType cardQuestionTypeFront, cardQuestionTypeBack; // user input methods for each direction
    private static int imageWidth = 400; // convert images to this size to save in data base
    private static int imageHeight = 300;
    private static int[] boxDates = {1, 3, 7, 14, 30, 60};
    private static int[] boxDatesReverse = {1, 7, 28};

    private static Dimension imageSize = new Dimension(400,300); // todo remove awt
    public static Dimension getImageSize() {
        return imageSize;
    } //todo remove awt

    // --- local preferences ---
    private static Font font; // screen font

    // --- temporary ---
    private static Direction direction = Direction.FRONT; // direction to ask: language1->language2 is default

    static {
        String dir;
        dir = System.getProperty("dir", null);
        if (dir == null) {
            if (System.getProperty("os.name").startsWith("Windows"))
                dir = System.getenv("USERPROFILE") + "\\AppData\\Roaming";
            else
                dir = System.getProperty("user.home");
            dir += File.separator + "JCardBox";
        }
        installationRoot = dir;

        supportedLanguages.add(new LanguageEnglish());
        supportedLanguages.add(new LanguageNL());
        supportedLanguages.add(new LanguageFrench());
        for (Language lang : supportedLanguages)
            selectedLanguages.add(lang);

        // use language set by system propery "language", otherwise use default language
        String languageString = System.getProperty("language", "english");
        language = supportedLanguages.get(0);
        for (Language l : supportedLanguages ) {
            if (l.getIdentifier().equals(languageString)) {
                System.out.println("set lang "+language+" "+l);
                language = l;
                break;
            }
        }
        setLanguage( language );
        readLocalSettings();
    }

    public static String getLanguageLabel1() {return languageLabel1; }
    public static String getLanguageLabel2() {return languageLabel2; }

    public static Language getLanguage() {return language;}
    public static ArrayList<Language> getSupportedLanguages() {return supportedLanguages; }
    public static ObservableList<Language> getSelectedLanguages() {return selectedLanguages; }
    public static String getLanguageIdentifier() {
        return language.getIdentifier();
    }
    public static String getLanguageLabel() {
        return language.getLabel();
    }
    public static String getLanguageSuffix() {
        return language.getDatabaseSuffix();
    }

    // ==== methods =======================================================================================

    public static void setLanguage( Language lang ) {
        language = lang;
        languageLabel2 = language.getLabel();
        dumpDir = installationRoot + java.io.File.separator + "dump";

        System.out.println("setLang "+lang);
        if (Database.isValid()) {
            Database.getDatabase().setLanguage(language);
        }
        OnlineSearch.setLanguage( language.getIdentifier() );
        boxDates = split(getStringPref("boxDates"+language.getDatabaseSuffix(), join(boxDates)));
        boxDatesReverse = split(getStringPref("boxDatesReverse"+language.getDatabaseSuffix(), join(boxDatesReverse)));
    }

    /** mark language as available */
    public static void enableLanguage( Language lang, boolean enable ) {
        if (enable) {
            if (!selectedLanguages.contains(lang)) selectedLanguages.add(lang);
        }
        else {
            if (selectedLanguages.size() > 1)
                selectedLanguages.remove(lang);
        }
        if (Database.getDatabase() instanceof H2Database)
            saveLocalSettings();
        else
            Database.getDatabase().setPrefs("SelectedLanguages", getLanguageSettingString() );
    }
    private static String getLanguageSettingString() {
        ArrayList<String> list = new ArrayList<>();
        for ( Language l : selectedLanguages )
            list.add(l.getIdentifier());
        String prefStr = String.join(",", list);

        return prefStr;
    }
    private static void setLanguageSettingString(String str) {
        ArrayList<Language> newLang = new ArrayList<>();
        String[] list = str.split(",");
        if (list.length > 0) {
            for (Language lang : supportedLanguages) {
                for ( String lstr : list ) {
                    if (lang.getIdentifier().equals(lstr)) {
                        newLang.add(lang);
                    }
                }
            }
        }
        if (newLang.size() > 0) {
            selectedLanguages.clear();
            selectedLanguages.setAll(newLang);
        }
    }

    public static void setDirection( Direction dir ) {
        direction = dir;
    }
    public static Direction getDirection() {
        return direction;
    }

    /**
     * after database has been opened, read language specific settings from database
     */
    public static void initializeFromDatabase() {
        useWikiMedia = getBooleanPref("useWikiMedia", useWikiMedia);
        useWiktionary = getBooleanPref("useWiktionary", useWiktionary);
        userName = getStringPref( "userName", null );
        defaultCategory = getStringPref("defaultCategory", defaultCategory);
        int dim[] = split(getStringPref("imageSize", imageWidth + "," + imageHeight));
        if (dim != null && dim.length == 2)
        {
            imageHeight = dim[0];
            imageWidth = dim[1];
        }
        language.loadDatabasePreferences();

        cardQuestionTypeFront = Globals.getStringPref( "QuestionTypeFront", "TextInput").equals("TextInput") ?
                UserQuestionType.Text : UserQuestionType.Choice;
        cardQuestionTypeBack = Globals.getStringPref( "QuestionTypeBack", "ChoiceInput").equals("TextInput") ?
                UserQuestionType.Text : UserQuestionType.Choice;
        if ( ! (Database.getDatabase() instanceof H2Database ) ) // for local database, language info is saved in ini file
            setLanguageSettingString(getStringPref("SelectedLanguages",""));
    }

    public static int[] getBoxDates( Direction direction ) {
        return direction == Direction.FRONT ? boxDates : boxDatesReverse;
    }

    public static void setBoxDates( int[] boxDates1, int[] boxDates2) {
        boolean valid;

        valid = checkBoxDates( boxDates1 );
        if (valid)
        {
            boxDates = boxDates1;
            Database.getDatabase().setPrefs("boxDates"+language.getDatabaseSuffix(), join(boxDates));
        }
        valid = checkBoxDates( boxDates2 );
        if (valid)
        {
            boxDatesReverse = boxDates2;
            Database.getDatabase().setPrefs("boxDatesReverse"+language.getDatabaseSuffix(), join(boxDatesReverse));
        }
    }

    private static boolean checkBoxDates(int[] boxDates) {
        boolean valid = true;
        if ( (boxDates == null) || (boxDates.length < 2) || (boxDates.length > 10) ) valid = false;
        if ( valid ) {
            for (int i=1; i<boxDates.length; i++) {
                if (boxDates[i] < boxDates[i-1]) {
                    valid = false;
                    break;
                }
            }
        }
        return valid;
    }

    /** get height for JList and JTable cells */
    public static int getFontHeight() {
        java.awt.Font font = (java.awt.Font) javax.swing.UIManager.get("List.font");
        java.awt.FontMetrics fm = new javax.swing.JLabel().getFontMetrics(font);
        return fm.getHeight() + fm.getLeading();
    }

    public static Font getFont() {
        return font;
    }

    public static void setFont(Font font) {
        Globals.font = font;
        saveLocalSettings();
    }
    public static boolean useWikiMedia() {
        return useWikiMedia;
    }
    public static void setUseWikiMedia(boolean useWikiMedia) {
        Globals.useWikiMedia = useWikiMedia;
        Database.getDatabase().setPrefs("useWikiMedia", "" + useWikiMedia);
    }
    public static String getUserName() {
        return userName;
    }
    public static void setUserName(String userName) {
        Globals.userName = userName;
        Database.getDatabase().setPrefs("userName", userName);
    }
    public static String getDefaultCategory() {
        return defaultCategory;
    }
    public static void setDefaultCategory(String defaultCategory) {
        Globals.defaultCategory = defaultCategory;
        Database.getDatabase().setPrefs("defaultCategory", defaultCategory);
    }

    public static int getImageWidth() {return imageWidth;}
    public static int getImageHeight() {return imageHeight;}
    public static void setImageSize(int width, int height) {
        Globals.imageHeight = height;
        Globals.imageWidth = width;
        Database.getDatabase().setPrefs("imageSize", width + "," + height);
    }
    public static boolean useWiktionary() {
        return useWiktionary;
    }
    public static void setUseWiktionary(boolean useWiktionary) {
        Globals.useWiktionary = useWiktionary;
        Database.getDatabase().setPrefs("useWiktionary", "" + useWiktionary);
    }
    public static String getDumpDir() { return dumpDir;}


    //------------- utility functions -----------------
    public static void setPref(String key, String value) {
        Database.getDatabase().setPrefs(key, value);
    }

    /** Returns the value associated with the specified key from database
     * @param key key whose associated value is to be returned
     * @param def default value, if no value for the given key exists
     */
    public static int getIntPref( String key, int def ) {
        String str = Database.getDatabase().getPrefs(key);
        int val = def;
        try {
            val = Integer.parseInt( str );
        } catch (Exception e) {
        }
        return val;
    }
    public static String getStringPref( String key, String def ) {
        String val = Database.isValid() ? Database.getDatabase().getPrefs(key) : null;
        if ( val == null ) val = def;
        return val;
    }
    public static boolean getBooleanPref( String key, boolean def ) {
        String str = Database.getDatabase().getPrefs(key);
        boolean val = def;
        if (str != null) {
            try {
                val = Boolean.parseBoolean(str);
            } catch (Exception e) {
            }
        }
        return val;
    }

    public static String join(int[] arr) {
        if (arr == null || arr.length == 0) return null;
        String ret = ""+arr[0];
        for (int i=1; i<arr.length; i++) {
            ret += ","+arr[i];
        }
        return ret;
    }
    public static int[] split(String str) {
        int[] ret = null;
        try {
            String[] s = str.split(",");
            ret = new int[s.length];
            for (int i = 0; i < s.length; i++) {
                ret[i] = Integer.parseInt(s[i]);
            }
        } catch (Exception e) {
            ret = null;
        }
        return ret;
    }

    public static String getInstallationRoot() {
        return installationRoot;
    }

    public static void setInstallationRoot(String installationRoot) {
        Globals.installationRoot = installationRoot;
    }

    public static UserQuestionType getQuestionType()
    {
        return ( direction == Direction.FRONT ) ? cardQuestionTypeFront : cardQuestionTypeBack;
    }
    public static UserQuestionType getQuestionType( Direction direction )
    {
        return ( direction == Direction.FRONT ) ? cardQuestionTypeFront : cardQuestionTypeBack;
    }
    public static void setQuestionType( Direction direction,  UserQuestionType type )
    {
        if ( direction == Direction.FRONT ) {
            cardQuestionTypeFront = type;
        }
        else {
            cardQuestionTypeBack = type;
        }
        Globals.setPref("QuestionTypeFront", cardQuestionTypeFront == UserQuestionType.Text ? "TextInput" : "ChoiceInput");
        Globals.setPref("QuestionTypeBack",  cardQuestionTypeBack  == UserQuestionType.Text ? "TextInput" : "ChoiceInput");
    }
    /**
     * Reads local settings from local ini-file. The remaining settings are stored in the database.
     */
    private static void readLocalSettings() {
        try {
            Properties prop = new Properties();
            String filename = getInstallationRoot() + File.separator + "localSettings.ini";
            FileInputStream in = new FileInputStream( filename );
            prop.load(in);
            in.close();

            setLanguageSettingString(prop.getProperty("selectedLanguages", ""));

            String fontName = prop.getProperty("fontName", Font.SANS_SERIF);
            int fontSize = Integer.parseInt(prop.getProperty("fontSize", "16"));
            font = new Font( fontName, Font.PLAIN, fontSize );
        } catch (Exception e) {
            font = new Font( Font.SANS_SERIF, Font.PLAIN, 16 );
            System.err.println(e);
        }
    }
    private static void saveLocalSettings() {
        try {
            Properties prop = new Properties();
            String filename = getInstallationRoot() + File.separator + "localSettings.ini";
            FileOutputStream out = new FileOutputStream( filename );
            prop.setProperty("fontName", font.getFamily());
            prop.setProperty("fontSize", font.getSize()+"");
            prop.setProperty("selectedLanguages", getLanguageSettingString() );
            prop.store(out, "");
            out.close();
        } catch (Exception e) {e.printStackTrace();}
    }

    public static ObservableList<String> getCategoryList() {
        ObservableList<String> list = FXCollections.observableArrayList();
        try
        {
            ArrayList<String> categories = Database.getDatabase().getCategoryNames();
            for (String name: categories) {
                list.add(name);
            }
            list.sort(Comparator.reverseOrder());
            if (categories.isEmpty()) {
                list.add("");
            }
            if ( !categories.contains( Globals.getDefaultCategory() ) ) {
                list.add(Globals.getDefaultCategory());
            }
            list.add( Lesson.ERROR_BOX_CATEGORY_LABEL );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
