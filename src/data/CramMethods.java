package data;
import java.util.*;

/**
 * CramMethods is a vector of different methods how wrong results are handled.
 */
public class CramMethods extends ArrayList<CramMethod>
{
    public CramMethods() {
        add(new RandomCard());
        add(new Repeat5());
        add(new DirectRepeat());
    }
}

class RandomCard implements CramMethod
{
    public void handleError(ArrayList<Card> list, Card error)
    {
        int idx = (int) (list.size() / 2 * Math.random()); // random position in the first half of the list
        list.add(idx, error); // card is taken from the end of the list -> put error into the first half
    }
    public String toString() {
        return "Fehler zufällig wiederholen";
    }
}

class DirectRepeat implements CramMethod
{
    public void handleError(ArrayList<Card> list, Card error)
    {
        list.add(list.size(), error);
    }
    public String toString() {
        return "Fehler direkt wiederholen";
    }
}

class Repeat5 implements CramMethod
{
    public void handleError(ArrayList<Card> list, Card error)
    {
        int size = list.size();
        int rand = ((int) (5*Math.random()))+1;
        rand = rand > size ? size : rand; // repeat within 5 cards; if less than 5 cards, insert at last position
        list.add(size - rand, error);
    }
    public String toString() {
        return "Fehler innerhalb von 5 Karten wiederholen";
    }
}

