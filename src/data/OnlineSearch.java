package data;
import java.net.*;
import java.util.ArrayList;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.image.BufferedImage;
import java.io.*;
import org.unbescape.java.JavaEscape; // to unescape special characters from jscon output
import org.unbescape.uri.UriEscape; // to escape special characters in https request

import javax.imageio.ImageIO;
import java.awt.Dimension;

// alternative search in Wikionary: not suitable for results with multiple words
// https://de.wiktionary.org/w/api.php?action=query&format=json&prop=iwlinks&iwprefix=en&titles=Katze

/**
 * class to search for images, sounds and translations from wikipedia and wiktionary
 * todo: improve search, there must be a better way to do it
 */
public class OnlineSearch {
    private static String languageIdentifier = "english";
    private static String wikiLanguage = "en"; // used for wiktionary results
    private static String soundPrefixes[] = {"En-us-","En-us-inlandnorth-","En"};

    private static String imageSearch1 = "https://en.wikipedia.org/w/api.php?action=query&format=json&rawcontinue&generator=images&prop=imageinfo&iiprop=url&";
    private static String imageSearch2 = "https://commons.wikimedia.org/w/api.php?action=query&format=json&generator=search&gsrnamespace=6&gsrlimit=30&prop=imageinfo&iiprop=url";

    private static String soundSearch = "https://"+ wikiLanguage +".wiktionary.org/w/api.php?action=query&format=json&rawcontinue&generator=images&prop=imageinfo&iiprop=url&";
    private static String completePageSearch = "https://de.wiktionary.org/w/api.php?action=query&format=json&rvprop=content&prop=revisions&";
    private static String translationSearch = completePageSearch;

    public static void setLanguage(String identifier)
    {
        languageIdentifier = identifier;
        if ( identifier.equals("french") ) {
            wikiLanguage = "fr";
            soundPrefixes = new String[]{"Fr"};
        }
        else if ( identifier.equals("nl") ) {
            wikiLanguage = "nl";
            soundPrefixes = new String[]{"Nl"};
        }
        else
        {
            wikiLanguage = "en";
            soundPrefixes = new String[]{"En-us-","En-us-inlandnorth-","En"};
        }
    }

    static {
        System.setProperty("http.agent", "JCardBox/0.9");
    }

    private static Word analyseWord(String str, String language)
    {
        String[] definite = null;
        String[] indefinite = null;

        if ( language.equals("german") )
        {
            definite = new String[] {"der", "die", "das"};
            indefinite = new String[] {"ein", "eine"};
        } else if ( language.equals("french") ) {
            definite = new String[] {"la", "le"};
            indefinite = new String[] {"une", "un"};
         } else if ( language.equals("nl") ) {
            definite = new String[] {"de", "het"};
            indefinite = new String[] {"een"};
       } else if ( language.equals("english") ) {
            if (str.startsWith( "to "))
                str = str.replaceFirst("to ", " ");
            return new Word( str, false, false );
        }
        else {
            return new Word( str, false, false );
        }

        for (String article : definite) {
            if (str.startsWith(article+" ")) {
                str = str.replaceFirst(article + " ", "").trim();
                return new Word(str, true, true);
            }
        }
        for (String article : indefinite) {
            if (str.startsWith(article+" ")) {
                str = str.replaceFirst(article + " ", "").trim();
                return new Word(str, true, false);
            }
        }

        return new Word( str, false, false );
    }

    private static String imageSize() {
        Dimension dim = Globals.getImageSize();
        return "&iiurlheight="+dim.height+"&iiurlwidth="+dim.width+"&";
    }

    /** search for simple past and past participle
     * @param title: infinitive + LanguageEnglish.tenses
     * @return past tense(s)
     * */
    private static ArrayList<String> searchTense( String title )
    {
        ArrayList<String> tenses = new ArrayList<>();
        String tenseStr = "";
        for (String str : LanguageEnglish.tenses ) {
            if (title.contains(str)) {
                tenseStr = str;
                title = title.replaceAll("\\[" + str + "\\]", "");
            }
        }
        if ( tenseStr.contains("simple") && tenseStr.contains("past") ) {
            tenseStr = "past_simple_I";
            tenses.add(tenseStr);
        }
        else if (tenseStr.equals("past"))
        {
            tenses.add("past_simple_I");
            tenses.add("past participle");
        }
        else
            tenses.add(tenseStr);

        if ( title.trim().length() == 0 ) return new ArrayList<>();

        StringJoiner foundTense = new StringJoiner(" ");
        try {
            String query = translationSearch + "titles=" + title;
            String result = loadURLContents(query);

            for (String searchTense : tenses ) {
                if (result.contains(searchTense)) {
                    Pattern pattern = Pattern.compile(searchTense + "[^=]*.([^\\\\]*)");
                    Matcher matcher = pattern.matcher(result);
                    if (matcher.find()) {
                        String found = matcher.group(1);
                        foundTense.add(found);
                    }
                }
            }

        } catch (IOException e) {
            System.out.println("searchTranslation:"+e);
        }

        ArrayList<String> list = new ArrayList<>();
        if (foundTense.length() > 0)
            list.add(foundTense.toString());
        return list;
    }

    public static ArrayList<String> searchTranslation( String title )
    {
        Word word = analyseWord(title, "german");
        title = word.getText();
        boolean isVerb = false;

        for (String str : LanguageEnglish.tenses )
        {
            if (title.contains(str))
            {
                 return searchTense(title);
            }
        }

        if ( title.length() == 0 ) return new ArrayList<>();
        ArrayList<String> lines = new ArrayList<>();
        try {
            String query = translationSearch + "titles=" + UriEscape.escapeUriPath(title);
            String result = loadURLContents(query);
            //System.out.println("q: "+query);

            if ( result.contains("Wortart|Verb|Deutsch") )
            {
                isVerb = true;
            }
            // System.out.println(query);
            // System.out.println("q:"+query);
            // translation part starts with Tabelle
            int idx = result.indexOf("Tabelle") + 7;
            if ( idx <= 0 ) return lines;
            String translations = result.substring( idx );
            idx = translations.indexOf("Dialekttabelle") ;
            if ( idx > 0 ) // cut after end of translations
                translations = translations.substring( 0, translations.indexOf("Dialekttabelle") );

            //  [4] {{\u00dc|fr|faire}} un {{\u00dc|fr|cours}}\n*
            Pattern pattern = Pattern.compile("\\{\\{"+ wikiLanguage +"\\}\\}:([^*]*)");
            Matcher matcher = pattern.matcher(translations);
            if (matcher.find()) {
                String found = matcher.group(1); // found = all translations for given languageString
                //System.out.println("found: "+found);

                found = found.replaceAll("\\[[^]]*\\]", ""); // remove [contents], e.g. [1-4]
                found = found.replaceAll("''[^']*''", ""); // remove contents of ''contents''

                String[] results = found.split(",|;");
                for (String r : results) {
                    // System.out.println("======= "+r);
                    if ( wikiLanguage.equals("fr") ) {
                        if (r.contains("{{f}}")) {
                            String gender = word.isDefinite() ? "la " : "une ";
                            r = gender + r;
                        }
                        else if (r.contains("{{m}}")) {
                            String gender = word.isDefinite() ? "le " : "un ";
                            r = gender + r;
                        }
                    }
                    if ( wikiLanguage.equals("nl") ) {
                        if (r.contains("{{f}}") || r.contains("{{n}}")) {
                            String gender = "het";
                            r = gender + r;
                        }
                        else if (r.contains("{{m}}")) {
                            String gender = "de";
                            r = gender + r;
                        }
                    }
                    r = r.replaceAll("\\{[^|]*.[^|]*.", ""); // remove from beginning e.g. {{..|en|
                    r = r.replaceAll("\\([^)]*.", ""); // remove from beginning e.g. {{..|en|
                    r = r.replaceAll("\\}\\}", ""); // remove ending "}}"
                    r = r.replaceAll("\\\\n.*","");    // remove everything after \n
                    r = r.replaceAll("\\s+", " ");  // remove multiple whitespaces in the middle
                    r = JavaEscape.unescapeJava(r); // for special chars
                    r = r.trim();

                    if ( wikiLanguage.equals("en") && isVerb && !r.startsWith("to ") )
                    {
                        r = "to "+r;
                    }

                    if (!lines.contains(r))
                        lines.add(r);
                }
            }
        } catch (IOException e) {
            System.out.println("searchTranslation:"+e);
        }
        //for(String str:lines) System.out.println(str);

        return lines;
    }

    public static ArrayList<String> searchSounds( String title ) throws IOException
    {
        Word word = analyseWord(title, languageIdentifier );
        title = UriEscape.escapeUriPath(word.getText());

        String query = soundSearch + "titles=" + title;
        System.out.println("sound query: "+query);
        String result = loadURLContents(query);

        ArrayList<String> sounds = new ArrayList<String>();
        Pattern pattern = Pattern.compile("(https://upload.wikimedia.org[^\"]*)");
        Matcher matcher = pattern.matcher(result);
        ArrayList<String> lines = new ArrayList<String>();

        while (matcher.find())
        {
            String found = matcher.group(1);
            if (found.contains("ogg")) { // only search for ogg files
                lines.add(found);
            }
        }

        for ( String soundPrefix : soundPrefixes) {
            String search = ( soundPrefix.endsWith("-") ) ? soundPrefix + title : soundPrefix + "-";
            System.out.println("search "+search);
            for (int i = 0; i < lines.size(); i++) {
                if (lines.get(i).contains(search)) {
                    sounds.add(lines.get(i));
                    lines.remove(i);
                    i--;
                }
            }
        }

        return sounds;
    }

    public static byte[] readSound(String soundUrl) throws IOException {
        URL url = new URL(soundUrl);
        URLConnection con = url.openConnection();
        int contentLength = con.getContentLength();
        String contentType = con.getContentType();

        if (contentType.startsWith("text/") || contentLength == -1) {
            throw new IOException("Not a binary file: "+soundUrl);
        }
        InputStream in = new BufferedInputStream(con.getInputStream());
        byte[] data = new byte[contentLength];
        int offset = 0;
        while (offset < contentLength) {
            int bytesRead = in.read(data, offset, data.length - offset);
            if (bytesRead == -1)
                break;
            offset += bytesRead;
        }
        in.close();

        if (offset != contentLength) {
            throw new IOException("Only read " + offset + " bytes; Expected " + contentLength + " bytes");
        }

        /*String filename = "a.ogg";
        FileOutputStream out = new FileOutputStream(filename);
        out.write(data);
        out.flush();
        out.close();*/

        return data;
    }

    private static String loadURLContents( String urlName ) throws IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlName);
        URLConnection con = url.openConnection();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            result.append(inputLine);
        }
        in.close();

        return result.toString();
    }

    /**
     * searches in wikimedia for images
     * @param title image title
     * @return list of URLs
     * @throws IOException
     */
    public static ArrayList<String> searchImages( String title ) throws IOException {
        Word word = analyseWord(title, languageIdentifier );
        title = word.getText();
        ArrayList<String> result = searchImagesWikionary(title);
        result.addAll(searchImages2(title));
        return result;
    }

    /**
     * searches for images on the wiktionary page of the word : often better results than wiki search
     */
    private static ArrayList<String> searchImagesWikionary( String title ) throws IOException {
        if ( title.length() == 0 ) return new ArrayList<>();

        ArrayList<String> images = new ArrayList<>();
        ArrayList<String> lines = new ArrayList<>();
        try {
            String query = translationSearch + "titles=" + UriEscape.escapeUriPath(title);
            String result = loadURLContents(query);
            int end = 0;
            while (end >= 0) {
                end = result.indexOf(".jpg", end+1);
                if (end > 0) {
                    int start = end - 1;
                    while (result.charAt(start) != '=') start--;
                    String found = result.substring(start + 1, end + 4);
                    lines.add( found );
                }
            }

            // search for image in given size
            for (String img : lines) {
                img = img.replace(' ','_');
                query = "https://commons.wikimedia.org/w/api.php?action=query&format=json&prop=imageinfo&iiprop=url&"+imageSize()+"titles=File:"+img;
                result = loadURLContents(query);

                // search in result for URLs beginning with "thumburl":"http://.... (these images are resized to iiurlwidth)
                Pattern pattern = Pattern.compile("\"thumburl\":\"(https:[^\"]*)");
                Matcher matcher = pattern.matcher(result);
                //System.out.println("wikiquer: "+query);
                while (matcher.find())
                {
                    images.add(matcher.group(1));
                    System.out.println("wikiurl: "+images.get(images.size()-1));
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return images;
    }

    private static ArrayList<String> searchImages1( String title ) throws IOException
    {
        String query = imageSearch1 + imageSize() + "titles="+title;
        //System.out.println("q:"+query);
        String result = loadURLContents(query);

        ArrayList<String> images = new ArrayList<String>();
        // search in result for URLs beginning with "thumburl":"http://.... (these images are resized to iiurlwidth)
        Pattern pattern = Pattern.compile("\"thumburl\":\"(http:[^\"]*)");
        //Pattern pattern = Pattern.compile("\"url\":\"(http:[^\"]*)"); // full siyze image
        Matcher matcher = pattern.matcher(result);
        while(matcher.find())
        {
            images.add(matcher.group(1));
        }

        return images;
    }
    public static ArrayList<String> searchImages2( String title ) throws IOException
    {
	title=UriEscape.escapeUriPath(title);
        String query = imageSearch2 + imageSize() + "&gsrsearch=%22"+title+"%22";
        //System.out.println("q:"+query);
        String result = loadURLContents(query);

        ArrayList<String> images = new ArrayList<String>();
        // search in result for URLs beginning with "thumburl":"http://.... (these images are resized to iiurlwidth)
        Pattern pattern = Pattern.compile("\"thumburl\":\"(https:[^\"]*)");
        Matcher matcher = pattern.matcher(result);
        while(matcher.find())
        {
            images.add(matcher.group(1));
        }

        return images;
    }

    public static void readImage(String imageUrl, ImageListener loader)  {
        new ImageLoadThread(imageUrl, loader).start();
    }

    private static class ImageLoadThread extends Thread {
        private String imageURL;
        private ImageListener loader;

        public ImageLoadThread(String url, ImageListener loader) {
            this.imageURL = url;
            this.loader = loader;
        }
        public void run() {
            try {
                URL url = new URL(imageURL);
                URLConnection con = url.openConnection();
                InputStream ins = con.getInputStream();
                BufferedImage img = ImageIO.read(ins);
                ins.close();
                loader.imageLoaded(img);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        setLanguage("french");
        //searchTranslation("lesen"); // mehrere Links im Ergebnis auf Französisch
        setLanguage("english");
        searchTranslation("eines Tages");
        //searchTranslation("riechen");
        //searchImages(search);
        //ArrayList<String> list = searchImages1(search);
        //ArrayList<String> list1 = searchSounds(search);
        //ArrayList<String> list = searchImagesWikionary( search );
        //setLanguage("french");
        //ArrayList<String> list = searchTranslation(search);
        //setLanguage("english");
        //byte[] raw = null;
        //if (!list.isEmpty()) {raw = readSound(list.get(0));} AudioPlayer.playClip(raw);
    }

}


class Word
{
    public Word(String text, boolean isNoun, boolean isDefinite) {
        this.text = text;
        this.isNoun = isNoun;
        this.isDefinite = isDefinite;
    }
    public String getText() {
        return text;
    }
    public void setIsDefinite(boolean isDefinite) {
        this.isDefinite = isDefinite;
    }
    public boolean isDefinite() {
        return isDefinite;
    }

    private String text;
    private String shortText;
    private boolean isDefinite; // if noun, true if there is a definite article otherwise indefinite
    private boolean isNoun;
}

