package data;
/*
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by anke on 8/16/15.
 *//*
public class OnlineSearchTest {

    @Test
    public void testSearchTranslationEnglish() throws Exception {
        String [][] translations = {
                {"Katze","cat"},
                {"lesen","to read"},
                {"see [past participle]","seen"},
                {"see [simple past]","saw"},
                {"see [past]","saw seen"}
        };
        OnlineSearch.setLanguage("english");
        for (String[] arr : translations) {
            System.out.println(arr[0]);
            String result1 = OnlineSearch.searchTranslation(arr[0]).get(0);
            assertEquals(arr[1], result1);
        }
    }
    @Test
    public void testSearchTranslationFrench() throws Exception
    {
        String [][] translations = {
                {"einzeln","seul"},   // only replace "ein" if it's an article
                {"lesen","lire"},     // links in translation
                {"zu Fuß", "à pied"}, // a phrase
                {"ein Haus","une maison"}, // indefinite article
                {"eine Katze","un chat"},
                {"die Katze","le chat"} // definite article
        };

        OnlineSearch.setLanguage("french");
        for (String[] arr : translations) {
            String result1 = OnlineSearch.searchTranslation(arr[0]).get(0);
            assertEquals(arr[1], result1);
        }
    }

    @Test
    public void testSearchSounds() throws Exception {
        OnlineSearch.setLanguage("english");
        int results = OnlineSearch.searchSounds("cat").size();
        assertTrue("found sounds for 'cat'", results > 0);
    }

    @Test
    public void testSearchImages() throws Exception {
        OnlineSearch.setLanguage("english");
        ArrayList<String> list = OnlineSearch.searchImages("cat");
        assertTrue("found sounds for 'cat'", list.size() > 0);
    }
}
*/