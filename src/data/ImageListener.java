package data;

import java.awt.image.BufferedImage;

public interface ImageListener {
	void imageLoaded( BufferedImage image );
}

