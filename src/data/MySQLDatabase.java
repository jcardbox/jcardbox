package data;

import java.io.File;
import java.sql.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by anke on 8/9/15.
 */
public class MySQLDatabase extends SQLDatabase
{
    private static HashMap<String, String> blobQueryImport = new HashMap<>();
    private static HashMap<String, String> blobQueryExport = new HashMap<>();
    private static final String options = "character set utf8 FIELDS TERMINATED BY ',' ENCLOSED BY '\"' ";

    static
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        blobQueryExport.put(MySQLDatabase.IMAGE_TABLE, "SELECT card_id, hex(image), lastChange from %s ");
        blobQueryExport.put(MySQLDatabase.AUDIO_TABLE, "SELECT card_id, hex(audio), lastChange from %s ");
        blobQueryImport.put(MySQLDatabase.IMAGE_TABLE, "LOAD DATA LOCAL INFILE '%s' INTO TABLE %s  %s(card_id, @heximage, lastChange) SET image=UNHEX(@heximage)");
        blobQueryImport.put(MySQLDatabase.AUDIO_TABLE, "LOAD DATA LOCAL INFILE '%s' INTO TABLE %s  %s(card_id, @hexaudio, lastChange) SET audio=UNHEX(@hexaudio)");
     }

    protected String getReplaceStatement() {return "REPLACE";}

    protected void loadDump(boolean withoutBlobs) throws SQLException
    {
        // todo handle version info: setPrefs("DatabaseVersion", databaseVersion);
        con.setAutoCommit(false);

        for (Language lang : Globals.getSelectedLanguages()) {
            Globals.setLanguage(lang);

            deleteTables();
            createTables();

            HashMap<String, String> tables = withoutBlobs ? basicTables : allTables;
            for (String tablename : tables.keySet()) {
                String filename = dumpDir + File.separator + tablename + ".csv";
                String columns = tables.get(tablename);

                fillTableFromCSV(tablename, columns, filename);
            }
        }
        con.commit();
        con.setAutoCommit(true);
    }

    protected void saveDump( boolean withoutBlobs ) throws SQLException {
        // use local h2 database to save to csv, "SELECT * from %s INTO OUTFILE" doesn't work for remote mysql databases.
        H2Database localTmpDB = (H2Database) SQLDatabase.connect("jdbc:h2:mem:", "", "");
        localTmpDB.setDumpDirectory(dumpDir);
        localTmpDB.con.setAutoCommit(false);

        con.setAutoCommit(false);
        long time = new java.util.Date().getTime();

        for (Language lang : Globals.getSelectedLanguages()) {
            Globals.setLanguage(lang);
            localTmpDB.setLanguage(lang);

            setPrefs("lastDumpTimestamp", "" + time);

            HashMap<String, String> tables = withoutBlobs ? basicTables : allTables;

            for (String tablename : tables.keySet()) {
                exportTableIntoH2Database(tablename, localTmpDB);
            }
        }
        con.commit();
        con.setAutoCommit(true);

        localTmpDB.saveDump(withoutBlobs);
        localTmpDB.con.setAutoCommit(true);
        localTmpDB.closeConnection();
    }

    /**
     * @param table
     * @param localDB H2 database to save the data of the MySQL Database.getDatabase().
     * MySQL command "SELECT * from %s INTO OUTFILE" doesn't work for remote mysql databases. So, the data is written into
     * a H2 database and then H2 is used to create the csv-dump.
     */
    private void exportTableIntoH2Database(String table, H2Database localDB) {
        // System.out.println("export table "+table);

        try {
            String query = blobQueryExport.get(table);
            if (query == null) {
                query = "SELECT * from %s ";
            }
            query = String.format(query, table);
            Statement stat = con.createStatement();
            ResultSet rs = stat.executeQuery(query);

            ArrayList<String> columnNames = getColumnNames(table);
            String columns = String.join(",", columnNames);

             while (rs.next())
             {
                String questionMarks = "?";
                for  (int i=1; i<columnNames.size(); i++) {
                    questionMarks += ",?";
                }

                String cmd = String.format("INSERT INTO %s(%s) Values(%s)", table, columns, questionMarks);
                PreparedStatement ps = localDB.con.prepareStatement(cmd);

                for (int i=1; i <= columnNames.size(); i++) {
                    Object obj = rs.getObject(i);
                    ps.setObject(i, obj);
                    //System.out.println(cmd + "  obj: " + obj);
                }
                //System.out.println("query "+cmd);
                ps.executeUpdate();
                ps.close();
            }
            stat.close();
        } catch ( Exception e) {
            e.printStackTrace();
        }
    }

    private void fillTableFromCSV(String table, String columns, String csvFileName)
    {
        /** special case: blobs in table carddata (image and audio) are saved hex encoded */
        String query = blobQueryImport.get(table);
        if (query == null) {
            query = "LOAD DATA LOCAL INFILE '%s' INTO TABLE %s %s ";
        }
        query= String.format(query, csvFileName, table, options);

        //System.out.println("import table "+table);
        try {
            Statement stat = con.createStatement();
            System.out.println("read dump "+query);
            stat.executeQuery(query);
            stat.close();
        } catch ( SQLException e) {
            e.printStackTrace();
        }
    }
}

