package data;
/** This class is automatically generated, DO NOT EDIT */

public class Version {
   public static final String VERSION="040674a";
   public static final String DATE="2021-01-02";
   
   public static String getVersion() {
      return VERSION + " (" + DATE + ")";
   }
}
