package data;

import javax.swing.*;

import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import net.miginfocom.swing.MigLayout;

import java.awt.Color;
import java.util.ArrayList;

/**
 * Created by Anke Visser on 9/6/16.
 */
public class LanguageFrench extends Language
{
    private FrenchKeyListener keyListener = new FrenchKeyListener();
    private static Color red = new Color(255, 225, 225);
    private static Color blue = new Color(215, 235, 255);

    @Override
    public Gender getGender( String word )
    {
        Gender gender;
        if ( word.startsWith("une") || word.startsWith("la") )
            gender = Gender.FEMALE;
        else if ( word.startsWith("un") || word.startsWith("le") )
            gender = Gender.MALE;
        else
            gender = Gender.UNDEFINED;
        return gender;
    }

    public void test() {
        boolean ok = Globals.getBooleanPref("useSpecialChar", true);
        if (ok) {
            String str = Globals.getStringPref("specialChar", "´");
        }
    }

    @Override
    public Color getColor( Gender gender )
    {
        Color color = null;
        if ( gender == Language.Gender.MALE )
            color = blue;
        else if ( gender == Language.Gender.FEMALE )
            color = red;
        return color;
    }

    @Override
    public String getLabel() {
        return "Französisch";
    }

    @Override
    public String getIdentifier() {
        return "french";
    }

    @Override
    public String getDatabaseSuffix() {
        return "_french";
    }

    @Override
    public boolean isConfigurable() {
        return true;
    }

    @Override
    public void configure( java.awt.Component parent ) {
        new ConfigDialog( parent ).setVisible(true);
    }

    class ConfigDialog extends JDialog
    {
        private JCheckBox check;
        public ConfigDialog( java.awt.Component parent )
        {
            setTitle(LanguageFrench.this.getLabel());
            JLabel header = new JLabel("Sprachkonfiguration: "+LanguageFrench.this.getLabel());
            java.awt.Font font = new java.awt.Font(header.getFont().getName(), java.awt.Font.BOLD, (int) (header.getFont().getSize()+2));
            header.setFont( font );

            FrenchKeyListener keyListener = LanguageFrench.this.keyListener;
            keyListener.setSpecialChar(Globals.getStringPref("specialChar", "´").charAt(0));
            boolean useSpecial = Globals.getBooleanPref("useSpecialChar",true);
            int rgb = Integer.parseInt(Globals.getStringPref("maleColorFrench", String.valueOf(blue.getRGB())));
            blue = new Color(rgb);
            rgb = Integer.parseInt( Globals.getStringPref("femaleColorFrench", String.valueOf(red.getRGB()) ) );
            red = new Color(rgb);

            JTextArea doc = new JTextArea();//todokeyListener.doc());
            doc.setEditable(false);
            JButton change = new JButton( "Spezialtaste ändern");
            change.addActionListener( e1 -> changeKey() );

            check = new JCheckBox("Sonderzeichen mit Spezialtaste (" + keyListener.getSpecialChar() +") generieren");
            check.setSelected(useSpecial);
            check.addActionListener( e2 ->  change.setEnabled(check.isSelected()));
            change.setEnabled(check.isSelected());

            JButton male = new JButton("männlich");
            male.addActionListener( e1 -> { blue = chooseColor( blue ); male.setBackground( blue ); } );
            male.setBackground( blue );
            JButton female = new JButton("weiblich");
            female.addActionListener( e1 -> { red = chooseColor( red ); female.setBackground( red ); } );
            female.setBackground( red );

            setLayout(new MigLayout("fill, wrap1"));
            add(header, "wrap 10pt");
            add(new JLabel("Eingabe von französischen Sonderzeichen"));
            add(Box.createHorizontalStrut(20), "split 2"); add(doc);
            add(Box.createHorizontalStrut(20), "split 2"); add(check, "split2");
            add(Box.createHorizontalStrut(20), "split 2"); add(change, "wrap 10pt");

            add(new JLabel("Hintergrundfarbe von Nomen"));
            add(male, "split 2");
            add(female, "wrap 10pt");

            JButton cancel = new JButton("Abbruch");
            cancel.addActionListener(e -> setVisible(false) );
            JButton ok = new JButton("Ok");
            ok.addActionListener(e -> ok() );
            add(cancel, "split2,sizegroup bttn, align right"); //sizegroups set all members to the size of the biggest member
            add(ok, "sizegroup bttn");

            setLocationRelativeTo(parent);
            pack();
        }

        private Color chooseColor( Color start ) {
            Color color = JColorChooser.showDialog( null, "Farbe", start );
            if ( color != null )
                return color;
            else
                return start;
        }


        public void changeKey() {
            keyListener.showConfigDialog();
            char special = keyListener.getSpecialChar();
            check.setText("Sonderzeichen mit Spezialtaste (" + special +") generieren");
        }

        public void ok()
        {
            Globals.setPref("maleColorFrench", String.valueOf(blue.getRGB()) );
            Globals.setPref("femaleColorFrench", String.valueOf(red.getRGB()) );
            Globals.setPref("useSpecialChar", ""+check.isSelected());
            Globals.setPref("specialChar", ""+keyListener.getSpecialChar());
            setVisible(false);
        }
    }

    @Override
    public void setInput(TextField textField) {
        textField.addEventFilter(KeyEvent.KEY_TYPED, new FrenchKeyListener());
    }
    /**
     * Key listener to enter french characters with a ascii or german ke yboard
     * if SPECIAL_CHAR is inserted, the letter left to is changed to a special character similar to the original one
     */

    class FrenchKeyListener implements EventHandler<KeyEvent> {
        char specialChar = '#';
        private ArrayList<String> chars = new ArrayList<>();

        public String name() {
            return "Französische Sonderzeichen";
        }

        public FrenchKeyListener() {
            chars.add("aàâæ");
            chars.add("eèéêë");
            chars.add("iîï");
            chars.add("oôœ");
            chars.add("uùû");
            chars.add("cç");
            int size = chars.size();
            for ( int i=0; i < size; i++)
            {
                chars.add(chars.get(i).toUpperCase());
            }
        }

        public char getSpecialChar() {return specialChar;}
        public void setSpecialChar( char s ) {specialChar = s;}

        @Override
        public void handle(KeyEvent ke) {
            System.out.println("keyEv "+ke);
            TextField answerField = (TextField) ke.getSource();
            if (ke.getCharacter().length() == 0) return;
            int pos = answerField.getCaretPosition() - 1;
            char newChar = ke.getCharacter().charAt(0);

            if (newChar == specialChar && pos > 0) {
                char oldChar = answerField.getText().charAt(pos);
                for (String str : chars) {
                    int idx = str.indexOf(oldChar);
                    if (idx >= 0) {
                        char replaceChar = str.charAt((idx + 1) % str.length());
                        answerField.replaceText(pos, pos + 1, "" + replaceChar);
                        ke.consume();
                    }
                }
            }
        }

        /**
         * dialog to enter char to switch between french special characters
         * todo: handle not printable characters
         */
        public void showConfigDialog() {
        /*
        String start = "Zeichen: ";
        JLabel special = new JLabel(start+specialChar);

        Object[] elems={
                "Spezialzeichen definieren zum Erstellen von Sonderzeichen",
                "Gewünschtes Zeichen bitte tippen",
                special
        };
        JOptionPane options = new JOptionPane(elems, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
        JDialog dialog = options.createDialog(null, "Konfiguration");

        dialog.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowGainedFocus(WindowEvent e) {
                dialog.requestFocusInWindow();
            }
        });

        dialog.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                String str = (Character.isISOControl(e.getKeyChar())) ? e.getKeyText(e.getKeyCode()): ""+e.getKeyChar();
                tmpChar = e.getKeyChar();
                special.setText( start+str );
                e.consume();
            }
        });
        dialog.setFocusable(true);
        dialog.requestFocus();
        dialog.setVisible(true);
        if (options.getValue() != null && ((Integer) options.getValue() == JOptionPane.OK_OPTION )) {
            specialChar = tmpChar;
        }
        */
        }

    }

}

