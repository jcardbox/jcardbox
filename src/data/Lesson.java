package data;
import java.util.ArrayList;
import java.util.Date;

public class Lesson {
	public static final Lesson boxLesson = new Lesson("1. Kasten: Vokabeln für morgen", Lesson.ERROR_BOX_CATEGORY);

	private String name;
	private String category;
	private int lessonID = -1; // database id
	private Date creationDate = null;
	private ArrayList<Card> cards = null;
	private boolean isLoaded = false;

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public static final String ERROR_CRAM_CATEGORY = "ERROR_CRAM_CATEGORY"; // database value
	public static final String ERROR_BOX_CATEGORY = "ERROR_BOX_CATEGORY"; // database value
	public static final String ERROR_CRAM_CATEGORY_LABEL = "Fehler-Lernen";
	public static final String ERROR_BOX_CATEGORY_LABEL  = "Fehler-Vokabelkasten";

	public Lesson(String lessonName, String category) {
		this.name = lessonName;
		this.category = category;
		this.cards = null; // not yet loaded
	}
	public Lesson(String lessonName, String category, int lessonID, Date date ) {
		this.name = lessonName;
		this.category = category;
		this.lessonID = lessonID;
		this.creationDate = date;
		this.cards = null;
	}
	public Lesson(String lessonName, String category, ArrayList<Card> cards) {
		this.name = lessonName;
		this.category = category;
		this.cards = cards;
	}

	public void load( boolean loadMultimediaContents ) {
		if ( isLoaded  && category != ERROR_BOX_CATEGORY ) return;

		if ( category == ERROR_BOX_CATEGORY ) {
			cards = Database.getDatabase().getBoxErrors( Globals.getDirection(), loadMultimediaContents );
		} else {
			if (lessonID < 0) Database.getDatabase().loadLessonInfo(this);
			cards = Database.getDatabase().loadLesson(lessonID, loadMultimediaContents);
		}
        isLoaded = loadMultimediaContents && cards != null;
	}

	public int getID() {
		return lessonID;
	}
	public void setID( int id ) {
		lessonID = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String newName) {
		name = newName;
	}

	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public ArrayList<Card> getCardCopy() {
		return new ArrayList<>(cards);
	}
	public ArrayList<Card> getCards() {
		return cards;
	}
	public void setCards(ArrayList<Card> cards) {
		this.cards = cards;
	}
	public String toString() {
		String date = creationDate == null ? "" : " ("+creationDate+")";
		return name+date;
	}
}
