
package data;
/** This class is automatically generated, DO NOT EDIT */
public class Version {
   public static final String VERSION="2090ea2";
   public static final String DATE="2018-09-30";
   public static final boolean COMMITTED=false;
   
   public static String getVersion() {
      return VERSION + " (" + DATE + ")" + ( COMMITTED ? "" : " - not yet committed" );
   }
}

   