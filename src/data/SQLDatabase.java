package data;

import gui.dialogs.DefaultDialogs;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.control.Alert;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.sql.*;
import java.sql.Date;
import java.util.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.ImageIO;
import java.nio.file.Files;
import java.util.zip.*;

/**
 * Created by anke on 8/9/15.
 */
public abstract class SQLDatabase {
    public static HashMap<String, String> getBasicTables() {
        return basicTables;
    }

    protected static HashMap<String, String> basicTables = new HashMap<>();
    protected static HashMap<String, String> tablesWithBlobs = new HashMap<>();
    protected static HashMap<String, String> allTables = new HashMap<>();
    protected static String CARDS_TABLE = "cards";
    protected static String LESSONS_TABLE = "lessons";
    protected static String LNAMES_TABLE = "lesson_names";
    protected static String CARDBOX_TABLE = "cardbox";
    protected static String CARDBOXBACK_TABLE = "cardbox_back";
    protected static String LOG_TABLE = "log";
    protected static String PREF_TABLE = "preferences";
    protected static String AUDIO_TABLE = "audio_data";
    protected static String IMAGE_TABLE = "image_data";
    protected static String CARD_COLUMNS = CARDS_TABLE + ".card_id, front, back ";

    public static final String databaseVersion = "0002";
    protected Connection con = null;
    protected String dumpDir;

    public void loadDump() throws SQLException {
        loadDump(false);
    }

    public void saveDump() throws SQLException {
        saveDump(false);
    }

    private HashMap<String, ArrayList<String>> allSynonyms;

    private static void setTableNames(String language_suffix) {
        CARDS_TABLE = "cards" + language_suffix;
        LESSONS_TABLE = "lessons" + language_suffix;
        LNAMES_TABLE = "lesson_names" + language_suffix;
        CARDBOX_TABLE = "cardbox" + language_suffix;
        CARDBOXBACK_TABLE = "cardbox_back" + language_suffix;
        LOG_TABLE = "log" + language_suffix;
        PREF_TABLE = "preferences" + language_suffix;
        AUDIO_TABLE = "audio_data" + language_suffix;
        IMAGE_TABLE = "image_data" + language_suffix;

        CARD_COLUMNS = CARDS_TABLE + ".card_id, front, back ";

        basicTables.clear();
        tablesWithBlobs.clear();
        allTables.clear();

        basicTables.put(PREF_TABLE, "name VARCHAR(20) primary key, value VARCHAR(255)");
        basicTables.put(CARDS_TABLE, "card_id INT not null primary key auto_increment, front VARCHAR(255), back VARCHAR(255)");
        basicTables.put(LNAMES_TABLE, "lesson_id INT not null auto_increment, name VARCHAR(80), category VARCHAR(80), creation_date DATE, primary key(lesson_id)");
        basicTables.put(LESSONS_TABLE, "lesson_id INT not null, card_id INT not null, primary key(lesson_id, card_id)");
        basicTables.put(CARDBOX_TABLE, "card_id INT not null, box INT not null, insert_date DATE not null, primary key(card_id)");
        basicTables.put(LOG_TABLE, "logType INT not null, log_date DATE, logMessage VARCHAR(200)");

        basicTables.put(CARDBOX_TABLE, "card_id INT not null, box INT not null, insert_date DATE not null, primary key(card_id)");
        basicTables.put(CARDBOXBACK_TABLE, "card_id INT not null, box INT not null, insert_date DATE not null, primary key(card_id)");

        tablesWithBlobs.put(AUDIO_TABLE, "card_id INT not null primary key, audio MEDIUMBLOB, lastChange LONG");
        tablesWithBlobs.put(IMAGE_TABLE, "card_id INT not null primary key, image MEDIUMBLOB, lastChange LONG");

        allTables.putAll(basicTables);
        allTables.putAll(tablesWithBlobs);
    }

    protected static ArrayList<String> getColumnNames(String table) {
        String columns = allTables.get(table);
        // quick and dirty hack to get column names
        String[] split = columns.split(",");
        ArrayList<String> columnNames = new ArrayList<>();
        for (int i = 0; i < split.length; i++) {
            String col = split[i].trim().split(" ")[0];
            if (col.startsWith("primary")) break;
            columnNames.add(col);
        }
        return columnNames;
    }

    /**
     * recreates basicTables from csv files
     *
     * @param ignoreBlobs don't save basicTables with large binary data (tables with images and sounds)
     */
    protected abstract void loadDump(boolean ignoreBlobs) throws SQLException;

    /**
     * saves all basicTables to csv files
     *
     * @param ignoreBlobs don't save basicTables with large binary data (tables with images and sounds)
     */
    protected abstract void saveDump(boolean ignoreBlobs) throws SQLException;

    protected abstract String getReplaceStatement();

    protected SQLDatabase() {
    }

    private String databaseURL;
    private String user;
    private String password;

    public static SQLDatabase connect(String databaseURL, String user, String password) throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        if (!databaseURL.startsWith("jdbc:"))
            databaseURL = "jdbc:" + databaseURL;
        System.out.println("SQLDatabase::connect to " + databaseURL);
        Connection con = DriverManager.getConnection(databaseURL, user, password);

        SQLDatabase db;
        if (databaseURL.contains("jdbc:h2")) {
            db = new H2Database();
        } else {
            db = new MySQLDatabase();
        }
        db.con = con;

        db.databaseURL = databaseURL;
        db.user = user;
        db.password = password;

        db.setLanguage( Globals.getLanguage() );

        return db;
    }

    public void setLanguage( Language language ) {
        setTableNames(language.getDatabaseSuffix());
        createTables();
    }

    /**
     * checks current connection and tries to reconnect, if connection has become invalid
     * e.g. because of network problems
     */
    private void checkConnection() throws SQLException {
        final int timeout = 3; // in seconds
        SQLException err = null;

        if (!con.isValid(timeout)) {
            try {
                con = DriverManager.getConnection(databaseURL, user, password);
            } catch (SQLException e) {
                e.printStackTrace();
                err = e;
            }
        }
        if (!con.isValid(timeout)) {
            Thread.dumpStack();
            DefaultDialogs.showExceptionDialog("Fehler", "Keine Verbindung zur Datenbank", err );
        }
    }

    private Statement createStatement() throws SQLException {
        checkConnection();
        return con.createStatement();
    }

    protected void setDumpDirectory(String dumpDir) {
        if (dumpDir != null) {
            File d = new File(dumpDir);
            boolean ok = true;
            if (!d.isDirectory()) {
                ok = d.mkdirs();
            }
            if (!ok)
                System.err.println("Dump directory not available: " + dumpDir);
        }
        this.dumpDir = dumpDir;
    }

    public void closeConnection() {
        try {
            con.close();
            System.out.println("SQLDatabase::close ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void loadDumpWithoutBlobs() throws SQLException {
        loadDump(true);
    }

    protected void loadFullDump() throws SQLException {
        loadDump(false);
    }

    protected void saveDumpWithoutBlobs() throws SQLException {
        saveDump(true);
    }

    public void saveFullDump() throws SQLException {
        setPrefs("timestamp", "" + new java.util.Date().getTime());
        saveDump(false);
    }

    /**
     * Saves all basicTables from current database to csv files. If cvs files already exist, the contents
     * of table carddata (which contains blobs) are incrementally updated.
     * returns number of entries that have been changed since last backup
     */
    public int saveIncrementalDump() throws SQLException {
        System.out.println("save incremental dump");
        checkConnection();
        setPrefs("timestamp", "" + new java.util.Date().getTime());

        long lastChange = H2Database.readChangeDateFromDump(dumpDir);

        con.setAutoCommit(false);
        // todo: assign id to ensure that the same database is used
        saveDumpWithoutBlobs(); // save current db without the large tables with blobs

        // read all csv including the blobs from previous dump into temporary database
        H2Database localTmpDB = (H2Database) SQLDatabase.connect("jdbc:h2:mem:", "", "");
        localTmpDB.setDumpDirectory(dumpDir);
        localTmpDB.loadFullDump();

        int modified = 0; // number of updated or new entries
        for (Language lang : Globals.getSelectedLanguages()) {
            Globals.setLanguage(lang);

            // check data for changed cards -> update
            Statement stat = createStatement();
            ResultSet rs = stat.executeQuery("SELECT card_id, image, lastChange FROM " + IMAGE_TABLE + " WHERE lastChange > " + lastChange);
            while (rs.next()) {
                // load data
                int cardID = rs.getInt(1);
                Blob blob = rs.getBlob(2);
                long change = rs.getLong(3);

                // save data
                String incCmd = getReplaceStatement() + " INTO " + IMAGE_TABLE + "(card_id, image, lastChange) VALUES (?,?,?)";
                PreparedStatement ps = localTmpDB.con.prepareStatement(incCmd);
                ps.setInt(1, cardID);
                ps.setBlob(2, blob);
                ps.setLong(3, change);
                ps.executeUpdate();

                modified++;
                System.out.println("----found modified card data entry " + cardID + " " + change);
            }

            rs = stat.executeQuery("SELECT card_id, audio, lastChange FROM " + AUDIO_TABLE + " WHERE lastChange > " + lastChange);
            while (rs.next()) {
                // load data
                int cardID = rs.getInt(1);
                byte[] audio = rs.getBytes(2);
                long change = rs.getLong(3);

                // save data
                String incCmd = getReplaceStatement() + " INTO " + AUDIO_TABLE + "(card_id, audio, lastChange) VALUES (?,?,?)";
                PreparedStatement ps = localTmpDB.con.prepareStatement(incCmd);
                ps.setInt(1, cardID);
                InputStream stream = new ByteArrayInputStream(audio);
                ps.setBinaryStream(2, stream, audio.length);
                ps.setLong(3, change);
                ps.executeUpdate();

                modified++;
                System.out.println("----found modified card data entry " + cardID + " " + change);
            }
            stat.close();
        }

        localTmpDB.saveFullDump();
        con.setAutoCommit(true);

        return modified;
    }

    private boolean tablesExist() throws SQLException {
        try {
            Set<String> tableset = new HashSet<String>();
            String cmd = "show tables";
            Statement stat = createStatement();
            ResultSet rs = stat.executeQuery(cmd);
            while (rs.next()) {
                tableset.add(rs.getString(1).toLowerCase());
            }
            for (String table : allTables.keySet()) {
                if (!tableset.contains(table.toLowerCase())) {
                    //System.out.println("missing table "+table + tableset);
                    return false;
                }
            }
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    protected void deleteTables() {
        try {
            Statement stat = createStatement();
            for (String table : allTables.keySet()) {
                String query = "drop table if exists " + table;
                stat.execute(query);
            }
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * delete lessons from ERROR_CRAM_CATEGORY which are older than one month
     */
    private void deleteOldEntries() throws SQLException {
        checkConnection();
        long msecsDay = 1000 * 60 * 60 * 24;
        Date lastMonth = new Date(0);
        lastMonth.setTime(new java.util.Date().getTime() - 30 * msecsDay);
        try {
            String cmd = "delete from " + LNAMES_TABLE + " where category= ? and creation_date < ? ";
            PreparedStatement ps = con.prepareStatement(cmd);
            ps.setString(1, Lesson.ERROR_CRAM_CATEGORY);
            ps.setDate(2, lastMonth);
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateDB() throws SQLException {
        deleteOldEntries();

        // check if any update is required
        int oldVersion = Globals.getIntPref("DatabaseVersion", Integer.parseInt(databaseVersion));
        //System.out.println("check updating database... test "+databaseVersion + " "+oldVersion);

        if (oldVersion >= Integer.parseInt(databaseVersion)) {
            return; // no update required
        }
        System.out.println("updating database from databaseVersion " + oldVersion + " to databaseVersion " + databaseVersion);
        try {
            //Statement stat = createStatement();

            createTables(); // create table cardbox_back

            //stat.close();
            // ------ set new database databaseVersion after having successfully updated the tableNames ---------
            setPrefs("DatabaseVersion", databaseVersion);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("answerFinished updating database");
    }

    //====================================================================================================
    // utility methods
    //====================================================================================================

    public void createTables() {
        if (con == null) throw new RuntimeException("createTables: connection failed");
        try {
            if ( tablesExist() ) return; // nothing to do
            Statement stat = createStatement();
            for (String tablename : allTables.keySet()) {
                String columns = allTables.get(tablename);

                String cmd = "CREATE TABLE IF NOT EXISTS %s ( %s )";
                cmd = String.format(cmd, tablename, columns);
                stat.execute(cmd);
            }
            stat.close();
        } catch (SQLException e) {
            System.err.println("error creating tables");
            e.printStackTrace();
        }
    }

    private boolean entryExists(String query) throws SQLException {
        Statement stat = createStatement();
        ResultSet rs = stat.executeQuery(query);
        return rs.next();
    }

    private boolean entryExists(PreparedStatement stat) {
        boolean exists = false;
        try {
            ResultSet rs = stat.executeQuery();
            exists = rs.next();
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return exists;
    }

    /**
     * @see Globals#getIntPref, Globals.getStringPref...
     */
    public String getPrefs(String key) {
        String value = null;

        try {
            Statement stat = createStatement();
            ResultSet rs = stat.executeQuery(String.format("SELECT value FROM " + PREF_TABLE + " WHERE name='%s'", key));
            if (rs.next()) {
                value = rs.getString(1);
            }
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * @see Globals#getStringPref
     */
    public void setPrefs(String key, String value) {
        String cmd = String.format("DELETE FROM " + PREF_TABLE + " WHERE name='%s'", key);
        executeCommand(cmd);
        cmd = String.format("INSERT INTO " + PREF_TABLE + "(name,value) VALUES ('%s','%s')", key, value);
        executeCommand(cmd);
    }

    /**
     * returns the autoincrement value from recently executed statement
     */
    private int getAutoIncrement(Statement stat) throws SQLException {
        ResultSet rs = stat.getGeneratedKeys();
        rs.next();
        return rs.getInt(1);
    }

    private void executeCommand(String command) {
        try {
            Statement stat = createStatement();
            stat.executeUpdate(command);
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    protected void deleteTableContents(String table) {
        try {
            Statement stat = createStatement();
            stat.executeUpdate("delete from " + table);
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void deleteTableContents() {
        for (String table : allTables.keySet()) {
            deleteTableContents(table);
        }
    }


    //====================================================================================================
    // logging methods
    //====================================================================================================

    /**
     * writes log to database, @see LogEntry.write()
     */
    public void writeLog(int logType, String message) {
        Date today = new Date(new java.util.Date().getTime());
        try {
            checkConnection();
            String cmd = "INSERT into " + LOG_TABLE + "(logType, log_date, logMessage) VALUES (?,?,?)";
            PreparedStatement ps = con.prepareStatement(cmd);
            ps.setInt(1, logType);
            ps.setDate(2, today);
            ps.setString(3, message);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<LogEntry> readLog(int days) {
        ArrayList<LogEntry> logs = new ArrayList<LogEntry>();

        long msecsDay = 1000 * 60 * 60 * 24;
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR_OF_DAY, 0);
        java.sql.Date today = new Date(now.getTime().getTime() - msecsDay * days);

        try {
            checkConnection();
            String cmd = "SELECT log_date, logMessage, logType FROM " + LOG_TABLE + " where log_date > ? ";
            PreparedStatement ps = con.prepareStatement(cmd);
            ps.setDate(1, today);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Date date = rs.getDate(1);
                String logMessage = rs.getString(2);
                int logType = rs.getInt(3);
                logs.add(new LogEntry(logType, date, logMessage));
                //System.out.println(date+" "+logMessage+" "+questions+" "+success);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return logs;
    }

    //====================================================================================================
    //  lesson methods
    //====================================================================================================

    /**
     * returns list of all categeries from the database
     */
    public ArrayList<String> getCategoryNames() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        Statement stat = createStatement();
        ResultSet rs = stat.executeQuery("SELECT distinct category FROM " + LNAMES_TABLE + "");
        while (rs.next()) {
            String name = rs.getString("category");
            if (name.equals(Lesson.ERROR_BOX_CATEGORY)) {
                name = Lesson.ERROR_BOX_CATEGORY_LABEL;
            } else if (name.equals(Lesson.ERROR_CRAM_CATEGORY)) {
                name = Lesson.ERROR_CRAM_CATEGORY_LABEL;
            }
            list.add(name);
        }
        stat.close();

        return list;
    }

    /**
     * returns a list of the names of all lessons in the given categorz
     */
    public ArrayList<Lesson> getAvailableLessons(String category) {
        if (category.equals(Lesson.ERROR_CRAM_CATEGORY_LABEL)) {
            category = Lesson.ERROR_CRAM_CATEGORY;
        }

        ArrayList<Lesson> list = new ArrayList<Lesson>();

        try {
            Statement stat = createStatement();
            ResultSet rs = stat.executeQuery(String.format("SELECT name, lesson_id, creation_date FROM " + LNAMES_TABLE + " where category = '%s' ORDER BY creation_date", category));
            while (rs.next()) {
                String name = rs.getString("name");
                int id = rs.getInt("lesson_id");
                java.util.Date date = rs.getDate("creation_date");
                Lesson lesson = new Lesson(name, category, id, date);
                list.add(lesson);
            }
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public boolean changeCard(Card card, String front, String back, BufferedImage image, byte[] audio) {
        boolean ok = true;
        try {
            checkConnection();
            String cmd = "UPDATE " + CARDS_TABLE + " SET front=?, back=? WHERE card_id = ?";
            PreparedStatement ps = con.prepareStatement(cmd);
            ps.setString(1, front);
            ps.setString(2, back);
            ps.setInt(3, card.getDatabaseID());
            ps.executeUpdate();

            card.setFront(front);
            card.setBack(back);
            card.setBufferedImage(image);
            card.setAudio(audio);

            updateImage(card, image);
            updateAudio(card, audio);
            saveToCache(card, true);
        } catch (SQLException e) {
            e.printStackTrace();
            ok = false;
        }

        return ok;
    }

    /**
     * sets lesson id and creation_date
     */
    public void loadLessonInfo(Lesson lesson) {

        try {
            Statement stat = createStatement();
            ResultSet rs = stat.executeQuery(String.format("SELECT lesson_id, creation_date FROM " + LNAMES_TABLE + " WHERE name='%s' AND category='%s'", lesson.getName(), lesson.getCategory()));
            if (rs.next()) {
                int lessonID = rs.getInt(1);
                java.util.Date date = rs.getDate(2);
                lesson.setID(lessonID);
                lesson.setCreationDate(date);
            }
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Card> loadLesson(int lessonID) {
        return loadLesson(lessonID, true);
    }

    public ArrayList<Card> loadLesson(int lessonID, boolean loadMultimedia) {
        ArrayList<Card> cards = new ArrayList<Card>();

        long time = new java.util.Date().getTime();
        try {
            con.setAutoCommit(false);

            Statement stat = createStatement();
            String query = "SELECT %s FROM " + CARDS_TABLE + ", " + LESSONS_TABLE + " WHERE " + LESSONS_TABLE + ".lesson_id=%d AND " + LESSONS_TABLE + ".card_id=" + CARDS_TABLE + ".card_id";
            ResultSet rs = stat.executeQuery(String.format(query, CARD_COLUMNS, lessonID));
            while (rs.next()) {
                Card card = readCard(rs);
                cards.add(card);
            }
            rs.close();
            stat.close();

            con.commit();
            con.setAutoCommit(true);

            long diff = new java.util.Date().getTime() - time;
            System.out.println("load time: " + diff);
            long l = new java.util.Date().getTime();

            if (loadMultimedia) {
                readMultimediaData(cards);
                long d = new java.util.Date().getTime() - l;
                System.out.println("multmed " + d);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        long s = new java.util.Date().getTime();
        addSynonyms(cards);
        long d = new java.util.Date().getTime() - s;
        System.out.println("syn" + d);

        return cards;
    }

    protected Card loadCard(int cardID) {
        Card card = null;
        try {
            Statement stat = createStatement();
            String query = "SELECT %s FROM " + CARDS_TABLE + " WHERE card_id=%d";
            ResultSet rs = stat.executeQuery(String.format(query, CARD_COLUMNS, cardID));
            while (rs.next()) {
                card = readCard(rs);
            }
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return card;
    }

    /**
     * deletes the given card and all corresponding entries in "+LESSONS_TABLE+" and cardbox
     *
     * @param card
     */
    public void deleteCard(Card card) {
        try {
            Statement stat = createStatement();
            stat.executeUpdate(String.format("DELETE FROM " + LESSONS_TABLE + " WHERE card_id='%d'", card.getDatabaseID()));
            stat.executeUpdate(String.format("DELETE FROM " + CARDBOX_TABLE + " WHERE card_id='%d'", card.getDatabaseID()));
            stat.executeUpdate(String.format("DELETE FROM " + CARDBOXBACK_TABLE + " WHERE card_id='%d'", card.getDatabaseID()));
            stat.executeUpdate(String.format("DELETE FROM " + AUDIO_TABLE + " WHERE card_id='%d'", card.getDatabaseID()));
            stat.executeUpdate(String.format("DELETE FROM " + IMAGE_TABLE + " WHERE card_id='%d'", card.getDatabaseID()));
            stat.executeUpdate(String.format("DELETE FROM " + CARDS_TABLE + " WHERE card_id='%d'", card.getDatabaseID()));
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteCardFromBox(Card card) {
        try {
            Statement stat = createStatement();
            stat.executeUpdate(String.format("DELETE FROM " + CARDBOX_TABLE + " WHERE card_id='%d'", card.getDatabaseID()));
            stat.executeUpdate(String.format("DELETE FROM " + CARDBOXBACK_TABLE + " WHERE card_id='%d'", card.getDatabaseID()));
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteCardFromLesson(Card card, Lesson lesson) {
        try {
            Statement stat = createStatement();
            stat.executeUpdate(String.format("DELETE FROM " + LESSONS_TABLE + " WHERE card_id='%d' and lesson_id='%d'",
                    card.getDatabaseID(), lesson.getID()));
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * deletes the given lesson, but not the cards that the lesson contains
     */
    public void deleteLesson(Lesson lesson) {
        try {
            Statement stat = createStatement();
            stat.executeUpdate(String.format("DELETE FROM " + LNAMES_TABLE + " WHERE lesson_id='%d'", lesson.getID()));
            stat.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * reads result from given ResultSet into Card
     * The corresponding query should use CARDQUERY
     */
    protected Card readCard(ResultSet rs) throws SQLException {
        Card card = new Card();
        card.setDatabaseID(rs.getInt(1));
        card.setFront(rs.getString(2));
        card.setBack(rs.getString(3));

        return card;
    }

    public void readMultimediaData(Card card) {
        try {
            ArrayList<Card> cards = new ArrayList<>();
            cards.add(card);
            readMultimediaData(cards);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * reads image and sound for each card of the given list
     *
     * @param cards
     * @throws SQLException
     */
    protected void readMultimediaData(ArrayList<Card> cards) throws SQLException {
        HashMap<Integer, Card> cardHash = new HashMap<>();
        ArrayList<String> imageIds = new ArrayList<>();
        ArrayList<String> soundIds = new ArrayList<>();
        for (Card card : cards) {
            // read image and audio data from cache, if available
            loadBlobsFromCache(card);

            if (card.getImage() == null) { // if not available in cache
                imageIds.add("" + card.getDatabaseID());
            }
            if (card.getAudio() == null) {
                soundIds.add("" + card.getDatabaseID());
            }
            cardHash.put(card.getDatabaseID(), card);
        }

        //==== read image data from db
        if (imageIds.size() > 0) {
            String cardStr = String.join(",", imageIds);
            String query = "SELECT card_id, image, lastChange FROM " + IMAGE_TABLE + " WHERE card_id IN (" + cardStr + ")";
            Statement stat = createStatement();
            ResultSet rsdata = stat.executeQuery(query);

            while (rsdata.next()) // image exists
            {
                // image exists => convert to buffered image
                int id = rsdata.getInt(1);
                Blob blob = rsdata.getBlob(2);
                if (blob != null) {
                    try {
                        InputStream in = blob.getBinaryStream();
                        BufferedImage bufImg = ImageIO.read(in);
                        in.close();
                        cardHash.get(id).setBufferedImage(bufImg);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            rsdata.close();
            stat.close();
        }
        if (soundIds.size() > 0) {
            //==== read audio data from db
            String cardStr = String.join(",", soundIds);
            String query = "SELECT card_id, audio, lastChange FROM " + AUDIO_TABLE + " WHERE card_id IN (" + cardStr + ")";
            Statement stat = createStatement();
            ResultSet rsdata = stat.executeQuery(query);

            while (rsdata.next()) // audio data exists
            {
                int id = rsdata.getInt(1);
                byte[] bytes = rsdata.getBytes(2);
                cardHash.get(id).setAudio(bytes);
            }
            rsdata.close();
            stat.close();

            for (Card card : cards) {
                saveToCache(card, false);
            }
        }
    }

    /**
     * retreives all synonyms from database, return empty list, if no entry is found
     */
    public ArrayList<Card> searchSynonym(String front) {
        ArrayList<Card> scards = new ArrayList<>();
        try {
            checkConnection();
            String stmt = "SELECT * FROM " + CARDS_TABLE + " WHERE front=?";
            PreparedStatement ps = con.prepareStatement(stmt);
            ps.setString(1, front);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                scards.add(readCard(rs));
            }
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return scards;
    }

    public void unsetSynonyms() {
        allSynonyms = null;
    }

    /**
     * adds all available synonyms of the database to each card of the list
     * todo optimize, save results into table
     */
    private void addSynonyms(ArrayList<Card> cards) {
        if (allSynonyms == null) // searches for all available synonyms in the database
        {
            HashMap<String, ArrayList<String>> hash = new HashMap<>();
            try {
                String stmt = "select * from " + CARDS_TABLE + " where front in (select front from " + CARDS_TABLE + " group by front having count(front)>1)";
                Statement stat = createStatement();
                ResultSet rs = stat.executeQuery(stmt);
                while (rs.next()) {
                    Card card = readCard(rs);
                    ArrayList<String> list = hash.get(card.getFront());
                    if (list == null) {
                        list = new ArrayList<String>();
                    }
                    list.add(card.getBack());
                    hash.put(card.getFront(), list);
                }
                stat.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            allSynonyms = hash;
            //for (String c : allSynonyms.keySet()) {System.out.println("syn--- "+c);}
        }

        for (Card card : cards) {
            ArrayList<String> syn = allSynonyms.get(card.getFront());
            card.setSynonyms(syn);
        }
    }

    /**
     * searches for the card with the given front or back text
     */
    public ArrayList<Card> searchCards(String text, boolean exactMatch) {
        ArrayList<Card> list = new ArrayList<Card>();
        try {
            checkConnection();
            String compare = exactMatch ? "=" : "LIKE";
            int limit = this instanceof H2Database ? 500 : 50;
            String query = "SELECT * FROM " + CARDS_TABLE + " WHERE front %s ? OR back %s ? LIMIT " + limit;
            query = String.format(query, compare, compare);
            PreparedStatement stat1 = con.prepareStatement(query);
            if (exactMatch) {
                stat1.setString(1, text);
                stat1.setString(2, text);
            } else {
                stat1.setString(1, "%" + text + "%");
                stat1.setString(2, "%" + text + "%");
            }

            ResultSet rs = stat1.executeQuery();

            Statement stat2 = createStatement();
            while (rs.next()) {
                Card card = readCard(rs);
                list.add(card);

                String query2 = String.format("SELECT box, insert_date FROM " + CARDBOX_TABLE + " WHERE card_id='%d'", card.getDatabaseID());
                ResultSet r2 = stat2.executeQuery(query2);
                int box = Card.BOX_NOT_INSERTED;
                if (r2.next()) box = r2.getInt(1);
                card.setBoxNr(box);

                r2 = stat2.executeQuery(String.format("SELECT name, category, " + LESSONS_TABLE + ".lesson_id, creation_date FROM " + LESSONS_TABLE + "," + LNAMES_TABLE + "  WHERE " + LESSONS_TABLE + ".card_id='%d' AND " + LESSONS_TABLE + ".lesson_id=" + LNAMES_TABLE + ".lesson_id", card.getDatabaseID()));
                ArrayList<Lesson> lessons = new ArrayList<Lesson>();
                while (r2.next()) {
                    Lesson lesson = new Lesson(r2.getString(1), r2.getString(2), r2.getInt(3), r2.getDate(4));
                    lessons.add(lesson);
                    card.setLessons(lessons);
                }
            }
            stat1.close();
            stat2.close();

            readMultimediaData(list);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public int getID(Card card) {
        int cardID = -1;
        try {
            String cmd = "SELECT card_id FROM " + CARDS_TABLE + " WHERE front=? and back=?";
            PreparedStatement ps = con.prepareStatement(cmd);
            ps.setString(1, card.getFront());
            ps.setString(2, card.getBack());
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                cardID = rs.getInt(1);
            ps.close();
            card.setDatabaseID(cardID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cardID;
    }

    public boolean cardExists(String front, String back) {
        boolean ok = false;
        try {
            String cmd = "SELECT front FROM " + CARDS_TABLE + " WHERE front=? AND back=?";
            PreparedStatement ps = con.prepareStatement(cmd);
            ps.setString(1, front);
            ps.setString(2, back);
            ok = entryExists(ps);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ok;
    }

    public boolean lessonExists(Lesson lesson) throws SQLException {
        checkConnection();
        String cmd = "SELECT lesson_id FROM " + LNAMES_TABLE + " WHERE name=? AND category=?";
        PreparedStatement ps = con.prepareStatement(cmd);
        ps.setString(1, lesson.getName());
        ps.setString(2, lesson.getCategory());
        return entryExists(ps);
    }

    public boolean cardInLessonExists(int lessonID, int cardID) throws SQLException {
        String cmd = "SELECT lesson_id FROM " + LESSONS_TABLE + " WHERE lesson_id=? AND card_id=?";
        PreparedStatement ps = con.prepareStatement(cmd);
        ps.setInt(1, lessonID);
        ps.setInt(2, cardID);
        return entryExists(ps);
    }

    /**
     * adds the list of card as new lesson to the database
     *
     * @param lesson the lesson that should be added to the database
     */
    public void addLesson(Lesson lesson) {
        try {
            checkConnection();
            if (lessonExists(lesson)) throw new RuntimeException("Lesson already exists");
            Date today = new Date(new java.util.Date().getTime());

            String cmd = "INSERT INTO " + LNAMES_TABLE + "(name,category,creation_date) VALUES (?,?,?)";
            PreparedStatement ps = con.prepareStatement(cmd, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, lesson.getName());
            ps.setString(2, lesson.getCategory());
            ps.setDate(3, today);
            ps.executeUpdate();
            int lessonID = getAutoIncrement(ps);

            lesson.setID(lessonID);
            lesson.setCreationDate(today);

            if (lesson.getCards() != null) {
                for (Card card : lesson.getCards()) {
                    int cardID = getID(card);
                    if (cardID < 0) {
                        addCard(card);
                        cardID = card.getDatabaseID();
                    }
                    addCardToLesson(cardID, lessonID);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * change oldLesson to name and category of newLesson
     */
    public void renameLesson(Lesson oldLesson, Lesson newLesson) throws Exception {
        if (newLesson == null || oldLesson == null || lessonExists(newLesson)) {
            throw new Exception("Lektion existiert bereits oder Name ist leer.");
        }
        Statement stat = createStatement();
        stat.execute(String.format("UPDATE " + LNAMES_TABLE + " set name='%s', category='%s' WHERE lesson_id='%d'",
                newLesson.getName(), newLesson.getCategory(), oldLesson.getID()));
        stat.close();
    }

    /**
     * adds a card to lesson with the given id
     */
    public void addCardToLesson(int cardID, int lessonID) {
        if (cardID < 0) throw new RuntimeException("error: tried to insert card with negative id");
        if (lessonID < 0) throw new RuntimeException("error: tried to insert card with negative lesson id");
        try {
            Statement stat = createStatement();
            stat.execute(String.format("INSERT INTO " + LESSONS_TABLE + "(lesson_id, card_id) VALUES (%d,%d)", lessonID, cardID));
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * If a card with the same front and back exists in database, the image and audio data is updated if it's
     * not set in the database
     */
    public void mergeCard(Card card) {
        try {
            String stmt = "SELECT %s FROM " + CARDS_TABLE + " WHERE front=? AND back=?";
            stmt = String.format(stmt, CARD_COLUMNS);
            PreparedStatement ps = con.prepareStatement(stmt, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, card.getFront());
            ps.setString(2, card.getBack());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Card orig = readCard(rs);
                readMultimediaData(orig);
                if (orig.getImage() != null)
                    card.setBufferedImage(orig.getImage());
                if (orig.getAudio() != null)
                    card.setAudio(orig.getAudio());
                card.setDatabaseID(orig.getDatabaseID());
                this.changeCard(orig, card.getFront(), card.getBack(), card.getImage(), card.getAudio());
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * adds a card to database
     * returns false, if the card already exists in databaes
     */
    public boolean addCard(Card card) {
        boolean added = false;
        try {
            checkConnection();
            if (!cardExists(card.getFront(), card.getBack())) {
                String stmt = "insert into " + CARDS_TABLE + "(front,back) values(?,?)";
                PreparedStatement ps = con.prepareStatement(stmt, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, card.getFront());
                ps.setString(2, card.getBack());
                //System.out.println("stmt: "+stmt);
                ps.executeUpdate();
                int cardID = getAutoIncrement(ps);

                card.setDatabaseID(cardID);

                if (card.getImage() != null) // save image, if available
                {
                    updateImage(card, card.getImage());
                }
                if (card.getAudio() != null) {
                    updateAudio(card, card.getAudio());
                }
                added = true;
            } else {
                System.err.println("not added but merged: " + card.getFront() + " " + card.getBack());
                mergeCard(card);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return added;
    }

    private void updateImage(Card card, BufferedImage image) throws SQLException {
        int cardID = card.getDatabaseID();
        long time = new java.util.Date().getTime();

        checkConnection();
        if (image != null) // save image, if available
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                ImageIO.write(image, "jpg", baos); // convert to jpg to reduce required memory
                baos.flush();
                byte[] imageInByte = baos.toByteArray();
                baos.close();
                InputStream stream = new ByteArrayInputStream(imageInByte);

                String picture = getReplaceStatement() + " INTO " + IMAGE_TABLE + "(card_id, image, lastChange) VALUES (?,?,?)";
                PreparedStatement ps = con.prepareStatement(picture);
                ps.setInt(1, cardID);
                ps.setBinaryStream(2, stream, imageInByte.length);
                ps.setLong(3, time);
                ps.executeUpdate();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else { // image == null
            String sql = "DELETE FROM " + IMAGE_TABLE + " WHERE card_id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, cardID);
            ps.executeUpdate();
        }
    }

    /**
     * updates the audio data of the card with the given card_id
     */
    private void updateAudio(Card card, byte[] audio) throws SQLException {
        int cardID = card.getDatabaseID();
        long time = new java.util.Date().getTime();

        checkConnection();
        if (audio != null) {
            InputStream stream = new ByteArrayInputStream(audio);
            String sound = getReplaceStatement() + " INTO " + AUDIO_TABLE + "(card_id, audio, lastChange) VALUES (?,?,?)";
            PreparedStatement ps = con.prepareStatement(sound);
            ps.setInt(1, cardID);
            ps.setBinaryStream(2, stream, audio.length);
            ps.setLong(3, time);
            ps.executeUpdate();
        } else {
            String sql = "DELETE FROM " + AUDIO_TABLE + " WHERE card_id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, cardID);
            ps.executeUpdate();
        }
    }


    //====================================================================================================
    // card box related methods
    //====================================================================================================

    /**
     * returns all lessons of the given category whose cards aren't (completely) put into the cardbox
     *
     * @see #lessonToBox
     */
    public ArrayList<Lesson> getUnsavedBoxLessons(String category, Direction direction) {
        ArrayList<Integer> lessonIDs = new ArrayList<Integer>();
        ArrayList<Lesson> lessons = new ArrayList<Lesson>();
        try {
            Statement stat = createStatement();
            ResultSet rs = stat.executeQuery("SELECT distinct lesson_id FROM " + LESSONS_TABLE + " WHERE card_id Not In(SELECT card_id FROM " + getBoxName(direction) + ")");
            while (rs.next()) {
                lessonIDs.add(rs.getInt(1));
            }

            for (int lessonID : lessonIDs) {
                String query = String.format("SELECT name, category, creation_date from " + LNAMES_TABLE + " where lesson_id=%s and category='%s'", lessonID, category);
                rs = stat.executeQuery(query);
                if (rs.next())
                    lessons.add(new Lesson(rs.getString(1), rs.getString(2), lessonID, rs.getDate(3)));
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return lessons;
    }

    private String getBoxName() {
        return Globals.getDirection() == Direction.FRONT ? CARDBOX_TABLE : CARDBOXBACK_TABLE;
    }

    private String getBoxName(Direction direction) {
        return direction == Direction.FRONT ? CARDBOX_TABLE : CARDBOXBACK_TABLE;
    }

    public int getBoxNr(Card card) {
        int boxNr = Card.BOX_NOT_INSERTED;
        if (card.getBoxNr() == Card.BOX_NOT_INSERTED) {
            if (getID(card) >= 0) {
                try {
                    PreparedStatement ps = con.prepareStatement("SELECT box FROM " + getBoxName() + " WHERE card_id = ? ");
                    ps.setInt(1, card.getDatabaseID());
                    ResultSet rs = ps.executeQuery();
                    rs.next();
                    boxNr = rs.getInt(1);
                } catch (SQLException e) {
                    System.err.println("Database:getBoxNr: " + e);
                }
            }
        }
        return boxNr;
    }

    /**
     * return number of cards to be checked today
     */
    public int getBoxCount(Direction direction) {
        int count = 0;
        Date today = new Date(new java.util.Date().getTime());
        long msecsDay = 1000 * 60 * 60 * 24;

        try {
            checkConnection();
            con.setAutoCommit(false);
            for (int boxNr = 0; boxNr < Globals.getBoxDates(direction).length; boxNr++) {
                Date activeDate = new Date(0);
                activeDate.setTime(today.getTime() - Globals.getBoxDates(direction)[boxNr] * msecsDay);

                PreparedStatement ps = con.prepareStatement("SELECT count(*) FROM " + getBoxName(direction) + "," + CARDS_TABLE +
                        " WHERE box = ? AND insert_date <= ? AND " + getBoxName(direction) + ".card_id=" + CARDS_TABLE + ".card_id");
                ps.setInt(1, boxNr);
                ps.setDate(2, activeDate);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    count += rs.getInt(1);
                }
                ps.close();
            } // all boxes
            con.commit();
            con.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }


    public ArrayList<Card> getBoxList(Direction direction) throws SQLException {
        Date today = new Date(new java.util.Date().getTime());

        ArrayList<Card> cards = new ArrayList<>();
        long msecsDay = 1000 * 60 * 60 * 24;

        long time = new java.util.Date().getTime();
        //todo slow
        checkConnection();
        con.setAutoCommit(false);
        for (int boxNr = 0; boxNr < Globals.getBoxDates(direction).length; boxNr++) {
            ArrayList<Card> box = new ArrayList<>();
            Date activeDate = new Date(0);
            activeDate.setTime(today.getTime() - Globals.getBoxDates(direction)[boxNr] * msecsDay);

            PreparedStatement ps = con.prepareStatement("SELECT " + CARD_COLUMNS + " FROM " + getBoxName(direction) + "," + CARDS_TABLE +
                    " WHERE box = ? AND insert_date <= ? AND " + getBoxName(direction) + ".card_id=" + CARDS_TABLE + ".card_id");
            ps.setInt(1, boxNr);
            ps.setDate(2, activeDate);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Card card = readCard(rs);
                card.setBoxNr(boxNr);
                box.add(card);
            }
            ps.close();

            long seed = System.nanoTime();
            Collections.shuffle(box, new Random(seed));
            cards.addAll(box);
        } // all boxes
        con.commit();
        con.setAutoCommit(true);

        readMultimediaData(cards);
        addSynonyms(cards);
        long diff = new java.util.Date().getTime() - time;
        System.out.println("time: " + diff); //todo
        return cards;
    }


    /**
     * @return list with cards of the 1. box that would be asked the next day
     */
    public ArrayList<Card> getBoxErrors(Direction direction, boolean loadMultimediaContents) {
        ArrayList<Card> list = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        Date today = new Date(cal.getTime().getTime());

        try { // todo use only one select
            checkConnection();
            PreparedStatement ps = con.prepareStatement("SELECT card_id FROM " + getBoxName(direction) + " WHERE box = 0 AND insert_date >= ?");
            ps.setDate(1, today);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int cardID = rs.getInt(1);
                String query = String.format("SELECT %s FROM " + CARDS_TABLE + " WHERE card_id=%d", CARD_COLUMNS, cardID);
                Statement stat = createStatement();
                ResultSet rsC = stat.executeQuery(String.format(query, CARD_COLUMNS, today));
                while (rsC.next()) {
                    Card card = readCard(rsC);
                    list.add(card);
                }
                stat.close();
            }
            ps.close();

            if (loadMultimediaContents)
                readMultimediaData(list);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        addSynonyms(list);
        return list;
    }

    /*
    public void getFinished() throws SQLException
    {
        checkConnection();
        con.setAutoCommit(false);

        PreparedStatement ps = con.prepareStatement("SELECT count(*), lesson_id FROM "+LESSONS_TABLE+" GROUP BY lesson_id");
        ResultSet rs = ps.executeQuery();
        Hashtable<Integer, Integer> hash = new Hashtable<>();
        while (rs.next())
        {
            int count = rs.getInt(1);
            int lessonID = rs.getInt(2);
            hash.put(lessonID, count);
        }
        ps.close();

        ps = con.prepareStatement("SELECT  lesson_id, name, category FROM "+LNAMES_TABLE+"");
        rs = ps.executeQuery();
        Hashtable<Integer, String> nameHash = new Hashtable<>();
        while (rs.next())
        {
            int lessonID = rs.getInt(1);
            String name = rs.getString(2);
            String category = rs.getString(3);
            nameHash.put(lessonID, name+" ("+category+")");
        }
        ps.close();

        //PreparedStatement ps = con.prepareStatement("SELECT "+CARDBOX_TABLE+".card_id, "+LESSONS_TABLE+".lesson_id FROM "+CARDBOX_TABLE+", "+LESSONS_TABLE+" WHERE box = -1 AND "+CARDBOX_TABLE+".card_id = "+LESSONS_TABLE+".card_id");
        ps = con.prepareStatement("SELECT count(*), lesson_id FROM "+CARDBOX_TABLE+", "+LESSONS_TABLE+" WHERE box = -1 AND "+CARDBOX_TABLE+".card_id = "+LESSONS_TABLE+".card_id GROUP BY lesson_id");
        rs = ps.executeQuery();
        while (rs.next())
        {
            int count = rs.getInt(1);
            int lessonID = rs.getInt(2);
            System.out.println(nameHash.get(lessonID)+" "+count+"/"+hash.get(lessonID));
        }
        ps.close();

        con.setAutoCommit(true);
    } */

    public BoxStatistics getBoxStatistics(Direction direction, int days) throws SQLException {
        long today = new Date(new java.util.Date().getTime()).getTime();
        long msecsDay = 1000 * 60 * 60 * 24;

        String stmt = "select max(box) from " + getBoxName(direction);
        Statement stat = createStatement();
        ResultSet rss = stat.executeQuery(stmt);
        rss.next();
        //int boxes = rss.getInt(1) + 1; // number of boxes in the database, may have been decreases later by user settings
        //int usedBoxes = Math.max(boxes, Globals.getBoxDates(direction).length);
        int usedBoxes = Globals.getBoxDates(direction).length;

        BoxStatistics statistics = new BoxStatistics();
        statistics.cardsInBox = new int[usedBoxes];
        statistics.cardsForDay = new int[usedBoxes][days];

        checkConnection();
        con.setAutoCommit(false);
        for (int boxNr = 0; boxNr < usedBoxes; boxNr++) {
            // query number of cards in each box
            PreparedStatement ps = con.prepareStatement("SELECT count(*) FROM " + getBoxName(direction) + " WHERE box = ?");
            ps.setInt(1, boxNr);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) statistics.cardsInBox[boxNr] = rs.getInt(1);
            ps.close();

            Date activeDate = new Date(0);
            Date startDate = new Date(0);
            for (int day = 0; day < days; day++) {
                long daysToAdd = day * msecsDay;
                // box nr bigger than user configures number of boxes => box is deleted => ask now
                int daysToStayInBox = boxNr < Globals.getBoxDates(direction).length ? Globals.getBoxDates(direction)[boxNr] : 0;
                // query number of cards in each box which have to be questioned in "days" days
                activeDate.setTime(today + daysToAdd - (daysToStayInBox * msecsDay));
                ps = con.prepareStatement("SELECT count(*) FROM " + getBoxName(direction) + " WHERE box = ? AND insert_date <= ? AND insert_date > ?");
                ps.setInt(1, boxNr);
                ps.setDate(2, activeDate);
                ps.setDate(3, startDate);
                rs = ps.executeQuery();
                if (rs.next()) statistics.cardsForDay[boxNr][day] = rs.getInt(1);
                ps.close();
                startDate.setTime(activeDate.getTime());
            }
        }
        PreparedStatement ps = con.prepareStatement("SELECT count(*) FROM " + getBoxName(direction) + " WHERE box = " + Card.BOX_FINISHED);
        ResultSet rs = ps.executeQuery();
        int count = 0;
        if (rs.next()) count = rs.getInt(1);
        ps.close();
        statistics.finishedCards = count;

        // count number of cards in the default category
        String category = Globals.getDefaultCategory();
        ps = con.prepareStatement("SELECT count(distinct(card_id)) FROM " + LNAMES_TABLE + ", " + LESSONS_TABLE + " WHERE category = ? AND " + LNAMES_TABLE + ".lesson_id = " + LESSONS_TABLE + ".lesson_id");
        ps.setString(1, category);
        //SELECT name, count(*) FROM "+LNAMES_TABLE+", "+LESSONS_TABLE+" WHERE category='Klasse5'  AND "+LNAMES_TABLE+".lesson_id = "+LESSONS_TABLE+".lesson_id group by name;       ps.setString(1, category );
        rs = ps.executeQuery();
        count = 0;
        if (rs.next()) count = rs.getInt(1);
        statistics.currentCategory = count;

        con.setAutoCommit(true);

        return statistics;
    }

    /**
     * retreives information about each lesson in the given category
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<LessonStatistics> getLessonStatistics(String category, Direction direction) {
        ArrayList<LessonStatistics> lessonStat = new ArrayList<>();

        String sql = "select " + LESSONS_TABLE + ".lesson_id, name, box, count(*) from " + LNAMES_TABLE + ", " + LESSONS_TABLE + ", " + getBoxName(direction) + " ";
        sql += "where " + LESSONS_TABLE + ".card_id = " + getBoxName(direction) + ".card_id and " + LESSONS_TABLE + ".lesson_id = " + LNAMES_TABLE + ".lesson_id ";
        sql += " AND category = '" + category + "' ";
        sql += " group by box, " + LESSONS_TABLE + ".lesson_id order by lesson_id, box";

        try {
            // check data for changed cards -> update
            Statement stat = createStatement();
            ResultSet rs = stat.executeQuery(sql);

            LessonStatistics lesson = null;
            int lessonID = -1;
            while (rs.next()) {
                int id = rs.getInt(1);
                if (id != lessonID) {
                    lessonID = id;
                    lesson = new LessonStatistics();
                    lesson.countInBox = new int[Globals.getBoxDates(direction).length + 1]; // last box is boxNr -1 (=answerFinished)
                    lesson.lessonID = id;
                    lesson.lessonName = rs.getString(2);
                    lessonStat.add(lesson);
                }

                int boxNr = rs.getInt(3);
                boxNr = boxNr == Card.BOX_FINISHED ? Globals.getBoxDates(direction).length : boxNr;
                lesson.countInBox[boxNr] = rs.getInt(4);

                //System.out.print(lesson.lessonName);for (int i=0; i<7; i++) System.out.print(lesson.countInBox[i]+" "); System.out.println();
            }
            stat.close();
            System.out.println("lesson: " + lessonStat.size());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lessonStat;
    }

    /* put card back into the given box */
    public void revertCard(Card card, int boxNr) {
        try {
            java.sql.Date activeDate = new java.sql.Date(0);
            if ( boxNr >= 0 ) {
                java.sql.Date today = new java.sql.Date(new java.util.Date().getTime());
                final long msecsDay = 1000 * 60 * 60 * 24;
                int boxDate = ( Globals.getBoxDates(Globals.getDirection())[boxNr] ) - 1;
                activeDate.setTime(today.getTime() - boxDate * msecsDay);
            }
            String cmd = "UPDATE " + getBoxName() + " SET box=?, insert_date=? WHERE card_id = ?";
            PreparedStatement ps = con.prepareStatement(cmd);
            ps.setInt(1, boxNr);
            ps.setDate(2, activeDate);
            ps.setInt(3, card.getDatabaseID());
            ps.executeUpdate();
            card.setBoxNr(boxNr);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /** quick hack to check, if user has started a lesson */
    public boolean hasBoxLessonStarted() {
        try {
            checkConnection();
            java.sql.Date today = new java.sql.Date(new java.util.Date().getTime());
            String cmd = "SELECT log_date FROM " + LOG_TABLE + " where log_date = ? ";
            PreparedStatement ps = con.prepareStatement(cmd);
            ps.setDate(1, today);
            ResultSet rs = ps.executeQuery();
            return (rs.next());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Inserts the card in the 1st box if !success or in the following box if it has been sucessfully answered.
     * If the answered card is in the last box, it will be deleted from the box on success.
     */
    public void cardIsExamined(Card card, boolean success) throws SQLException {
        Date date = new Date(new java.util.Date().getTime());
        int boxNr = success ? card.getBoxNr() + 1 : 0;

        if (boxNr >= Globals.getBoxDates(Globals.getDirection()).length) {
            boxNr = Card.BOX_FINISHED;
        }

        checkConnection();
        String cmd = "UPDATE " + getBoxName() + " SET box=?, insert_date=? WHERE card_id = ?";
        PreparedStatement ps = con.prepareStatement(cmd);
        ps.setInt(1, boxNr);
        ps.setDate(2, date);
        ps.setInt(3, card.getDatabaseID());
        ps.executeUpdate();
    }

    /**
     * puts the cards from the given lesson into the card box
     */
    public void lessonToBox(Lesson lesson, Direction direction) {
        if (lesson.getID() < 0) loadLessonInfo(lesson);
        try {
            lessonToBox(lesson.getID(), direction);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void lessonToBox(int lessonID, Direction direction) throws SQLException {
        checkConnection();
        ArrayList<Card> cards = loadLesson(lessonID);

        try {
            Date date = new Date(new java.util.Date().getTime());
            for (Card card : cards) {
                boolean exists = entryExists(String.format("SELECT card_id FROM " + getBoxName(direction) + " WHERE card_id=%d", card.getDatabaseID()));

                if (!exists) {
                    PreparedStatement ps = con.prepareStatement("INSERT INTO " + getBoxName(direction) + "(card_id, box, insert_date) VALUES (?,?,?)");
                    ps.setInt(1, card.getDatabaseID());
                    ps.setInt(2, 0); // start with box number 0
                    ps.setDate(3, date);
                    ps.executeUpdate();
                } else System.out.println("existiert bereits: " + card.getFront()); // todo remove
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private final String getCacheDir()
    {
        String lang = Globals.getLanguage().getIdentifier();
        String languageDir="";
        if ( !lang.equals("english") )
        {
             languageDir = File.separator + lang;
        }
        return dumpDir + File.separator + ".." + File.separator + "cache" + languageDir;
    }

    // cache image and sound data to improve performance of remote database
    private void saveToCache(Card card, boolean overwrite) {
        if (this instanceof H2Database) return; // no cache needed for local db

        File cacheDir = new File( getCacheDir() );
        if (card.getImage() != null) {
            try {
                File cacheFile = new File(cacheDir, card.getDatabaseID() + ".jpg");
                if (overwrite || !cacheFile.exists()) {
                    if (!cacheDir.exists()) cacheDir.mkdirs();
                    ImageIO.write(card.getImage(), "jpg", cacheFile);
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        if (card.getAudio() != null && card.getAudio().length > 0) {
            try {
                File cacheFile = new File(cacheDir, card.getDatabaseID() + ".ogg");
                if (overwrite || !cacheFile.exists()) {
                    if (!cacheDir.exists()) cacheDir.mkdirs();

                    FileOutputStream out = new FileOutputStream(cacheFile);
                    out.write(card.getAudio());
                    out.close();
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    private void loadBlobsFromCache(Card card) {
        if (this instanceof H2Database) return; // no cache needed for local db

        File cacheDir = new File( getCacheDir() );
        { // image
            File cacheFile = new File(cacheDir, card.getDatabaseID() + ".jpg");
            try {
                if (cacheFile.exists()) {
                    BufferedImage image = ImageIO.read(cacheFile);
                    card.setBufferedImage(image);
                }
            } catch (Exception e) {
                System.out.println(cacheFile.getAbsolutePath() + ": " + e);
            }
        }
        { // audio ogg
            File cacheFile = new File(cacheDir, card.getDatabaseID() + ".ogg");
            try {
                if (cacheFile.exists()) {
                    byte[] data = Files.readAllBytes(cacheFile.toPath());
                    card.setAudio(data);
                }
            } catch (Exception e) {
                System.out.println(cacheFile.getAbsolutePath() + ": " + e);
            }
        }

    }

    /**
     * extracts the given zip file
     *
     * @param zipFilePath the zip file
     * @param dir         directory to extract the contents of the zip into
     */
    public static void extractZip(String zipFilePath, String dir) {
        try {
            ZipFile zipFile = new ZipFile(zipFilePath);

            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry zipEntry = entries.nextElement();
                long size = zipEntry.getSize();
                String fileEntry = dir + File.separator + zipEntry.getName();
                if (size > 0) {
                    BufferedInputStream input = new BufferedInputStream(zipFile.getInputStream(zipEntry));
                    FileOutputStream output = new FileOutputStream( fileEntry );
                    byte[] buf = new byte[1024];
                    int bytesRead;
                    while ((bytesRead = input.read(buf)) > 0) {
                        output.write(buf, 0, bytesRead);
                    }
                    input.close();
                    output.close();
                } else {
                    File createFile = new File( fileEntry );
                    try {
                        createFile.createNewFile();
                    } catch (IOException e) { System.out.println("Warning extracting empty zip entry: "+fileEntry+" "+e);}
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createZip(ArrayList<String> inputFiles, String zipFilePath) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(zipFilePath);
            ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);

            for (String inputFilePath : inputFiles) {
                File inputFile = new File(inputFilePath);
                // a ZipEntry represents a file entry in the zip archive
                // We name the ZipEntry after the original file's name
                ZipEntry zipEntry = new ZipEntry(inputFile.getName());
                zipEntry.setMethod(ZipEntry.DEFLATED);
                zipOutputStream.putNextEntry(zipEntry);

                FileInputStream fileInputStream = new FileInputStream(inputFile);
                byte[] buf = new byte[1024];
                int bytesRead;

                // Read the input file by chucks of 1024 bytes
                // and write the read bytes to the zip stream
                while ((bytesRead = fileInputStream.read(buf)) > 0) {
                    zipOutputStream.write(buf, 0, bytesRead);
                }
                zipOutputStream.closeEntry();
            }
            zipOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* only for testing purpose: change the insertion dates of the cards */
    public void testChangeCardDates() throws SQLException {
        System.out.println("called test method testChangeCardDates");
        con.setAutoCommit(false);
        java.sql.Date today = new java.sql.Date(new java.util.Date().getTime());
        Direction direction = Direction.FRONT;
        for (int boxNr = 0; boxNr < Globals.getBoxDates(direction).length; boxNr++) {
            ArrayList<Card> box = new ArrayList<>();
            PreparedStatement ps = con.prepareStatement("SELECT " + CARD_COLUMNS + " FROM " + getBoxName() + "," + CARDS_TABLE +
                    " WHERE box = ? AND " + getBoxName() + ".card_id=" + CARDS_TABLE + ".card_id");
            ps.setInt(1, boxNr);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Card card = readCard(rs);
                card.setBoxNr(boxNr);
                box.add(card);
            }
            ps.close();
            for ( Card card : box ) // change card date in box
            {
                Date activeDate = new Date(0);
                final long msecsDay = 1000 * 60 * 60 * 24;
                activeDate.setTime(today.getTime() - (Globals.getBoxDates(direction)[boxNr]) * msecsDay);
                String cmd = "UPDATE " + getBoxName() + " SET insert_date=? WHERE card_id = ?";
                ps = con.prepareStatement(cmd);
                ps.setDate(1, activeDate);
                ps.setInt(2, card.getDatabaseID());
                ps.executeUpdate();
            }
        } // all boxes
        con.commit();
        con.setAutoCommit(true);
    }

}

