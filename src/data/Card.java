package data;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;


public class Card 
{
	public static final int BOX_FINISHED = -1;
	public static final int BOX_NOT_INSERTED = -2;

	// database values for table "cards"
	private String front;
	private String back;
	private BufferedImage image;
	private byte [] audio;

	// generated values (may be empty)s
	private int databaseID = -1;
	private int boxNr = BOX_NOT_INSERTED; // optional value: database values for table "cardbox", starts with 0
    private ArrayList<Lesson> lessons;  // lessons which contain this card

	private ArrayList<String> synonyms; // if front exists more than one time in the database

	public Card() {
		front = null;
		back = null;
		image = null;
		audio = null;
		boxNr = BOX_NOT_INSERTED;
	}
    public Card( String front, String back ) {
        this.front = front;
        this.back = back;
    }

	public int getBoxNr() {
		return boxNr;
	}
	public void setBoxNr(int boxNr) {
		this.boxNr = boxNr;
	}
	public int getDatabaseID() {
		return databaseID;
	}
	public void setDatabaseID(int databaseID) {
		this.databaseID = databaseID;
	}
	public BufferedImage getImage() {
		return image;
	}
	public void setBufferedImage(BufferedImage image) {
		this.image = image;
	}
	public byte[] getAudio() {
		return audio;
	}
	public void setAudio(byte[] raw) {
		audio = raw;
	}
	public boolean isValid() {
		return front != null && back != null && !front.isEmpty() && !back.isEmpty();
	}

	public String getFront() {
		return front;
	}
	public String getBack() {
		return back;
	}
	public String getQuestion() {
		return Globals.getDirection() == Direction.FRONT ? front : back;
	}
	public String getAnswer() {
		return Globals.getDirection() == Direction.FRONT ?  back : front;
	}
	public void setFront(String front) {
		if (front != null) front = front.trim();
		this.front = front;
	}
	public void setBack(String back) {
		if (back != null) back = back.trim();
		this.back = back;
	}


    public ArrayList<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(ArrayList<Lesson> lessons) {
        this.lessons = lessons;
    }

    public ArrayList<String> getSynonyms() {
		if (synonyms == null) synonyms = new ArrayList<String>();
		return synonyms;
	}

	public void setSynonyms(ArrayList<String> synonyms) {
		this.synonyms = synonyms;
	}

	public String toString() {
		String str = front + ":" + back;
		return str;
	}

	@Override
	public boolean equals( Object o ) {
		boolean same;
		if (! (o instanceof Card) ) return false;
		Card c = (Card) o;
		same = front.equals(c.front) && back.equals(c.back);
		if (same) same = ( image == c.image ) || ( image != null && image.equals(c.image) );
		if (same) same = ( audio == c.audio ) || ( Arrays.equals(audio, c.audio) );

		return same;
	}
}
