package gui;
import data.*;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.fxml.*;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LessonToBoxDialog extends Stage implements Initializable
{
    @FXML
    Label directionLabel;
    @FXML
    private Pane lessonSelection;
    @FXML
    private LessonSelection lessonSelectionController; // class+Controller
    @FXML
    private TableView lessonView;
    @FXML
    private LessonView lessonViewController; // class+Controller
    @FXML
    private Button directionButton;
    @FXML
    private Button back;
    @FXML
    private Button toBoxButton;

    private ReadOnlyObjectProperty<Lesson> selected;
    private Direction direction = Globals.getDirection();


    public LessonToBoxDialog()
    {
        setTitle( "Lektion in Karteikasten kopieren" );

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("LessonToBoxDialog.fxml"));
        fxmlLoader.setController(this);
        Parent parent = null;
        try {
            parent = fxmlLoader.load();
            setScene(new Scene(parent));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        selected = lessonSelectionController.getSelectedLessonProperty();
        selected.addListener( (observable, oldLesson, newLesson) -> lessonViewController.setLesson(newLesson) );
        lessonSelectionController.setLessonInterface( new UnsavedBoxLessons() );
        lessonSelectionController.reload();

        back.setOnAction(e->close() );
        toBoxButton.setOnAction( e->addToBox() );
        directionButton.setOnAction( e->changeDirection() );

        String txt = Globals.getLanguageLabel1() + " -> " + Globals.getLanguageLabel2();
        directionLabel.setText(txt);

        back.setTooltip(new Tooltip("Schließen und zurück zum Hauptfenster"));
        directionButton.setTooltip(new Tooltip("Abfragerichtung wechseln"));
        toBoxButton.setTooltip(new Tooltip("Gewählte Lektion in die Lernkartei einfügen"));
    }

    private void changeDirection() {
        direction = ( direction == Direction.FRONT ) ? Direction.BACK : Direction.FRONT;

        String txt = direction == Direction.FRONT ? Globals.getLanguageLabel1() + " -> " + Globals.getLanguageLabel2() :
                Globals.getLanguageLabel2() + " -> " + Globals.getLanguageLabel1();
        directionLabel.setText(txt);
        Globals.setDirection(direction);
        lessonSelectionController.reload();

        Globals.setDirection( Direction.FRONT );
    }

    private void addToBox() {

        Lesson lesson = lessonSelectionController.getSelectedLesson();
        //todo for (Lesson lesson:lessonPreview.getSelectedLessons()) {
            Database.getDatabase().lessonToBox(lesson, direction );
        //}

        Globals.setDirection(direction);
        lessonSelectionController.reload();
    }

}
