package gui;

import data.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 */
public class CardAnswerChoice implements Initializable, CardAnswerInterface
{
    @FXML
    private Pane pane1;
    @FXML
    private Pane pane2;
    @FXML
    private Button showAnswer;
    @FXML
    private Button answerTrue;
    @FXML
    private Button answerFalse;
    @FXML
    private Label answerLabel;
    @FXML
    private Button play;
    @FXML
    private Button cancel;

    private Card card;
    private AnswerListener listener;
    private boolean answer = false;
    private ExitStatus status;
    private boolean currentQuestionFinished = false;


    @Override
    public void initialize(URL url, ResourceBundle ressourceBundle)
    {
        showAnswer.setOnAction( event -> showAnswer() );

        answerTrue.setOnAction( e->setAnswer(true) );
        answerFalse.setOnAction( e->setAnswer(false) );
        pane1.setVisible(true);
        pane2.setVisible(false);
        answerLabel.setText( "" );
        play.setVisible(false);
        play.setOnAction( e-> AudioPlayer.playClip( card.getAudio()) );
        cancel.setOnAction( e -> cancelled() );
    }

    public void cancelled() {
        status = ExitStatus.ABORTED;
        listener.answerFinished();
    }

    // answer is displayed
    public void showAnswer() {
        pane1.setVisible(false);
        pane2.setVisible(true);
        answerLabel.setText( card.getAnswer() );
        if (card.getAudio() != null) {
            AudioPlayer.playClip(card.getAudio());
            play.setVisible( true );
        }
        else {
            play.setVisible(false);
        }
        status = ExitStatus.ANSWERED;
        listener.answerFinished(); // notify
    }

    // user selects if he knew the answer
    public void setAnswer( boolean answer ) {
        pane2.setVisible(false);
        pane1.setVisible(true);
        answerLabel.setText( "" );
        this.answer = answer;
        status = ExitStatus.ANSWERED_AND_CONFIRMED;
        Language.Gender gender = Globals.getLanguage().getGender(card.getAnswer());
        //todo Color background = Globals.getLanguage().getColor( gender );
        currentQuestionFinished = true;
        play.setVisible(false);

        listener.answerFinished(); // notify
    }

    public void setQuestion( Card c )
    {
        card = c;
    }
    public void setAnswerListener(AnswerListener q)
    {
        listener = q;
    }
    public boolean isCorrect() { return answer; }
    public ExitStatus getStatus() { return status; }
    public String getAnswer() { return ""; } // no textual answer
}
