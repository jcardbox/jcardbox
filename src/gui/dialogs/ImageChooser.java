package gui.dialogs;

import data.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.awt.image.BufferedImage;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ImageChooser extends Stage implements Initializable
{
	@FXML
	private Pane imagePane;
	@FXML
	Button cancel;
	@FXML
	Button ok;
	@FXML
	Button delete;
	@FXML
	Button search;
	@FXML
	Slider slider;
	@FXML
	ImageView imageView;
	@FXML
	TextField input;

	private BufferedImage image; // currently choosen image
	private ArrayList<String> foundImages; // result of wiki search
	private BufferedImage tmpImages[];
	private static final int IMAGE_BUFFER_SIZE = 5; // load 10 images at once
	private boolean cancelled = false;
	private String initialText;

	public ImageChooser( String initialText )
	{
		this.initialText = initialText;
		setTitle( "Bild wählen" );

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ImageChooser.fxml"));
		fxmlLoader.setController(this);
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
			setScene(new Scene(parent));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		slider.setBlockIncrement(1.);
	}

	public void initialize(URL url, ResourceBundle resourceBundle)
	{
        imagePane.setMinHeight( Globals.getImageSize().getHeight() );
        imagePane.setMaxHeight( imagePane.minHeightProperty().get() );
        imagePane.setMinWidth( Globals.getImageSize().getWidth() );

		input.setText( initialText );
		search.setOnAction( e-> searchWiki() );

		slider.setMin(0);
		slider.setMax(0);
		slider.valueProperty().addListener( (o, oldVal, newVal) -> showImage( newVal.intValue() ) );

		cancel.setOnAction( e-> {image = null; cancelled = true; close();} );

		ok.setOnAction( e-> close() );

		delete.setOnAction( e-> {image = null; imageView.setImage(null);} );

		if (Globals.useWikiMedia()) searchWiki();
	}

	public BufferedImage getImage() {
		return image;
	}
	public boolean isCancelled() { return cancelled; }

	private void searchWiki()
	{
		try {
			//double t=System.currentTimeMillis();
			foundImages = OnlineSearch.searchImages(input.getText());
			//System.out.println("searchImages time: "+(System.currentTimeMillis()-t)); t=System.currentTimeMillis();
			int size = foundImages.size();
			if (size > 0) {
				slider.setValue(0);
				slider.setMax( size-1 );
				slider.setDisable(false);

				tmpImages = new BufferedImage[size];
				loadImages(0, IMAGE_BUFFER_SIZE); // load first 10 images
				showImage(0);
			}
			else {
				slider.setValue(0);
				slider.setMax(0 );
				slider.setDisable(true);
				image = null;
				imageView.setImage(null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class ImageLoaded implements ImageListener
	{
		private int index;
		public ImageLoaded( int index ) {
			this.index = index;
		}
		public void imageLoaded( BufferedImage newImage ) {
			tmpImages[index] = newImage;
			if (index == slider.getValue()) {
				javafx.scene.image.WritableImage img = javafx.embed.swing.SwingFXUtils.toFXImage(newImage, null);
				imageView.setImage(img);
				image = newImage;
			}
		}
	}

	/** load the next count images starting with index idx */
	private void loadImages(int idx, int count) {
		//todo setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		for (int index = idx; index < tmpImages.length && index < idx+count; index++)
		{
			if (tmpImages[index] == null) {
				OnlineSearch.readImage(foundImages.get(index), new ImageLoaded(index));
			}
		}

		//setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	private void showImage(int index) {
		if (tmpImages == null) return;

		int bufferIndex = Math.min( index + 2, tmpImages.length-1 );
		if (tmpImages[bufferIndex] == null) {
			loadImages(bufferIndex, IMAGE_BUFFER_SIZE);
		}
		image = tmpImages[index];
		if ( image == null ) {
			imageView.setImage( null );
		} else {
			javafx.scene.image.WritableImage img = javafx.embed.swing.SwingFXUtils.toFXImage(image, null);
			imageView.setImage(img);
		}
	}


	/*
	private class ImageLoader implements ActionListener {
		final JFileChooser fc = new JFileChooser();

		public BufferedImage scaleImage( BufferedImage image ) {
			Dimension size = Globals.getImageSize();
			double ratio1 = image.getHeight() / image.getWidth();
			double ratio2 = size.height / size.width;
			int width = size.width;
			int height = size.height;
			if (ratio1 < ratio2) {
				height = image.getHeight() * width / image.getWidth();
			} else {
				width = image.getWidth() * height / image.getHeight();
			}
			Image scaled = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
			BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			Graphics g = newImage.getGraphics();
			g.drawImage(scaled, 0, 0, null);
			g.dispose();
			return newImage;
		}

		public void actionPerformed(ActionEvent arg0)
		{
			int returnVal = fc.showOpenDialog(ImageChooser.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				try {
					File file = fc.getSelectedFile();
					image = ImageIO.read(file);
					image = scaleImage( image );
					ImageIcon imageIcon = new ImageIcon(image);
					imageLabel.setIcon(imageIcon);

					// disable wiki part
					slider.setEnabled(false);
					tmpImages = null;
				} catch (Exception e) {
					e.printStackTrace();
				}
			} // user hasn't cancelled
		}
	}
	*/
}
