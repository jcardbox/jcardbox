package gui.dialogs;

import data.Card;
import data.Database;
import data.Lesson;
import data.SQLDatabase;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.fxml.*;

/**
 * Dialog to delete a card from different Database tables
 */
public class CardDeleteDialog extends Stage implements Initializable
{
    private ArrayList<CheckBox> deps = new ArrayList<>();
    private ArrayList<Lesson> selectedLessons = new ArrayList<>();
    private ArrayList<Card> cards;

    @FXML
    private CheckBox butAll;
    @FXML
    private CheckBox butBox;
    @FXML
    private Button ok;
    @FXML
    private Button cancel;

    public CardDeleteDialog( Card card) {
        this( new ArrayList<Card>(Arrays.asList(card)));
    }

    public CardDeleteDialog( ArrayList<Card> cards) {
        setTitle("Vokabel löschen");
        this.cards = cards;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("CardDeleteDialog.fxml"));
		fxmlLoader.setController(this);
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
			setScene(new Scene(parent));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
    }

    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        /*
        p.add(new JLabel(), "wrap 10px");
        if (cards.size() == 1) {
            Card card = cards.get(0);
            p.add(new JLabel("Karte: " + card.getFront() + " - " + card.getBack()), "center, wrap 10px");
        }
*/
        String lcard = ( ( cards.size() > 1 ) ? "Karten" : "Karte" ) + " löschen";
        //cardCheck = new JCheckBox( "Alle Vorkommen der " + lcard+ " löschen" );


        boolean hasBoxEntry = false;
        for (Card card : cards) {
            int boxNr = Database.getDatabase().getBoxNr(card);
            if (boxNr >= 0) {
                hasBoxEntry = true;
                break;
            }
        }

        butAll.setOnAction( e-> set( butAll ) );

        butBox.setVisible( hasBoxEntry );
        if ( hasBoxEntry ) {
            deps.add( butBox );
        }
        boolean isInLesson = false;
        for (Card card : cards) {
            if (card.getLessons() != null && card.getLessons().size() > 0)
            {
                isInLesson = true;
                break;
            }
        }
        if ( isInLesson ) {
            //p.add(new JLabel(lcard + " aus der gewählten Lektion entfernen"));
            ArrayList<Lesson> lessons = new ArrayList<>();
            for (Card card : cards) {
                for (Lesson lesson : card.getLessons()) {
                    if (!lessons.contains(lesson)) {
                        lessons.add(lesson);
                    }
                }
            }
            for ( Lesson lesson : lessons )
            {
                //todo
                /*
                final JCheckBox box = new JCheckBox( lesson.getName() );
                p.add(box);
                deps.add(box);
                box.addActionListener(e -> setLessonSelected(lesson, box.isSelected()));
                */
            }
        }

        ok.setOnAction( e-> { deleteCard(); close(); } );
        cancel.setOnAction( e->close() );
    }

    private void setLessonSelected(Lesson lesson, boolean selected) {
        if (selected)
            selectedLessons.add(lesson);
        else
            selectedLessons.remove(lesson);
    }

    private void set( CheckBox box ) {
        for (CheckBox b:deps) {
            b.setDisable( box.isSelected() );
            b.setSelected(true);
        }
    }

    private void deleteCard() {
        SQLDatabase db = Database.getDatabase();

        for ( Card card : cards ) {
            if ( butAll.isSelected() ) { // delete every occurence of the given card
                db.deleteCard( card ) ;
            } else {
                if ( butBox.isSelected() ) {
                    db.deleteCardFromBox( card );
                }
                for (Lesson lesson : selectedLessons) {
                    db.deleteCardFromLesson( card, lesson );
                }
            }
        }
    }

}
