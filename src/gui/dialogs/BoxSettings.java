package gui.dialogs;
import data.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.fxml.*;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class BoxSettings extends Stage implements Initializable
{
	@FXML
	private Button ok;
	@FXML
	private Button cancel;
	@FXML
	TextField dir1;
	@FXML
	TextField dir2;
	@FXML
	Label label1;
	@FXML
	Label label2;

	public BoxSettings()
	{
		setTitle( "Einstellungen Karteikasten" );
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("BoxSettings.fxml"));
		fxmlLoader.setController(this);
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
			setScene(new Scene(parent));
		}
		catch (IOException e) {	e.printStackTrace(); }
	}

	public void initialize(URL url, ResourceBundle resourceBundle)
	{
		ok.setOnAction( e-> ok() );
		cancel.setOnAction( e-> close() );
		dir1.setText(Globals.join(Globals.getBoxDates(Direction.FRONT)));
		dir2.setText(Globals.join(Globals.getBoxDates(Direction.BACK)));
		label1.setText( Globals.getLanguageLabel1() +"->"+ Globals.getLanguageLabel2() );
		label2.setText( Globals.getLanguageLabel2() +"->"+ Globals.getLanguageLabel1() );

	}

	private void ok()
	{
		Globals.setBoxDates(Globals.split(dir1.getText()), Globals.split(dir2.getText()) );
		close();
	}

}
