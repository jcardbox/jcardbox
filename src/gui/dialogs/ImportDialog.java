package gui.dialogs;

import data.*;
import gui.LessonView;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by Anke on 4/8/17.
 */

public class ImportDialog extends Stage implements Initializable {
    @FXML
    private ComboBox<String> category;
    @FXML
    private TextField lessonName;
    @FXML
    private LessonView lessonViewController;
    @FXML
    private Button cancel;
    @FXML
    private Button ok;

    // ==============================================================================================
    private ArrayList<Card> cards;
    private String defaultLessonName;

    public ImportDialog( ArrayList<Card> cards, String name ) {
        setTitle("Lektionen importieren");
        this.cards = cards;
        defaultLessonName = name;

        // create new window and fill it
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ImportDialog.fxml"));
        fxmlLoader.setController(this);
        try {
            setScene(new Scene(fxmlLoader.load()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle ressourceBundle) {
        ObservableList<String> categories = Globals.getCategoryList();
        category.setItems(categories);
        category.setValue(Globals.getDefaultCategory());

        lessonName.setText( defaultLessonName );
        lessonViewController.setLesson( cards );

        cancel.setOnAction( e->close() );
        ok.setOnAction( e-> importLesson() );

    }

    private void importLesson() {
        String name = lessonName.getText();
        String category = this.category.getSelectionModel().getSelectedItem();

        if (name.isEmpty()) return;

        boolean exists = false;
        try {
            exists = Database.getDatabase().lessonExists( new Lesson( name, category) );
        } catch (java.sql.SQLException e) {
            System.err.println(e);
        }

        if (!exists) {
            Lesson lesson = new Lesson(name, category);
            lesson.setCards(cards);
            Database.getDatabase().addLesson(lesson);
            close();
        }
        else {
            DefaultDialogs.showErrorDialog( "Fehler", "Eine Lektion mit diesem Namen existiert bereits!");
        }
    }
}
