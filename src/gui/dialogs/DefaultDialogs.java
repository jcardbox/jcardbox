package gui.dialogs;

import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * Created by A.Visser on 3/26/17.
 */
public class DefaultDialogs {
    public static void showErrorDialog(String title, String message )
    {
        showExceptionDialog( title, message, null );
    }
    public static void showExceptionDialog(String title, String message, Exception e)
    {
        // Platform.runLater(() -> {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStyleClass().add("jcardbox");

        alert.setTitle( title );
        alert.setHeaderText( message );

        if (e != null) {
            TextArea textArea = new TextArea(e.toString());
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            Label label = new Label("The exception stacktrace was:");
            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(label, 0, 0);
            expContent.add(textArea, 0, 1);
            alert.getDialogPane().setExpandableContent(expContent);
        }

        alert.showAndWait();
        // });
    }

}
