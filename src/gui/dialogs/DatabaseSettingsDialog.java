package gui.dialogs;
import data.*;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class DatabaseSettingsDialog extends Stage implements Initializable
{
	@FXML
	private TextField dirInput;
	@FXML
	private TextField serverInput;
	@FXML
	private TextField useridInput;
	@FXML
	private TextField passwordInput;
	@FXML
	private RadioButton internal;
	@FXML
	private RadioButton remote;
	@FXML
	private Button ok;
	@FXML
	private Button cancel;
	@FXML
	private Button chooseDir;

	private Database.ConnectionData data = new Database.ConnectionData();
	private static String languageDir = Globals.getInstallationRoot() + File.separator + Globals.getLanguageIdentifier();
	private static String inifile = Globals.getInstallationRoot() + File.separator + "settings.ini";
	private boolean cancelled = false;

	public static boolean installationExists() {
		return ( new File(languageDir + File.separator + "database").exists() );
	}

	public boolean isCancelled() {return cancelled;}

	public DatabaseSettingsDialog()
	{
		initModality(Modality.APPLICATION_MODAL);
		languageDir = Globals.getInstallationRoot() + File.separator + Globals.getLanguageIdentifier();
		inifile = Globals.getInstallationRoot() + File.separator + "settings.ini";

		createInstallationRoot();
		data.readIniFile();

		setTitle( "Datenbankeinstellungen" );

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("DatabaseSettingsDialog.fxml"));
		fxmlLoader.setController(this);
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
			setScene(new Scene(parent));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void initialize(URL url, ResourceBundle resourceBundle)
	{
		dirInput.setText(Globals.getInstallationRoot());
		chooseDir.setOnAction( event ->
		{
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Open Resource File");
			File file = fileChooser.showOpenDialog( this );
			if (file != null) {
				dirInput.setText(file.getAbsolutePath());
			}
		});

		boolean installationDirIsSet =  ( System.getProperty("dir", null) != null );
		if ( installationDirIsSet || new File(inifile).exists() ) {
			dirInput.setDisable( true );
			chooseDir.setDisable( true );
		}

		serverInput.setText(data.getDatabaseURL());
		useridInput.setText(data.getDatabaseUser());
		passwordInput.setText(data.getDatabasePass());

		ArrayList<Control> remoteDbElements = new ArrayList<>();
		remoteDbElements.add(serverInput);
		remoteDbElements.add(useridInput);
		remoteDbElements.add(passwordInput);

		internal.setOnAction(e -> {
			for (Control c : remoteDbElements)
				c.setDisable(true);
		});
		remote.setOnAction(e -> {
			for (Control c : remoteDbElements)
				c.setDisable(false);
		});

		// set values according to saved settings
		if (!data.isInternalDatabase()) {
			remote.setSelected(true);
		}
		else {
			internal.setSelected(true);
			for(Control c : remoteDbElements )
				c.setDisable(true);
		}

		ok.setOnAction(e -> okPressed());
		cancel.setOnAction(e -> cancelPressed());
	}


	private void okPressed() {
		try {
			applySettings();
			hide();
		} catch (Exception e) {
			DefaultDialogs.showExceptionDialog("Fehler", "Fehler beim Öffnen der Datenbank", e);
			e.printStackTrace();
		}
	}

	private void cancelPressed() {
		hide();
		cancelled = true;
	}

	private void applySettings()
	{
		String dirname = dirInput.getText();
		Globals.setInstallationRoot( dirname );
		boolean ok = createInstallationRoot();
		if (!ok) return; // abort database operations, but keep dialog after error message has been shown

		data.setInternalDatabase(internal.isSelected());
		data.setDatabaseURL(serverInput.getText());
		data.setDatabaseUser(useridInput.getText());
		data.setDatabasePass(passwordInput.getText());
		data.saveIniFile();
	}

	private static boolean createInstallationRoot()
	{
		boolean ok = createDirectoryStructure( Globals.getInstallationRoot() );

		return ok;
	}

	private static boolean createDirectoryStructure( String dirname ) {
		boolean ok = false;

		try {
			File dir = new File(dirname + File.separator + Globals.getLanguageIdentifier());
			if (!dir.exists()) {
				if (!dir.mkdirs()) throw new Exception("Installationsverzeichnis konnte nicht angelegt werden: " + dir);
			}
			File datadir = new File(dir, "database");
			datadir.mkdir();
			File subdir = new File(dir, "exports");
			subdir.mkdir();
			subdir = new File(dir, "imports");
			subdir.mkdir();
			subdir = new File(dir, "cache");
			subdir.mkdir();
			ok = true;

		} catch (Exception e) {
			DefaultDialogs.showExceptionDialog("Fehler", "Fehler beim Erstellen der Verzeichnisstruktur", e);
		}
		return ok;
	}

}
