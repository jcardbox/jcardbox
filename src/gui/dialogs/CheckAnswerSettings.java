package gui.dialogs;
import data.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.fxml.*;

import java.awt.*;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class CheckAnswerSettings extends Stage implements Initializable
{
	@FXML
	private Button ok;
	@FXML
	private Button cancel;
	@FXML
	ComboBox<String> dir1;
	@FXML
	ComboBox<String> dir2;

	private ObservableList<String> list1 = FXCollections.observableArrayList();
	private ObservableList<String> list2 = FXCollections.observableArrayList();

	public CheckAnswerSettings()
	{
		setTitle( "Antworten auswerten" );
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("CheckAnswerSettings.fxml"));
		fxmlLoader.setController(this);
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
			setScene(new Scene(parent));
		}
		catch (IOException e) {	e.printStackTrace(); }
	}

	public void initialize(URL url, ResourceBundle resourceBundle)
	{
		ok.setOnAction( e-> ok() );
		cancel.setOnAction( e-> close() );

		String inputTypeLabels[] = {
				"Programm prüft Korrektheit (Texteingabe)",
				"Nutzer prüft Korrektheit"};
		String directions[] = {
				Globals.getLanguageLabel1() + "->" + Globals.getLanguageLabel2(),
				Globals.getLanguageLabel2() + "->" + Globals.getLanguageLabel1()
		};

		dir1.setItems(list1);
		dir2.setItems(list2);
		for (int i=0; i<2; i++ ) {
			list1.add( inputTypeLabels[i] );
			list2.add( inputTypeLabels[i] );
		}

		Globals.UserQuestionType type = Globals.getQuestionType( Direction.FRONT );
		dir1.getSelectionModel().select( type == Globals.UserQuestionType.Text ? 0 : 1 );

		type = Globals.getQuestionType( Direction.BACK );
		dir2.getSelectionModel().select( type == Globals.UserQuestionType.Text ? 0 : 1 );
	}

	private void ok()
	{
		Globals.UserQuestionType type = dir1.getSelectionModel().getSelectedIndex() == 0 ? Globals.UserQuestionType.Text : Globals.UserQuestionType.Choice;
		Globals.setQuestionType( Direction.FRONT, type );
		type = dir2.getSelectionModel().getSelectedIndex() == 0 ? Globals.UserQuestionType.Text : Globals.UserQuestionType.Choice;
		Globals.setQuestionType( Direction.BACK, type );
		close();
	}

}
