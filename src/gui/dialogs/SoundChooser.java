package gui.dialogs;
import data.*;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import gui.AudioPlayer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class SoundChooser extends Stage implements Initializable
{
	@FXML
	Button cancel;
	@FXML
	Button ok;
	@FXML
	Button delete;
	@FXML
	Button search;
	@FXML
	Button play;
	@FXML
	TextField input;
	@FXML
	ListView list;

	private String searchText;
	private ArrayList<String> sounds; // result of wiki search
	private byte[] sound; // currently choosen sound
	private byte[][] tmpSounds;

	private boolean cancelled = false;
	
	public boolean isCancelled() {
		return cancelled;
	}

	/**

	 * @param searchText title of the sound that should be searched
	 */
	public SoundChooser(String searchText)
	{
		this.searchText = searchText;
		setTitle( "Sound aus dem Wiktionary wählen" );

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SoundChooser.fxml"));
		fxmlLoader.setController(this);
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
			setScene(new Scene(parent));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	public void initialize(URL url, ResourceBundle resourceBundle)
	{
	//JButton btnLoadFromFile = new JButton("Load from File");

		search.setOnAction( event ->  searchWiki() );

		input.setText( searchText );
		Globals.getLanguage().setInput(input);

		delete.setOnAction( event -> { sound = null; cancelled = false; close(); } );
		cancel.setOnAction( event -> { sound = null; cancelled = true; close(); } );
		ok.setOnAction( event -> close() );
		play.setOnAction( event -> playCurrentSound() );
		searchWiki();
	}
	
	/**
	 * @return selected sound 
	 */
	public byte[] getSound() {
		if ( tmpSounds != null )
		{
			int index = list.getSelectionModel().getSelectedIndex();
			try {
				if (tmpSounds[index] == null) {
					tmpSounds[index] = OnlineSearch.readSound(sounds.get(index));
				}
				sound = tmpSounds[index];
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sound;
	}
	
	private void searchWiki() {
		try {
			sounds = OnlineSearch.searchSounds(input.getText());
			int size = sounds.size();
			System.out.println("found sound: "+size);
			ObservableList<String> listModel = FXCollections.observableArrayList();

			list.setItems( listModel );

			for (String sound : sounds) {
				int idx = sound.lastIndexOf('/');
				listModel.add(sound.substring(idx+1));
			}
			if (size > 0) {
				list.getSelectionModel().select(0);
				
				tmpSounds = new byte[size][];
				sound = null;
			} else
				tmpSounds = null;

		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	private void playCurrentSound() {
		if (tmpSounds != null)
			playSound();
	}
	
	private void playSound() {
		sound = getSound();
		if (sound != null)
		  AudioPlayer.playClip(sound);
	}

}
