package gui.dialogs;
import data.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.fxml.*;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class CardSearch extends Stage implements Initializable
{
	@FXML
	private Button ok;
	@FXML
	private Button cancel;
	@FXML
	private Button search;
	@FXML
	private TextField input;
	@FXML
	private CheckBox exactMatchCheck;
	@FXML
	private ListView<CardWrapper> list;
	@FXML
	MenuItem edit;
	@FXML
	MenuItem insert;
	@FXML
	MenuItem delete;

	private ObservableList<CardWrapper> listModel = FXCollections.observableArrayList();; // model for the ListView above
	private ArrayList<Card> result;

	private class CardWrapper {
		private Card card;
		private String label;
		public CardWrapper(Card c, String l) {
			card = c; label = l;
		}
		public Card getCard() {
			return card;
		}
		public String toString() {
			return label;
		}
	}

	public CardSearch()
	{
		setTitle( "Suche" );

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("CardSearch.fxml"));
		fxmlLoader.setController(this);
		try {
			setScene(new Scene(fxmlLoader.load()));
		}
		catch (IOException e) {e.printStackTrace();}
	}

	public void initialize(URL url, ResourceBundle resourceBundle)
	{
		search.setOnAction( e-> search( input.getText(), exactMatchCheck.isSelected() ) );
		cancel.setOnAction( e-> { close();});
		list.setItems(listModel);
		edit.setOnAction( event -> changeCard( list.getSelectionModel().getSelectedItem().getCard() ) );
		delete.setOnAction( e->delete(list.getSelectionModel().getSelectedItem().getCard()) );
		insert.setOnAction( e->box(list.getSelectionModel().getSelectedItem().getCard()) );
	}

	private void search(String searchString, boolean exactMatch) {
		listModel.clear();
		result = Database.getDatabase().searchCards(searchString, exactMatch);

		for (Card card : result) {
			String label = card.getFront() + ":" + card.getBack();
			if (card.getBoxNr() >= 0) label += ", Fach " + (card.getBoxNr() + 1);
			if (card.getLessons() != null && card.getLessons().size() > 0) {
				Lesson lesson = card.getLessons().get(0); // todo show all?
				label += ", Lektion " + lesson.getName() + " (" + lesson.getCategory() + ")";
				if (card.getLessons().size() > 1) label += "...";
			}

			listModel.add( new CardWrapper(card, label) );
		}
	}

	private void changeCard( Card card ) {
		CardEditDialog dialog = new CardEditDialog(card);
		dialog.showAndWait();
	}

	private void delete( Card card ) {
		new CardDeleteDialog(card).showAndWait();
	}

	private void box(Card card ) {
		List<String> possibilities = new ArrayList<>();

		int[] boxDates = Globals.getBoxDates(Globals.getDirection());
		for (int i = 0; i < boxDates.length; i++) {
			possibilities.add("Kasten " + (i+1) + " (" + boxDates[i] + " Tage)");
		}
		possibilities.add(0, "Nicht eingefügt");
		possibilities.add( possibilities.size(),"Erfolgreich gelernt");

		int currentIndex = card.getBoxNr();
		if (currentIndex == Card.BOX_NOT_INSERTED)
			currentIndex = 0;
		else if (currentIndex == Card.BOX_FINISHED)
			currentIndex = possibilities.size() - 1;
		else
			currentIndex++;

		String before = possibilities.get(currentIndex);

		ChoiceDialog<String> dialog = new ChoiceDialog<>(before, possibilities);
		DialogPane pane = dialog.getDialogPane();
		pane.getStylesheets().add(getClass().getResource("../JCardBox.css").toExternalForm());
		pane.getStyleClass().add("jcardbox");

		dialog.setTitle("Karteifach ändern");
		dialog.setHeaderText("Karteifach ändern\nBisheriges Fach: " + before);
		dialog.setContentText("Neues Fach:");


		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()){
			int newBox = 0;
			for (String str : possibilities) {
				if (str.equals( result.get() )) {
					if (newBox == 0)
						newBox = Card.BOX_NOT_INSERTED;
					else if (newBox == possibilities.size()-1 )
						newBox = Card.BOX_FINISHED;
					else
						newBox--;

					String text = "Karteikasten einsortieren: "+card.getFront() + ":" + card.getBack() + " - " + before + " -> " + str;
					LogEntry entry = new LogEntry(LogEntry.LOG_MODIFY_BOX, text);
					entry.write();

					Database.getDatabase().revertCard(card, newBox);

					break;
				}
				newBox++;
			}
		}

	}

}
