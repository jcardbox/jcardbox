package gui.dialogs;

import data.*;

import gui.CardEdit;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * creates a panel which allows the user to select a category and shows all available lessons
 */
public class CardEditDialog extends Stage implements Initializable
{
    // lesson selection tag
    @FXML
    private Pane cardEdit;
    @FXML
    private CardEdit cardEditController;
    @FXML
    private Button cancel;
    @FXML
    private Button ok;

    private Card card;

    public CardEditDialog( Card card ) {
        setTitle("Karte bearbeiten");
        this.card = card;

        // create new window and fill it
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("CardEditDialog.fxml"));
        fxmlLoader.setController(this);
        try {
            setScene(new Scene(fxmlLoader.load()));
        }
        catch (IOException e) {e.printStackTrace();}
    }
    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        cardEditController.setCard( card );
        ok.setOnAction( e->changeCard() );
        cancel.setOnAction( e->close() );
    }

    private void changeCard() {
        Card orig = this.card;
        Card card = cardEditController.getCard();
        Database.getDatabase().changeCard(orig, card.getFront(), card.getBack(), card.getImage(), card.getAudio() );
        close();
    }
}
