package gui.dialogs;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by anke on 4/6/17.
 */
public class Dialogs extends Stage {
    public Dialogs( String title, String fxml )
    {
        setTitle( title );
        FXMLLoader fxmlLoader = new FXMLLoader( getClass().getResource( fxml ) );
        fxmlLoader.setController(this);
        Parent parent = null;
        try {
            parent = fxmlLoader.load();
            setScene( new Scene(parent) );
        }
        catch (IOException e) {	e.printStackTrace(); }
    }
}
