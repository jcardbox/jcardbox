package gui;

import gui.dialogs.*;
import data.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Controller of CardEdit.fxml
 */
public class CardEdit implements Initializable
{
    @FXML
    private Label header;
    @FXML
    private TextField front;
    @FXML
    private ComboBox<String> back;
    @FXML
    private ImageView imageView;
    @FXML
    private Button image;
    @FXML
    private Button audio;
    @FXML
    private Button play;
    @FXML
    private FlowPane imagePane;
    @FXML
    private CheckBox wiktionary;
    @FXML
    private CheckBox wikimedia;

    private static final String EXISTS = " (schon vorhanden)";
    private String lastInput = "";
    private byte[] sound = null;

    private ArrayList<Card> cardList = new ArrayList<>();

    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        imagePane.setMinHeight( Globals.getImageSize().getHeight() );
        imagePane.setMaxHeight( imagePane.minHeightProperty().get() );
        imagePane.setMinWidth( Globals.getImageSize().getWidth() );

        front.focusedProperty().addListener( e-> searchTranslation() );

        image.setOnAction( e->selectImage() );
        audio.setOnAction( e->selectSound() );
        play.setDisable( true );
        play.setOnAction( e->AudioPlayer.playClip( sound ) );

        back.setOnAction( e-> backSelected() );

        wiktionary.setSelected( Globals.useWiktionary() );
        wikimedia.setSelected( Globals.useWikiMedia() );
        wiktionary.setOnAction( e-> Globals.setUseWiktionary( wiktionary.isSelected() ) );

        setHeader("");
    }

    public void setHeader( String str ) {
        header.setText( str );
    }

    public Card getCard()
    {
        Card card = new Card();
        card.setFront( front.getText().trim() );
        String backStr = back.getValue();
        if (backStr == null || backStr.trim().length() == 0)
            return card;
         if (backStr.contains(EXISTS)) { // existing card selected
            backStr = backStr.replace(EXISTS,"");
        }

        card.setBack( backStr );
        java.awt.image.BufferedImage bimage = null;
        if ( imageView.getImage() != null ) {
            bimage = javafx.embed.swing.SwingFXUtils.fromFXImage(imageView.getImage(), null);
        }
        card.setBufferedImage( bimage );
        card.setAudio(sound);

        return card;
    }

    public void setCard( Card card ) {
        setCard( card, true );
    }
    private void setCard( Card card, boolean newEntry )
    {
        if (card != null) {
            front.setText( card.getFront() );
            lastInput = card.getFront();

            if (newEntry) {
                back.setOnAction( null );
                cardList.clear();
                cardList.add(card);
                back.getItems().clear();
                back.getItems().add(card.getBack());
                back.getSelectionModel().select(0);
                back.setOnAction( e-> backSelected() );
            }

            if ( card.getImage() != null )
            {
                javafx.scene.image.WritableImage img = javafx.embed.swing.SwingFXUtils.toFXImage(card.getImage(), null);
                imageView.setImage(img);
            }
            else
                imageView.setImage(null);
            sound = card.getAudio();
            play.setDisable( card.getAudio() == null );
        }
    }

    public void clear() {
        sound = null;
        lastInput="";

        front.setText("");
        if (back.getItems().size() >= 0)
            back.getItems().clear();
        back.getEditor().clear();
        back.setValue("");
        imageView.setImage( null );
        play.setDisable( true );
        cardList.clear();
    }

    /* change image and sound, if data exists for the selected cardback */
    private void backSelected()
    {
        if (back.getSelectionModel().getSelectedIndex() < 0)
            return;
        Card card = cardList.get(back.getSelectionModel().getSelectedIndex());
        if ( card != null && card.getDatabaseID() >= 0 ) { // set sound and image, if available
            Database.getDatabase().readMultimediaData( card );
            setCard( card, false );
        }
        else { // clear sound and image
            sound = null;
            play.setDisable( true );
            imageView.setImage( null );
        }
    }

    private void selectImage()
    {
        Card c = getCard();
        if (!c.isValid()) return;
        String translation = c.getBack().replaceFirst("^to ", "");
        ImageChooser chooser = new ImageChooser( translation );
        chooser.showAndWait();
        if (!chooser.isCancelled()) {
            java.awt.image.BufferedImage bufimage = chooser.getImage();
            if (bufimage != null) {
                javafx.scene.image.WritableImage img = javafx.embed.swing.SwingFXUtils.toFXImage(bufimage, null);
                imageView.setImage( img );
            } else {
                imageView.setImage( null );
            }
        }
    }

    private void selectSound() {
        Card c = getCard();
        if (!c.isValid()) return;
        String translation = c.getBack().replaceFirst("^to ", "");
        SoundChooser soundChooser = new SoundChooser( translation );
        soundChooser.showAndWait();
        if (!soundChooser.isCancelled()) {
            sound = soundChooser.getSound();
            play.setDisable(sound == null);
        }
    }

    private void searchTranslation()
    {
        if (front.focusedProperty().get()) return; // gained focus, search translation if focus got lost

        Card card = getCard();
        String frontTxt = front.getText().trim();
        if (frontTxt.length() < 2 ) return;
        if (frontTxt.equals(lastInput)) return; // nothing changed

        try {
            back.setOnAction( null );
            back.getItems().clear();
            cardList.clear();
            if ( card.isValid() ) {
                cardList.add(card); // add items from previous translation to enable changes in front without deleting translation
                back.getSelectionModel().select( cardList.size() -1 ); // select previous translation
            }

            // add entries from database
            if (Database.getDatabase() != null) {
                ArrayList<Card> cards = Database.getDatabase().searchSynonym(front.getText());
                for (Card c : cards) {
                    cardList.add(c);
                }
            }
            // search wiki for translations
            if (Globals.useWiktionary()) {
                lastInput = frontTxt;
                ArrayList<String> translationList = OnlineSearch.searchTranslation(frontTxt);

                for (String str : translationList) {
                    cardList.add(new Card(card.getFront(), str));
                }
            }
            for (Card c : cardList) {
                back.getItems().add( c.getBack() + (c.getDatabaseID() < 0 ? "" : " "+EXISTS));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            back.setOnAction( e-> backSelected() );
            if ( back.getItems().size() > 0 )
                back.getSelectionModel().select(0);
        }
    }
}
