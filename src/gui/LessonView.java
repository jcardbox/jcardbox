package gui;

import data.Card;
import data.Direction;
import data.Globals;
import data.Lesson;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class LessonView implements Initializable
{
    @FXML
    private TableView<Row> lessonView;
    @FXML
    TableColumn<Row, String> frontCol;
    @FXML
    TableColumn<Row, String> backCol;

    private Lesson lesson;
    private SimpleObjectProperty<Card> selectedCard = new SimpleObjectProperty<Card>(null);

    public void initialize(URL url, ResourceBundle resourceBundle)
    {
    }

    public void clearSelection() {
        selectedCard.set(null);
        lessonView.getSelectionModel().clearSelection();
    }

    public SimpleObjectProperty<Card> getCardProperty() {
        return selectedCard;
    }

    public void reload() {
        setLesson(lesson);
    }

    public void setLesson( Lesson newLesson )
    {
        frontCol.setText( Globals.getDirection() == Direction.FRONT ? Globals.getLanguageLabel1() : Globals.getLanguageLabel2() );
        backCol.setText( Globals.getDirection() != Direction.FRONT ? Globals.getLanguageLabel1() : Globals.getLanguageLabel2() );

        lesson = newLesson;
        lessonView.setItems( lessonToObservable( newLesson ) );
        frontCol.setCellValueFactory(cellData -> cellData.getValue().frontProperty() );
        backCol.setCellValueFactory(cellData -> cellData.getValue().backProperty()) ;

        lessonView.getSelectionModel().selectedItemProperty().addListener( (obs, old, newRow) -> selectRow(newRow) );
    }

    public void setLesson( ArrayList<Card> cardList)
    {
        lesson = null;
        frontCol.setText( Globals.getDirection() == Direction.FRONT ? Globals.getLanguageLabel1() : Globals.getLanguageLabel2() );
        backCol.setText( Globals.getDirection() != Direction.FRONT ? Globals.getLanguageLabel1() : Globals.getLanguageLabel2() );

        ObservableList<Row> data = FXCollections.observableArrayList();
        for ( Card c : cardList) {
            data.add(new Row(c));
        }

        lessonView.setItems( data );
        frontCol.setCellValueFactory(cellData -> cellData.getValue().frontProperty() );
        backCol.setCellValueFactory(cellData -> cellData.getValue().backProperty()) ;
    }


    private void selectRow( Row row )
    {
        if (row != null) {
            selectedCard.set(row.getCard());
        }
        else
        {
            selectedCard.set( null );
        }
    }

    private class Row // javafx wrapper for data.Card
    {
        private Card card;
        private final StringProperty front;
        private final StringProperty back;
        public Row( Card card ) {
            this.card = card;
            this.front = new SimpleStringProperty( card.getQuestion() );
            this.back = new SimpleStringProperty( card.getAnswer() );
        }
        public StringProperty frontProperty() {
            return front;
        }
        public StringProperty backProperty() {
            return back;
        }
        public Card getCard() {
            return card;
        }
    }

    private ObservableList<Row> lessonToObservable(Lesson lesson )
    {
        ObservableList<Row> data = FXCollections.observableArrayList();
        if (lesson != null) {
            lesson.load(false);
            for (Card c : lesson.getCards()) {
                data.add(new Row(c));
            }
        }
        return data;
    }

}