package gui;
import data.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.fxml.FXMLLoader;
import java.io.*;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

public class BoxStatisticDialog extends Stage implements Initializable
{
	@FXML
	private Pane lessonSelection;
	@FXML
	private LessonSelection lessonSelectionController; // class+Controller
	@FXML
	private StackedBarChart<Number, String> dayChart;
	@FXML
	private StackedBarChart<Number, String> lessonChart;
	@FXML
	private NumberAxis xAxis;
	@FXML
	private CategoryAxis yAxis;
	@FXML
	private NumberAxis xAxisDay;
	@FXML
	private CategoryAxis yAxisDay;
	@FXML
	private GridPane grid;
	@FXML
	private ComboBox<String> categoryCombo;
	@FXML
	private ComboBox<String> languageCombo;
	@FXML
	private Tab log;

	private int searchDays = 15;
	private Direction direction = Direction.FRONT;
	private BoxStatistics stat;
	private	LogView logView = new LogView();

	public BoxStatisticDialog()
	{
		setTitle( "Statistiken" );

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("BoxStatisticDialog.fxml"));
		fxmlLoader.setController(this);
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
			setScene(new Scene(parent));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	public void initialize(URL url, ResourceBundle resourceBundle)
	{
		// lesson bar chart
		yAxis.setLabel("Lektionen");
		xAxis.setLabel("Vokabeln");
		xAxis.setAutoRanging(false);
		lessonChart.setAnimated( false );

		ObservableList<String> categories = Globals.getCategoryList();
		categoryCombo.setItems( categories );
		categoryCombo.setOnAction(e-> updateLessonChart() );
		categoryCombo.setValue( Globals.getDefaultCategory() );

		ObservableList<String> directionStr = FXCollections.observableArrayList();
		for (Language lang : Globals.getSelectedLanguages() ) {
			directionStr.add( Globals.getLanguageLabel1() + "->" + lang.getLabel() );
			directionStr.add( lang.getLabel() + "->" + Globals.getLanguageLabel1() );
		}
		languageCombo.setItems( directionStr );
		languageCombo.getSelectionModel().select(0);
		languageCombo.setOnAction( e-> {lastCategory=""; updateData();} );

		log.setContent( logView );
		updateData();
	}

	private void updateData() {
		int langIdx = languageCombo.getSelectionModel().getSelectedIndex()/2;
		Language lang = Globals.getSelectedLanguages().get( langIdx );
		Language old = Globals.getLanguage();
		Globals.setLanguage( lang );

		direction = languageCombo.getSelectionModel().getSelectedIndex()%2 == 0 ? Direction.FRONT : Direction.BACK;
		try {
			stat = Database.getDatabase().getBoxStatistics( direction, searchDays );
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}

		logView.update();
		updateTextStatistics();
		updateDayChart();
		updateLessonChart();

		Globals.setLanguage( old );
	}

	private void updateTextStatistics()
	{
		grid.getChildren().clear();

		int boxCount = Math.max(stat.cardsForDay.length, Globals.getBoxDates(direction).length);
		int dayFound = 0;
		for (int i=0; i<searchDays; i++) {
			int sum=0;
			for (int boxNr = 0; boxNr < boxCount; boxNr++) {
				sum+=stat.cardsForDay[boxNr][i];
			}
			dayFound = i;
			if (sum > 0) break;
		}

		String nextDate;
		if (dayFound == 0)
			nextDate="heute";
		else if (dayFound == 1)
			nextDate="morgen";
		else
			nextDate="in "+dayFound+ " Tagen";

		int row = 0;
		for (int i=0; i<boxCount; i++)
		{
			int days = i < Globals.getBoxDates(direction).length ? Globals.getBoxDates(direction)[i] : 0;
			String str1=(i+1)+". Kasten ("+days+" Tage): ";
			String str2=stat.cardsInBox[i]+" Karten ";
			String str3=stat.cardsForDay[i][dayFound]+" Karten " + nextDate;

			grid.add(new Label(str1), 0, row);
			grid.add(new Label(str2), 1, row);
			grid.add(new Label(str3), 2, row);
			row++;
		}

		row++;
		String label[]={"Heute","Morgen","In 2 Tagen","In 3 Tagen"};
		for (int i=0; i<4; i++) {
			int sum=0;
			for (int j=0; j<boxCount; j++) {
				sum+=stat.cardsForDay[j][i];
			}
			grid.add(new Label(label[i]), 0, row);
			grid.add(new Label(""+sum), 1, row++);
		}

		int sum=0;
		for (int j=0; j<boxCount; j++) {
			sum+=stat.cardsInBox[j];
		}
		row++;
		grid.add(new Label("Abgeschlossene Karten: "), 0, row);
		grid.add(new Label(""+stat.finishedCards), 1, row++);
		grid.add(new Label("Karten im Kasten: "), 0, row);
		grid.add(new Label(""+sum), 1, row++);
		grid.add(new Label("Karten in " + Globals.getDefaultCategory() + ": "), 0, row);
		grid.add(new Label(""+stat.currentCategory), 1, row++);
	}



	private String lastCategory = "";
	void updateLessonChart( )
	{
		lessonChart.getData().clear();
		boolean absolute = true; // todo

		String categoryStr = categoryCombo.getSelectionModel().getSelectedItem();
		if (lastCategory.equals(categoryStr)) return; // up to date
		lastCategory = categoryStr;
		ArrayList<LessonStatistics> stat = Database.getDatabase().getLessonStatistics( categoryStr, direction );
		System.out.println("chart : "+stat.size() + "  "+lessonChart.getData().size());

		if (stat.size() == 0) return;

		if ( lessonChart.getData().size() > 0 )
		{   // workaround to start always with the same color: 8 colors are available, use all of them with
			// dummy entries
			int rem = 8 - ((Globals.getBoxDates(direction).length + 1) % 8);
			for (int i = 0; i < rem; i++) {
				lessonChart.getData().add(new XYChart.Series<>());
			}
		}
		lessonChart.getData().clear();

		int numberOfLessons = stat.size();
		lessonChart.setPrefHeight(30*numberOfLessons);

		int total[] = new int[numberOfLessons];
		if (absolute)
		{
			for (int box = Globals.getBoxDates(direction).length; box >= 0 ; box--) {
				XYChart.Series<Number, String> series = new XYChart.Series<>();
				String boxname = box == Globals.getBoxDates(direction).length ? "fertig" : "Kasten "+(box+1);
				series.setName(boxname);
				for (int lesson = 0; lesson < numberOfLessons; lesson++) {
					LessonStatistics lessonRes = stat.get(lesson);
					box = ( box == Card.BOX_FINISHED ) ? Globals.getBoxDates(direction).length + 1 : box;
					int value = lessonRes.countInBox[box];
					total[lesson] += value;
					String left = lessonRes.lessonName;
					series.getData().add(new XYChart.Data<Number, String>(value, left));
				}
				lessonChart.getData().addAll(series);
			}

			int max = 0;
			for (int num : total) {
				if (num > max) max = num;
			}
			xAxis.setTickUnit(2.0);
			xAxis.setMinorTickCount(2);
			xAxis.setUpperBound( (max+1)/2*2 );
			yAxis.setTickLabelRotation(0);
		}
		else // percent values
		{
			for (int lesson = 0; lesson < numberOfLessons; lesson++) {
				LessonStatistics lessonRes = stat.get(lesson);
				for (int box = Globals.getBoxDates(direction).length; box >= 0 ; box--) {
					total[lesson] += lessonRes.countInBox[box];
				}
			}

			for (int box = Globals.getBoxDates(direction).length; box >= 0 ; box--) {
				XYChart.Series<Number, String> series = new XYChart.Series<>();
				String boxname = box == Globals.getBoxDates(direction).length ? "fertig" : "Kasten "+(box+1);
				series.setName(boxname);
				for (int lesson = 0; lesson < numberOfLessons; lesson++) {
					LessonStatistics lessonRes = stat.get(lesson);
					box = ( box == Card.BOX_FINISHED ) ? Globals.getBoxDates(direction).length + 1 : box;
					double value = lessonRes.countInBox[box]*100./total[lesson];
					System.out.println(lessonRes.lessonName+": "+value);
					series.getData().add(new XYChart.Data<Number, String>(value, lessonRes.lessonName));
				}
				lessonChart.getData().addAll(series);
			}

			xAxis.setTickUnit(5.0);
			xAxis.setMinorTickCount(1);
			xAxis.setUpperBound(100);

			yAxis.setTickLabelRotation(0);
		}
	}


	void updateDayChart() {
		dayChart.getData().clear();

		dayChart.setPrefHeight(14*50);

		xAxisDay.setAutoRanging( false );

		dayChart.setTitle("Vokabeln in x Tagen");
		yAxisDay.setLabel("Tage");
		xAxisDay.setLabel("Anzahl der Vokabeln");

		int days = stat.cardsForDay[0].length;
		int total[] = new int[days];
		int max=0;
		int boxCount = Globals.getBoxDates(direction).length;
		for (int day=0; day<days; day++) {
			for (int boxNr = 0; boxNr < boxCount; boxNr++) {
				total[day]+=stat.cardsForDay[boxNr][day];
			}
			if (total[day] > max) max = total[day];
		}

		for (int box = Globals.getBoxDates(direction).length-1; box >= 0 ; box--) {
			XYChart.Series<Number, String> series = new XYChart.Series<>();
			series.setName("Kasten "+(box+1));
			for (int day=0; day<days; day++) {
				series.getData().add(new XYChart.Data<Number, String>(stat.cardsForDay[box][day], ""+day));
			}
			dayChart.getData().addAll(series);
		}

		xAxisDay.setTickUnit(2.0);
		xAxisDay.setMinorTickCount(2);
		xAxisDay.setUpperBound( (max+1)/2*2 );
		yAxisDay.setTickLabelRotation(0);
	}


}
