package gui;
import data.*;

import java.net.URL;
import java.sql.SQLException;
import java.util.*;

import gui.dialogs.CardEditDialog;
import javafx.stage.Stage;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import java.io.*;

import javafx.fxml.Initializable;


/**
 * checks the card box every day for cards that have to be examined
 * cards that have been answered correctly are moved to the next box; incorrectly answered card are moved to to first box
 */
public class BoxLesson extends Stage implements Initializable, AnswerListener
{
	@FXML
	private Button change;
	@FXML
	private Button markOk;
	@FXML
	private Label header;
	@FXML
	private Label subheader;
	@FXML
	private Label question;
	@FXML
	private ImageView image;
	@FXML
	private Pane answerForm;
	@FXML
	private CardAnswer answerFormController; // answerTextForm+Controller
	@FXML
	private FlowPane imagePane;

	private ArrayList<Card> list;
	private Card card; // current Card

	private ArrayList<Card> errors = new ArrayList<Card>();
	private int answered = 0;  // questions that have been answered
	private int wrongAnswers = 0;

	private final int boxes = Globals.getBoxDates( Globals.getDirection() ).length;
	private int answerCount[] = new int[ boxes ];
	private int errorCount[] = new int[ boxes ];
	private int startSize;

	/*
     * Called when FXML file is loaded (via FXMLLoader.load()).  It will execute before the form is shown.
     */
	@Override
	public void initialize(URL url, ResourceBundle resourceBundle)
	{
		imagePane.setMinHeight( Globals.getImageSize().getHeight() );
		imagePane.setMaxHeight( imagePane.minHeightProperty().get() );
		imagePane.setMinWidth( Globals.getImageSize().getWidth() );

		header.setText("Vokabelkasten (noch "+startSize+"/"+startSize+" Vokabeln)");
		subheader.setText("Fehler: 0");

		if ( Globals.getQuestionType() == Globals.UserQuestionType.Choice ) {
			markOk.setVisible( false );
		}
		change.setOnAction( e-> changeCard() );
		markOk.setOnAction( e-> markCardOk() );

		answerFormController.setAnswerListener( this );
		examine();
	}

	public BoxLesson(ArrayList<Card> list) {
		this.list = list;
		startSize = list.size();

		// create new window and fill it
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("BoxLesson.fxml"));
		fxmlLoader.setController(this);
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
			setScene(new Scene(parent));
			this.setTitle("Vokabelkasten");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void markCardOk() {
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle("Bestätigung");
		alert.setContentText("Als korrekt markieren?");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent() && result.get() == ButtonType.OK) {
			String answer = answerFormController.getAnswer();
			LogEntry log = new LogEntry(LogEntry.LOG_UNDO, card.getQuestion()+LogEntry.SEP+card.getAnswer()+LogEntry.SEP+answer);
			log.write();
			Database.getDatabase().revertCard(card, card.getBoxNr());
			nextQuestion();
		}
	}

	private void changeCard() {
		CardEditDialog dialog = new CardEditDialog(card);
		dialog.showAndWait();
	}

	void examine()
	{
		long seed = System.nanoTime();
		Collections.shuffle(list, new Random(seed));

		card = list.remove(0);
		setQuestion( card );
	}

	public void setQuestion(Card card) {
		markOk.setVisible(false); change.setVisible(false);
		this.card = card;
		answerFormController.setQuestion( card );
		String questionText = "";
		if (card.getBoxNr() >= 0) questionText = "["+(card.getBoxNr()+1)+"] ";
		questionText += card.getQuestion();
		question.setText(questionText);
		if ( ( Globals.getDirection() == Direction.FRONT ) && (card.getImage() != null) )
		{
			javafx.scene.image.WritableImage img = javafx.embed.swing.SwingFXUtils.toFXImage(card.getImage(), null);
			image.setImage(img);
		}
		else
			image.setImage(null);
	}

	public Result getResult() {
		return new Result(answered, wrongAnswers, wrongAnswers);
	}


	/** show next question or quit, if current one is last question */
	private void nextQuestion() {
		if (!list.isEmpty()) // take next card
		{
			card = list.remove(list.size() - 1);
			setQuestion(card);
			header.setText("Vokabelkasten (noch "+(list.size()+1)+"/"+startSize+" Vokabeln)");
			subheader.setText("Fehler: "+wrongAnswers);
		} else { // lesson is done"
			close();
			writeLog();
		}
	}

	/** called after question has been answered
	 *  result is stored and new question is prepared here
	 */
	@Override
	public void answerFinished() {
		CardAnswer controller = answerFormController;

		ExitStatus status = answerFormController.getStatus();

		if ( status == ExitStatus.ANSWERED ) {
			if (!controller.isCorrect()) {
				markOk.setVisible(true);
			}
			change.setVisible(true);

			if ( card.getImage() != null )
			{
				javafx.scene.image.WritableImage img = javafx.embed.swing.SwingFXUtils.toFXImage(card.getImage(), null);
				image.setImage(img);
			}

			return; // not yet confirmed
		}

		if ( status != ExitStatus.ABORTED)
		{
			try {
				answered++;

				boolean answerIsCorrect = controller.isCorrect();
				Database.getDatabase().cardIsExamined(card, answerIsCorrect);

				int boxNr = card.getBoxNr() < boxes ? card.getBoxNr() : boxes - 1;
				answerCount[boxNr]++;
				if (!answerIsCorrect)
				{
					wrongAnswers++;
					if (!errors.contains(card)) {
						errors.add(card);
						errorCount[boxNr]++;
					}
				}
				nextQuestion();
			} catch (SQLException e) {
				e.printStackTrace();
				showError();
			}
		}
		if ( status != ExitStatus.ANSWERED_AND_CONFIRMED) { // lesson aborted
			close();
			list.add( card ); // not yet answered
			writeLog();
		}
	}

	private void showError() {
		Alert alert = new Alert(Alert.AlertType.ERROR);
		DialogPane dialogPane = alert.getDialogPane();
		dialogPane.getStylesheets().add(getClass().getResource("JCardBox.css").toExternalForm());
		dialogPane.getStyleClass().add("jcardbox");

		alert.setTitle("Fehler");
		alert.setHeaderText("Fehler beim Schreiben in die Datenbank");
		alert.showAndWait();
	}

	private void writeLog() {
		// write log
		if ( startSize-(list.size()) > 1 ) {
			LogEntry logEntry = new LogEntry( startSize, list.size(), answerCount, errorCount );
			logEntry.write();
		}
	}

}
