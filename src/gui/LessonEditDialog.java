package gui;

import data.*;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * creates a panel which allows the user to select a category and shows all available lessons
 */
public class LessonEditDialog extends Stage implements Initializable
{
    @FXML
    private Tab lessonEditTab;
    @FXML
    private Pane lessonOverview;
    @FXML
    private LessonOverview lessonOverviewController;
    @FXML
    private Pane lessonCardEdit;
    @FXML
    private LessonCardEdit lessonCardEditController;

    public LessonEditDialog() {
        setTitle("Lektionen erstellen und bearbeiten");

        // create new window and fill it
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("LessonEditDialog.fxml"));
        fxmlLoader.setController(this);
        Parent parent = null;
        try {
            parent = fxmlLoader.load();
            setScene(new Scene(parent));
            //this.initModality(Modality.APPLICATION_MODAL);
            //this.initStyle(StageStyle.TRANSPARENT);
            //this.setAlwaysOnTop(true);
            this.setWidth( 1000 );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        lessonOverviewController.getSelectedLessonProperty().addListener(e-> selectedLessonChanged() );
        lessonEditTab.setOnSelectionChanged( event -> lessonTabChanged() );

        lessonCardEditController.getCloseButton().setOnAction( event ->  close() );
        lessonOverviewController.getCloseButton().setOnAction( event ->  close() );

        selectedLessonChanged();
    }

    private void selectedLessonChanged() {
        Lesson lesson = lessonOverviewController.getSelectedLessonProperty().get();
        if ( lesson == null ) return;
        lesson.load( true );
        if ( lesson.getName().equals("Neu") )
            lessonEditTab.setDisable( true );
        else
        {
            lessonEditTab.setDisable( false );
            if ( lesson.getCards().size() == 0 ) // empty lesson -> directly switch to edit mode
                lessonEditTab.getTabPane().getSelectionModel().select( lessonEditTab );
        }
    }

    private void lessonTabChanged() {
        if (lessonEditTab.isSelected()) {
            Lesson lesson = lessonOverviewController.getSelectedLessonProperty().get();
            lessonCardEditController.setLesson( lesson );
        }
    }
}