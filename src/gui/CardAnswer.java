package gui;

import data.*;
import javafx.fxml.*;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

interface AnswerListener {
    void answerFinished(); // called if question has been answered or aborted
}

enum ExitStatus {ANSWERED, ANSWERED_AND_CONFIRMED, ANSWERED_AND_ABORTED, ABORTED };

interface CardAnswerInterface {
    ExitStatus getStatus();
    boolean isCorrect();
    void setQuestion( Card c );
    void setAnswerListener(AnswerListener q);
    String getAnswer();
}
/**
 */
public class CardAnswer implements Initializable, CardAnswerInterface
{
    @FXML
    private Pane answerText;
    @FXML
    private CardAnswerText answerTextController; // answerText+Controller
    @FXML
    private Pane answerChoice;
    @FXML
    private CardAnswerChoice answerChoiceController; // answerText+Controller
    @FXML

    private CardAnswerInterface answers[];
    private Pane panes[];
    private int current;

    @Override
    public void initialize(URL url, ResourceBundle ressourceBundle)
    {
        answers = new CardAnswerInterface[]{answerTextController, answerChoiceController};
        panes = new Pane[]{answerText, answerChoice};
        setQuestionType();
    }

    private void setQuestionType() {
        if ( Globals.getQuestionType() == Globals.UserQuestionType.Choice ) {
            panes[0].setVisible( false );
            panes[1].setVisible( true );
            current = 1;
        } else {
            panes[0].setVisible( true );
            panes[1].setVisible( false );
            current = 0;
        }
    }

    public ExitStatus getStatus() { return answers[current].getStatus(); }
    public boolean isCorrect() { return answers[current].isCorrect(); }
    public void setQuestion( Card c ) {answers[current].setQuestion(c); }
    public void setAnswerListener(AnswerListener q) { // listener gets called if question has been answered or aborted
        answers[current].setAnswerListener( q );
    }
    public String getAnswer() {
        return answers[current].getAnswer();
    }
}

