package gui;
import data.*;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;

public class LogView extends TableView<Row>
{
    private TableColumn<Row, String> dateCol = new TableColumn<>("Datum");
    private TableColumn<Row, String> logCol  = new TableColumn<>("Meldung");
    private ObservableList<Row> data = FXCollections.observableArrayList();;

    public LogView() {
        dateCol.setCellValueFactory(cellData -> cellData.getValue().dateProperty() );
        logCol.setCellValueFactory(cellData -> cellData.getValue().logProperty()) ;

        ArrayList<LogEntry> logs = Database.getDatabase().readLog(7);
        for (LogEntry entry : logs) {
            Row row = new Row(entry);
            data.add(0, row);
        }
        this.getColumns().add(dateCol);
        this.getColumns().add(logCol);
        this.setItems( data );

        this.setColumnResizePolicy( CONSTRAINED_RESIZE_POLICY );
        //dateCol.prefWidthProperty().bind(widthProperty().multiply(0.1));
        setColumnResizePolicy((param) -> true ); // use column size
        Platform.runLater(() -> customResize(this));
    }

    public void update() {
        data.clear();
        ArrayList<LogEntry> logs = Database.getDatabase().readLog(7);
        for (LogEntry entry : logs) {
            Row row = new Row(entry);
            data.add(0, row);
        }
    }

    public void customResize(TableView<?> view) {

        AtomicLong width = new AtomicLong();
        view.getColumns().forEach(col -> {
            width.addAndGet((long) col.getWidth());
        });
        double tableWidth = view.getWidth();

        if (tableWidth > width.get()) {
            view.getColumns().forEach(col -> {
                col.setPrefWidth(col.getWidth()+((tableWidth-width.get())/view.getColumns().size()));
            });
        }
    }
}

class Row // javafx wrapper for data.LogView
{
    private final StringProperty date;
    private final StringProperty log;

    public Row( LogEntry entry ) {
        date = new SimpleStringProperty( ""+entry.getDate() );
        log = new SimpleStringProperty( entry.getMessage() );
    }
    public StringProperty dateProperty() {
        return date;
    }
    public StringProperty logProperty() {
        return log;
    }
}
