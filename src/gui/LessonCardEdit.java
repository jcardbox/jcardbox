package gui;

import data.*;

import gui.dialogs.CardDeleteDialog;
import javafx.beans.property.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * creates a panel which allows the user to select a category and shows all available lessons
 */
public class LessonCardEdit implements Initializable
{
    @FXML
    private TableView lessonView;
    @FXML
    private LessonView lessonViewController; // class+Controller
    @FXML
    private Pane cardEdit;
    @FXML
    private CardEdit cardEditController; // class+Controller
    @FXML
    private Button delete;
    @FXML
    private Button clear;
    @FXML
    private Button ok;
    @FXML
    private Button back;

    private Lesson lesson;
    private SimpleObjectProperty<Card> selectedCard;

    public Button getCloseButton() {return back;}

    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        selectedCard = lessonViewController.getCardProperty();
        selectedCard.addListener( (o, oldCard, newCard) -> cardEditController.setCard( newCard ) );

        delete.setOnAction( e->deleteCard() );
        clear.setOnAction( e->clear() );
        ok.setOnAction( e-> cardToDatabase() );
    }

    public void setLesson( Lesson newLesson )
    {
        lesson = newLesson;
        if (lesson != null) {
            newLesson.load( true ); // also load multimedia contents
            lessonViewController.setLesson(newLesson);
            cardEditController.setHeader( lesson.getName() + " ("+lesson.getCards().size() + " Vokabeln)");
        }
    }

    private void clear() {
        lessonViewController.clearSelection();
        cardEditController.clear();
    }

    private void deleteCard()
    {
        Card selectedCard = cardEditController.getCard();
        if ( selectedCard == null || lesson.getCards().size() == 0 ) return;

        ArrayList<Lesson> lessons = new ArrayList<>();
        lessons.add( lesson );
        //todo for ( Card selectedCard : cards )
        selectedCard.setLessons( lessons );

        new CardDeleteDialog( selectedCard ).showAndWait();

        lesson = new Lesson( lesson.getName(), lesson.getCategory(), lesson.getID(), lesson.getCreationDate() );
        lesson.load( true );
        lessonViewController.setLesson( lesson );
    }
    private void updateTable() {
        //lessonView.updateTable();
        cardEditController.setHeader(lesson.getName() + " (" + lesson.getCardCopy().size() + " Vokabeln)");
    }

    private void cardToDatabase() {
        Card card = cardEditController.getCard();

        if (card.isValid())
        {
            Card selected = this.selectedCard.get();
            if (selected == null)
                appendCard(card);
            else
                changeCard(card);
            lessonViewController.reload();
        }
    }

    private void changeCard( Card card )
    {
        Card selected = this.selectedCard.get(); // selected selectedCard from list;

        Database.getDatabase().changeCard(selected, card.getFront(), card.getBack(), card.getImage(), card.getAudio() );
        cardEditController.clear();
   }

    private void appendCard( Card card ) {
        ArrayList<Card> cards = lesson.getCards();

        if (card.isValid())
        {
            if (lesson.getID() == -1) { // new lesson
                Database.getDatabase().addLesson( lesson );
            }

            if (card.getDatabaseID() >= 0) { // selectedCard exists is database
                Database.getDatabase().changeCard(card, card.getFront(), card.getBack(), card.getImage(), card.getAudio());
            } else { // new selectedCard
                Database.getDatabase().addCard(card);
            }

            boolean cardInLessonExists = false;
            for (Card lcard : cards ) {
                if ( lcard.getDatabaseID() == card.getDatabaseID() )
                    cardInLessonExists = true;
            }
            if ( !cardInLessonExists )
            {
                Database.getDatabase().addCardToLesson(card.getDatabaseID(), lesson.getID());
                cards.add(card);
                cardEditController.setHeader( lesson.getName() + " ("+lesson.getCards().size() + " Vokabeln)");
            }
            cardEditController.clear();
        }
    }




}
