package gui;

import data.Database;
import data.Lesson;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ResourceBundle;

/**
 */
public class NewLesson implements Initializable
{
    @FXML
    private Button ok;
    @FXML
    private TextField name;
    @FXML
    private Label error;

    private String category;
    private StringProperty lessonName= new SimpleStringProperty();

    public void setCategory( String category ) {
        this.category = category;
    }

    public StringProperty getLessonNameProperty() { return lessonName; }

    @Override
    public void initialize(URL url, ResourceBundle ressourceBundle)
    {
        ok.setOnAction( e->setLessonName() );
    }

    private void setLessonName() {
        String lessonName = name.getText().trim();
        if (lessonName.isEmpty()) return;

        boolean exists = false;
        try {
            exists = Database.getDatabase().lessonExists( new Lesson(lessonName, category) );
        } catch (java.sql.SQLException e) {
            System.err.println(e);
        }

        if ( exists ) {
            error.setText("Fehler: Lektion ist bereits vorhanden");
            error.setTextFill(Color.web("#ff0000"));
        }
        else {
            this.lessonName.set( lessonName );
        }
    }
}
