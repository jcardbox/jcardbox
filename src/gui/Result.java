package gui;
import javafx.scene.control.*;
import javafx.scene.control.Alert.*;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Optional;

public class Result
{
	private int answers;    // questions that have been checkAnswer excluding duplicates
	private int uniqErrors; // number of wrong answers excluding duplicates
	private int errors; // number of wrong answers including duplicates

	/** number of questions tat have been checkAnswer, no duplicates */
	public int getAnwerCount() { return answers; }
	/** number of answers that have been correct at the first try */
	public int getInitialCorrect() { return answers - uniqErrors; }
	///** number of wrong answers excluding duplicates */
	//public int getUniqErrors() { return uniqErrors; }
	/** number of wrong answers including duplicates */
	public int getErrors() { return errors; }

	public Result(int answers, int uniqErrors, int errors) {
		this.answers = answers; this.uniqErrors = uniqErrors; this.errors = errors;
	}

	void showResultDialog(String lessonName) {
		Alert alert = new Alert(AlertType.INFORMATION);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(getClass().getResource("JCardBox.css").toExternalForm());
        dialogPane.getStyleClass().add("jcardbox");

        alert.setTitle("Ergebnis");
        alert.setHeaderText("Lektion:" + lessonName);
        alert.setContentText(String.format("%d Vokabeln wurden abgefragt\n",this.getAnwerCount() ) +
                String.format("%d wurden beim 1. Mal richtig beantwortet\n",this.getInitialCorrect()) +
                String.format("%d Fehler\n",this.getErrors() ) );

        if ( this.getErrors() == 0 ) {
            alert.setContentText(String.format("Alle %d Vokabeln korrekt!",this.getAnwerCount() ) );
            Image icon = new Image(getClass().getResourceAsStream("/res/smilie.png"));
            alert.setGraphic(new ImageView(icon));
        }

        alert.showAndWait();
    }

    boolean showResultDialogAskLearn( String lessonName ) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(getClass().getResource("JCardBox.css").toExternalForm());
        dialogPane.getStyleClass().add("jcardbox");

        ButtonType buttonLearn = new ButtonType( "Lernen" );
        ButtonType buttonCancel = new ButtonType("Ende", ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonLearn, buttonCancel);
        alert.setTitle("Ergebnis");
        alert.setHeaderText("Lektion:" + lessonName);
        alert.setContentText(String.format("%d Vokabeln wurden abgefragt\n",this.getAnwerCount() ) +
                String.format("%d wurden beim 1. Mal richtig beantwortet\n",this.getInitialCorrect()) +
                String.format("%d Fehler\n",this.getErrors() ) );

        if ( this.getErrors() == 0 ) {
            alert.setContentText(String.format("Alle %d Vokabeln korrekt!",this.getAnwerCount() ) );
            Image icon = new Image(getClass().getResourceAsStream("/res/smilie.png"));
            alert.setGraphic(new ImageView(icon));
        }

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonLearn) {
            return true;
        }
        return false;
    }
}