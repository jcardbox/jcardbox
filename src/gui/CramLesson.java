
package gui;
import data.*;

import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * checks the card box every day for cards that have to be examined
 * cards that have been answered correctly are moved to the next box; incorrectly answered card are moved to to first box
 */
public class CramLesson extends Stage implements Initializable, AnswerListener
{
	@FXML
	private Label header;
	@FXML
	private Label subheader;
	@FXML
	private Label question;
	@FXML
	private ImageView image;
	@FXML
	private Pane answerForm;
	@FXML
	private CardAnswer answerFormController; // answerForm+Controller
	@FXML
	private FlowPane imagePane;

	private CramMethod cramMethod;
	private Lesson lesson;
	private ArrayList<Card> list;
	private Card card; // current Card
	private ArrayList<Card> errors = new ArrayList<Card>();
	private ArrayList<Card> answered = new ArrayList<Card>();
	private int answerCount;  // questions that have been checkAnswer
	private int errorCount; // wrong answers including duplicates
	private int lessonSize; // initial number or cards

	public CramLesson(Lesson lesson, CramMethod errorMethod) {
		this.setTitle("Lernen");
		this.lesson = lesson;
		this.list = lesson.getCardCopy();
		cramMethod = errorMethod;

		lessonSize = list.size();

		// create new window and fill it
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("CramLesson.fxml"));
		fxmlLoader.setController(this);
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
			setScene(new Scene(parent));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle)
	{
		imagePane.setMinHeight( Globals.getImageSize().getHeight() );
		imagePane.setMaxHeight( imagePane.minHeightProperty().get() );
		imagePane.setMinWidth( Globals.getImageSize().getWidth() );
		//image.setFitHeight( Globals.getImageSize().getHeight() );
		//image.setFitWidth( Globals.getImageSize().getWidth() );
		header.setText(lesson.getName()+" (noch "+lessonSize+"/"+lessonSize+" Vokabeln)");
		subheader.setText("Fehler: 0");

		answerFormController.setAnswerListener( this );
		cram();
	}

	public Result getResult() {
		// get number of uniq questoins that have been asked
		return new Result(answered.size(), errors.size(), errorCount);
	}

	private void cram()
	{
		long seed = System.nanoTime();
		Collections.shuffle(list, new Random(seed));

		card = list.remove(0);
		setQuestion( card );
	}

	public void setQuestion(Card card) {
		this.card = card;
		answerFormController.setQuestion( card );
		String questionText = "";
		if (card.getBoxNr() >= 0) questionText = "["+(card.getBoxNr()+1)+"] ";
		questionText += card.getQuestion();
		question.setText(questionText);
		if ( ( Globals.getDirection() == Direction.FRONT ) && (card.getImage() != null) )
		{
			javafx.scene.image.WritableImage img = javafx.embed.swing.SwingFXUtils.toFXImage(card.getImage(), null);
			image.setImage(img);
		}
		else
			image.setImage(null);
	}

	@Override
	public void answerFinished() {
		CardAnswer controller = answerFormController;

		ExitStatus status = answerFormController.getStatus();

		if ( status == ExitStatus.ANSWERED ) {
			if ( card.getImage() != null )
			{
				javafx.scene.image.WritableImage img = javafx.embed.swing.SwingFXUtils.toFXImage(card.getImage(), null);
				image.setImage(img);
			}

			return; // not yet confirmed
		}
		if ( status != ExitStatus.ABORTED)
		{
			boolean answerIsCorrect = controller.isCorrect();

			if (!answered.contains( card )) {
				answered.add(card);
			}
			answerCount++;
			if ( !answerIsCorrect )  // put wrong answer back into the list
			{
				cramMethod.handleError(list, card);
				errorCount++;
				if (!errors.contains( card )) {
					errors.add( card );
				}
			}
			if ( !list.isEmpty() ) // take next card
			{
				card = list.remove( list.size() - 1 );
				setQuestion( card );
			}
			else { // lesson is done
				close();
				finished();
			}
		}
		if ( status == ExitStatus.ABORTED) { // lesson aborted
			list.add( card ); // current card hasn't been questioned
			close();
			finished();
		}
		header.setText("Lektion"+" "+lesson.getName()+" (noch "+(list.size()+1)+"/"+lessonSize+" Vokabeln)");
		subheader.setText("Fehler: "+errorCount);

	}

	/** dialog is finished */
	private void finished()
	{
		// write results into log
		if ( answerCount > 0 )
		{
			int questionsAsked = lessonSize;
			int firstAnswerCorrect = lessonSize - errors.size();
			if ( answerFormController.getStatus() == ExitStatus.ABORTED ) { // lesson aborted
				int notYetCorrectlyAnswered = list.size();
				questionsAsked -= notYetCorrectlyAnswered;
				int notYetAnswered = 0;
				for ( Card elem : list ) {
					if (!errors.contains(elem)) notYetAnswered++;
				}
				firstAnswerCorrect -= notYetAnswered;
			}

			LogEntry entry = new LogEntry(lesson.getName(), lessonSize, questionsAsked,
					firstAnswerCorrect, errorCount, errors.size());
			entry.write();
		}

		// save errors into new lesson, if more than one error and lesson isn't already an error lesson
		if ( (errors.size() > 1 ) &&
				! lesson.getCategory().equals( Lesson.ERROR_CRAM_CATEGORY ) &&
				! lesson.getCategory().equals( Lesson.ERROR_BOX_CATEGORY )	)
		{
			SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String dateStr = simpleFormat.format(date);
			String lessonName = dateStr + "_"+lesson.getName();
			Lesson lesson = new Lesson( lessonName, Lesson.ERROR_CRAM_CATEGORY, errors );

			if ( lesson.getID() < 0 ) Database.getDatabase().loadLessonInfo(lesson);
			if ( lesson.getID() < 0 ) {
				Database.getDatabase().addLesson(lesson);
			}
			else { // add new cards to existing lesson
				for( Card card : errors )
				{
					try {
						if (!Database.getDatabase().cardInLessonExists(lesson.getID(), card.getDatabaseID()))
						{
							Database.getDatabase().addCardToLesson(card.getDatabaseID(), lesson.getID());
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} // write error lesson
	}

}
