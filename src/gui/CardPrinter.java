package gui;

import data.*;
import java.util.ArrayList;
import java.util.List;
import javafx.print.*;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.Window;


/**
 * Created by anke on 7/5/15.
 */
public class CardPrinter {
    private List<Lesson> lessons;
    private ArrayList<Integer> lessonOnPage = new ArrayList<>(); // lesson nr which starts on the given page
    private ArrayList<Integer> cardOnPage = new ArrayList<>();

    private static final int padX = 20;
    private static final int padY = 20;
    private static final int rowHeight = 15;
    private static final int headerHeight = 30;
    private static final int fontsize = (int) (rowHeight - 3 + .5);
    private static final int columns = 2;

    private Canvas canvas = new Canvas();
    private Window parent;

    public static void configure() {
        //todo: configure columns, fontsize, page format...
    }

    public CardPrinter( List<Lesson> lessons, Window parent )
    {
        this.lessons = new ArrayList<>();
        if (lessons.size() > 1) { // reverse order, the most recent entry of the given list is on 1st position
            for ( Lesson lesson : lessons )
                this.lessons.add( 0, lesson );
        }
        print();

        /*
        PageFormat format = pjob.
        Paper paper = new Paper();
        paper.setSize(595, 842);
        paper.setImageableArea(43, 43, 509, 756);
        format.setPaper(paper);
        pjob.setPrintable(this, format);

        try {
            boolean doPrint = pjob.printDialog();
            if (doPrint) {
                calculateLayout(format);
                pjob.print();
            }
        } catch ( PrinterException e ) {
            e.printStackTrace();
        }*/
    }

    public void calculateLayout(int height, int width)
    {
        lessonOnPage.add(0); // first page starts with first lesson and first card
        cardOnPage.add(0);

        float h = height; // remaining space in y direction on this page
        for ( int lessonIdx = 0; lessonIdx < lessons.size(); lessonIdx ++)
        {
            Lesson lesson = lessons.get( lessonIdx );
            h -= headerHeight; // lesson header line

            int cards = lesson.getCards().size();
            int rows = (int) (h / rowHeight); // remaining rows on this page
            int cardIdx = 0;
            while ( cards > rows ) { // current lesson dosn't fit on this page
                cardIdx += rows;
                lessonOnPage.add(lessonIdx);
                cardOnPage.add(cardIdx);
                h = height;
                rows = (int) (h / rowHeight); // remaining rows on this page
                cards -= rows;
            }
            h -= (cards) * rowHeight;
        }
    }


    /**
     *
     */
    public void print() {
        /*
        Printer printer = Printer.getDefaultPrinter();
        PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.DEFAULT);
        double width = pageLayout.getPrintableWidth();
        double height = pageLayout.getPrintableHeight();
        */

        Printer first = Printer.getDefaultPrinter();
        if (first == null) {
            first = Printer.getAllPrinters().iterator().next();
        }
System.out.println("printer "+first);
        Node node = new Circle(100, 200, 200);
        PrinterJob job = PrinterJob.createPrinterJob(first);
       job.showPrintDialog( parent );
        if (job != null) {
            System.out.println("printer1 "+job.getJobStatus());
            boolean success = job.printPage(node);
            System.out.println("printer2 "+job.getJobStatus());
            if (success) {
                System.out.println("job success");
                job.endJob();
            }
            else {
                System.out.println("not successful");

            }
        }
        if (1==1) return;
        if (job != null) {
            job.showPrintDialog( parent );
            draw();
            boolean success = job.printPage(node);
            if (success) {
                job.endJob();
            } else {
                System.err.println("not successfull");
            }
        }
        else {
            System.err.println("print " +Printer.getAllPrinters().size());
            System.err.println("job null");
        }
    }

    public void draw() {
        int height = 800;
        int width = 600;
        canvas.setHeight( height );
        canvas.setWidth( width );
        calculateLayout(800, 600);

        /*
        if ( page >= lessonOnPage.size() ) {
            return NO_SUCH_PAGE;

        int startY = (int) pf.getImageableY()+ padY;
        int endY = (int) (pf.getImageableY()+pf.getImageableHeight())- padY;
        int colWidth = (int) (pf.getImageableWidth() - 2*padX) / columns;
        }*/

        int startY = 20;
        int colWidth = 400;
        int endY = 700;

        int startLesson = 0; //;lessonOnPage.get( page );
        int cardIdx = 0; //cardOnPage.get( page );

        GraphicsContext gc = canvas.getGraphicsContext2D();
        //     gc.translate(pf.getImageableX(), pf.getImageableY());
        Font plainFont = new Font("Serif", fontsize);
        Font boldFont = new Font("Serif", fontsize);

        gc.moveTo(padX, 30 );
        gc.lineTo( 400,400 );

        gc.setFont(plainFont);
        for (int lessonIdx = startLesson; lessonIdx < lessons.size(); lessonIdx++ )
        {
            Lesson lesson = lessons.get(lessonIdx);
            if (cardIdx == 0) { // new lesson begins
                //g2d.setFont(boldFont)
                gc.fillText(lesson.getName(), padX, startY + headerHeight - rowHeight );
                startY += headerHeight;
                //g2d.setFont(plainFont);
            }
            // contents of one lesson
            while ( (cardIdx < lesson.getCards().size()) && ( startY < endY) ) {
                Card card = lesson.getCards().get(cardIdx);
                // text, todo: handle text that doesn't fit in one column
                gc.fillText(card.getFront(), padX, startY);
                gc.fillText(card.getBack(), padX + colWidth, startY);

                // lines
                int addY = 3;
                int ypos = startY+addY;
                gc.moveTo(padX, ypos );
                gc.lineTo( padX+colWidth*columns, ypos );
                for (int i=1; i<columns; i++) {
                    int xline = ( padX + i*colWidth ) - 5;
                    gc.moveTo( xline-5, startY - rowHeight + addY );
                    gc.lineTo( xline-5, startY + addY );
                }

                startY += rowHeight;
                cardIdx++;
            }
            cardIdx = 0; // next lesson or next page starts
        }
    }

    public static void main(String args[]) {
        final ArrayList<Card> list = new ArrayList<Card>();
        for (int i=0; i<10; i++)
        {
            Card vok = new Card();
            vok.setFront("AÄÖÜ 1234567890 12345"+i);
            vok.setBack("ä     \u00c4  1234567890 123456"+i);
            list.add(vok);
        }
        Lesson lesson = new Lesson("test1", "klasse4", list);
        ArrayList<Lesson> lessons = new ArrayList<>();
        lessons.add(lesson);
        lessons.add(lesson);
        new CardPrinter(lessons, null);
    }
}
