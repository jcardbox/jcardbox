package gui;

import data.Database;
import data.Globals;
import gui.dialogs.DatabaseSettingsDialog;
import gui.dialogs.DefaultDialogs;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.*;

/**
 * -Ddir=/absolutePathToDatabase
 * -Dlanguage=english
 */

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        boolean showSettings = false;
        if ( new File(Globals.getInstallationRoot()).exists() ) {
            try {
                Database.connectToDatabase();
            } catch (Exception e) {
                e.printStackTrace();
                DefaultDialogs.showExceptionDialog("Fehler", "Keine Verbindung zur Datenbank", e);
                showSettings = true;
            }
        } else {
            showSettings = true;
        }

        if ( showSettings )
        {
            DatabaseSettingsDialog d = new DatabaseSettingsDialog();
            d.showAndWait();
            if (!d.isCancelled())
                try {
                    Database.connectToDatabase();
                }
                catch (Exception e) {
                    e.printStackTrace();
                    DefaultDialogs.showExceptionDialog("Fehler", "Keine Verbindung zur Datenbank", e);
                }
            else {
                System.exit(-1);
            }
        }
        if (Database.getDatabase() == null) System.exit(-1);

        String file = Globals.getInstallationRoot() + File.separator + "err.txt";
        Main.redirectSystemErr(file);

        Database.getDatabase().updateDB();
        Globals.initializeFromDatabase();


        Parent root = FXMLLoader.load(getClass().getResource("JCardBox.fxml"));
        String name = Globals.getUserName() != null ?  " : "+Globals.getUserName() : "";

        primaryStage.setTitle("JCardBox"+name);
        primaryStage.setScene(new Scene(root));
        primaryStage.centerOnScreen();
        primaryStage.show();
    }

    public static void main(String[] args) {
        Platform.setImplicitExit(true);
        try {
            launch(args);
        } catch (Exception e) {
            e.printStackTrace();
            if (Database.getDatabase() != null )
                Database.getDatabase().closeConnection();
        }
    }

    private static void redirectSystemErr( String outfile )
    {
        try {
            PrintStream ps = new PrintStream(new FileOutputStream( new File(outfile)));
            MultiOutputStream multi = new MultiOutputStream(ps, System.err);
            System.setErr(new PrintStream(multi));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

/** allows to write to multiple streams, e.g. to file and stderr */
class MultiOutputStream extends OutputStream
{
    OutputStream[] outputStreams;

    public MultiOutputStream(OutputStream... outputStreams)
    {
        this.outputStreams= outputStreams;
    }
    public void write(int b) throws IOException
    {
        for (OutputStream out: outputStreams)
            out.write(b);
    }
    public void write(byte[] b) throws IOException
    {
        for (OutputStream out: outputStreams)
            out.write(b);
    }
    public void write(byte[] b, int off, int len) throws IOException
    {
        for (OutputStream out: outputStreams)
            out.write(b, off, len);
    }
    public void flush() throws IOException
    {
        for (OutputStream out: outputStreams)
            out.flush();
    }
    public void close() throws IOException
    {
        for (OutputStream out: outputStreams)
            out.close();
    }
}
