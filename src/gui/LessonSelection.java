package gui;

import data.Database;
import data.Globals;
import data.Lesson;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * creates a panel which allows the user to select a category and shows all available lessons
 */
public class LessonSelection implements Initializable
{
    @FXML
    private ComboBox<String> combo;
    @FXML
    private ListView<Lesson> list; // lessonInterface in the given category

    // ==============================================================================================
    public static final LessonInterface showAllLessons = new AllLessons();
    private ObservableList<Lesson> listModel; // model for the ListView above
    private LessonInterface lessonInterface = showAllLessons;
    private boolean multiple = false;
    private ReadOnlyObjectProperty<Lesson> selectedLesson;
    private ObservableList<Lesson> selectedLessons;

    public String getCategory() {
        return combo.getValue();
    }

    @Override
    public void initialize(URL url, ResourceBundle ressourceBundle)
    {
        if (!multiple) {
            // todo list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        } else {
            // list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        }
        ObservableList<String> categories = Globals.getCategoryList();
        combo.setItems( categories );
        combo.setOnAction( e-> reload() );
        combo.setValue( Globals.getDefaultCategory() );

        listModel = FXCollections.observableArrayList();
        list.setItems( listModel );
        list.getSelectionModel().select(0);
        selectedLesson = list.getSelectionModel().selectedItemProperty();
        selectedLessons = list.getSelectionModel().getSelectedItems();

        reload();
    }

    public void setLessonInterface( LessonInterface lessons )
    {
        this.lessonInterface = lessons;
    }
    public void enableMultipleSelection( boolean multiple )
    {
        this.multiple = multiple;
    }

    /** reload database values */
    public void reload()
    {
        int idx = list.getSelectionModel().getSelectedIndex();
        if ( idx < 0 ) idx=0;
        listModel.clear();
        if (combo.getValue().equals(Lesson.ERROR_BOX_CATEGORY_LABEL)) { // box errors
            listModel.add(0, Lesson.boxLesson);
        } else {
            String category = combo.getValue();
            ArrayList<Lesson> lessons = lessonInterface.getLessons( category );
            for ( Lesson lesson : lessons )
                listModel.add( 0, lesson ); // add in reverse order, laterst first
        }
        list.getSelectionModel().select(idx);
    }

    public void addLesson( int index, Lesson lesson) {
        listModel.add( index, lesson );
        list.getSelectionModel().select( index );
    }

    public Lesson getSelectedLesson() {
        return list.getSelectionModel().getSelectedItem();
    }

    public ObservableList<Lesson> getSelectedLessons() {
        return selectedLessons;
    }


    public ReadOnlyObjectProperty<Lesson> getSelectedLessonProperty() {
        return selectedLesson;
    }

}

interface LessonInterface {
    ArrayList<Lesson> getLessons( String category );
}

/** used for box view to show lessons which have not been saved yet */
class UnsavedBoxLessons implements LessonInterface {
    public ArrayList<Lesson> getLessons( String category ) {
        return Database.getDatabase().getUnsavedBoxLessons(category, Globals.getDirection() );
    }
}
class AllLessons implements LessonInterface {
    public ArrayList<Lesson> getLessons( String category ) {
        return Database.getDatabase().getAvailableLessons(category);
    }
}

