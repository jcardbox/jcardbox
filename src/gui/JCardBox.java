package gui;
import data.*;
import java.sql.SQLException;
import java.util.*;

import gui.dialogs.*;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import java.io.*;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

import static javafx.geometry.Pos.CENTER;


/**
 * -Ddir=/absolutePathToDatabase
 * -Dlanguage=english
 */
public class JCardBox
{
    @FXML
    private MenuBar menuBar;
    @FXML
    private Menu viewMenu;
    @FXML
    private Menu settingsMenu;
    @FXML
    private Menu helpMenu;
    @FXML
    private Button box1;
    @FXML
    private Button box2;
    @FXML
    private Button cram1;
    @FXML
    private Button cram2;
    @FXML
    private Button edit;
    @FXML
    private Button lessonToBox;
    @FXML
    private Button exit;
    @FXML
    private VBox ledLine;

    private Button buttons[][]; // buttons with direction string (language -> foreign language)

    private Stage currentStage;

    @FXML
    private void initialize()
    {
        buttons = new Button[][]{{box1, box2},{cram1,cram2}};

        String dir1 = Globals.getLanguageLabel1() +"->"+ Globals.getLanguageLabel2();
        String dir2 = Globals.getLanguageLabel2() +"->"+ Globals.getLanguageLabel1();

        box1.setText( dir1 );
        box2.setText( dir2 );
        cram1.setText( dir1 );
        cram2.setText( dir2 );

        box1.setOnAction( event -> cardBox( Direction.FRONT ) );
        box2.setOnAction( event -> cardBox( Direction.BACK ) );
        cram1.setOnAction( event-> selectLessonAndLearn( Direction.FRONT ) );
        cram2.setOnAction( event-> selectLessonAndLearn( Direction.BACK ) );
        edit.setOnAction( event -> lessonEdit() );
        lessonToBox.setOnAction( event -> new LessonToBoxDialog().showAndWait() );
        exit.setOnAction( e-> System.exit(0));

        fillViewMenu( viewMenu );
        fillSettingsMenu(settingsMenu);
        fillHelpMenu(helpMenu);

        updateLeds();
    }
    private void lessonEdit() {
        if (currentStage != null) {
            currentStage.toFront();
            return;
        }
        LessonEditDialog diag = new LessonEditDialog();
        currentStage = diag;
        diag.showAndWait();
        currentStage = null;
    }

    private void fillViewMenu( Menu viewMenu ) {
        Menu mitem = new Menu("Sprache wählen");
        viewMenu.setOnShown( e1 ->
        {
            mitem.getItems().clear();

            ObservableList<Language> languages = Globals.getSelectedLanguages();
            for (Language lang : languages) {
                MenuItem item;
                item = new MenuItem( lang.getLabel() );
                mitem.getItems().add( item );
                item.setOnAction( e-> changeLanguage( lang ));
            }
        });
        viewMenu.getItems().add(mitem);

        viewMenu.getItems().add(new SeparatorMenuItem());

        MenuItem boxItem = new MenuItem("Statistik Lernkartei" );
        viewMenu.getItems().add(boxItem);
        boxItem.setOnAction( e -> new BoxStatisticDialog().showAndWait());

        MenuItem searchItem = new MenuItem("Vokabel suchen");
        viewMenu.getItems().add(searchItem);
        searchItem.setOnAction(e -> new CardSearch().showAndWait() );
    }

    private void fillSettingsMenu( Menu settings )
    {
        Menu mitem = new Menu("Sprachen aktivieren");
        settings.getItems().add(mitem);

        ArrayList<Language> languages = Globals.getSupportedLanguages();
        for (Language lang : languages) {
            final CheckMenuItem item = new CheckMenuItem( lang.getLabel() );
            mitem.getItems().add( item );
            if ( Globals.getSelectedLanguages().contains( lang ) )
                item.setSelected( true );

            item.setOnAction( e ->
            {
                Globals.enableLanguage( lang, item.isSelected() );
                item.setSelected(Globals.getSelectedLanguages().contains(lang));
            });
        }

        MenuItem item;
        item = new MenuItem("Sprache konfigurieren");
        item.setOnAction( e2 -> Globals.getLanguage().configure( null ) );
        settings.getItems().add(item);
        settings.getItems().add( new SeparatorMenuItem() );

        item = new MenuItem("Name");
        item.setOnAction( e-> {
            TextInputDialog dialog = new TextInputDialog( Globals.getUserName() );
            dialog.setTitle("Name");
            dialog.setHeaderText("Bitte Namen eingeben:");
            dialog.setContentText("Name:");
            Optional<String> result = dialog.showAndWait();

            result.ifPresent( name -> {
                Globals.setUserName(name);
                Stage stage = (Stage) menuBar.getScene().getWindow();
                stage.setTitle("JCardBox: "+name);
            } );

        });
        settings.getItems().add(item);

        item = new MenuItem("Klasse");
        item.setOnAction( e-> {
            TextInputDialog dialog = new TextInputDialog( Globals.getDefaultCategory() );
            dialog.setTitle("Klasse wählen");
            dialog.setHeaderText("Bitte Klasse eingeben:");
            dialog.setContentText("Klasse:");
            Optional<String> result = dialog.showAndWait();
            result.ifPresent( name -> Globals.setDefaultCategory(name) );
        });
        settings.getItems().add(item);

        item = new MenuItem("Schrift");
        //todo swing item.setOnAction( e-> selectFont() );
        settings.getItems().add(item);

        item = new MenuItem("Karteifächer");
        item.setOnAction( event ->  new BoxSettings().showAndWait() );
        settings.getItems().add(item);

        item = new MenuItem("Abfragemethode");
        item.setOnAction( event ->  new CheckAnswerSettings().showAndWait() );
        settings.getItems().add(item);

        settings.getItems().add( new SeparatorMenuItem() );

        item = new MenuItem("Sicherungdatei erstellen (inkrementell)");
        item.setOnAction( e->saveDump(true) );
        settings.getItems().add(item);

        Menu dumpMenu = new Menu("Sicherung");
        settings.getItems().add(dumpMenu);
        MenuItem readDump = new MenuItem("Sicherungsdatei lesen");
        readDump.setOnAction( e -> Dump.loadDumpFile(null));
        //JMenuItem readLastDump = new JMenuItem("Letzte Sicherungsdatei lesen");
        //readLastDump.setOnAction( e1 -> Dump.loadLastDump(JCardBox.this) );
        MenuItem writeDump = new MenuItem("Neue Sicherungsdatei erstellen");
        writeDump.setOnAction( event -> saveDump(false) );
        MenuItem writeIncDump = new MenuItem("Inkrementelle Sicherungsdatei erstellen");
        writeIncDump.setOnAction( event -> saveDump(true) );
        dumpMenu.getItems().add(readDump);
        dumpMenu.getItems().add(new SeparatorMenuItem());
        dumpMenu.getItems().add(writeDump);
        dumpMenu.getItems().add(writeIncDump);

        settings.getItems().add( new SeparatorMenuItem() );

        item = new MenuItem("Datenbankeinstellungen");
        item.setOnAction(e -> showDatabaseSettings() );
        settings.getItems().add(item);
    }

    private void showDatabaseSettings() {
        DatabaseSettingsDialog d = new DatabaseSettingsDialog();
        d.showAndWait();
        if (!d.isCancelled())
            try {
                Database.connectToDatabase();
            } catch (Exception e) {
                e.printStackTrace();
                DefaultDialogs.showExceptionDialog("Fehler", "Keine Verbindung zur Datenbank", e);
             }
    }

    private void fillHelpMenu( Menu helpMenmu ) {
        MenuItem item = new MenuItem("Über JCardBox");
        helpMenmu.getItems().add(item);
        item.setOnAction( e -> about() );
    }


    private void changeLanguage( Language lang ) {
        if ( lang != Globals.getLanguage() )
        {
            Globals.setLanguage(lang);

            for (Button[] arr : buttons) {
                arr[0].setText(Globals.getLanguageLabel1() + "->" + Globals.getLanguageLabel2());
                arr[1].setText(Globals.getLanguageLabel2() + "->" + Globals.getLanguageLabel1());
            }
        }
    }


    /**
     * learn cards in card box
     */
    private void cardBox( Direction direction ) {
        if (currentStage != null) {
            currentStage.toFront();
            return;
        }

        Globals.setDirection( direction );
        try {
            ArrayList<Card> list = Database.getDatabase().getBoxList( direction );
            if (list.isEmpty())
            {
                int[] val = getNumberOfBoxCards( direction );
                int cards = val[0];
                int days = val[1];
                String message;
                if ( cards > 0 ) {
                    message = (days > 1) ? "In " + days + " Tagen: " : "Morgen: ";
                    message += cards + (cards > 1 ? " Vokabeln " : " Vokabel");
                } else {
                    message = "Keine Vokabeln vorhanden";
                }

                showInfoDialog("Info", "Keine Karten heute", message );
                return;
            }

            BoxLesson lesson = new BoxLesson( list );
            currentStage = lesson;
            lesson.showAndWait();

            Result res=lesson.getResult();
            if ( res.getAnwerCount() > 0 ) {
                boolean learn = res.showResultDialogAskLearn( "Lernkartei" );
                if (learn) {
                    Lesson lesson2 = Lesson.boxLesson;
                    lesson2.load(true );
                    CramMethod method = new CramMethods().get(0);
                    learn(lesson2, method, direction);
                }
                updateLeds();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        Globals.setDirection(Direction.FRONT);
        currentStage = null;
    }
    private int[] getNumberOfBoxCards( Direction direction ) {
        int searchDays = 7;
        BoxStatistics stat;
        try {
            stat = Database.getDatabase().getBoxStatistics( direction, searchDays );
        } catch (SQLException e) {
            return new int[]{0,0};
        }

        int boxCount = Globals.getBoxDates( direction ).length;
        int dayFound = 0;
        int sum=0;
        for (int i=0; i<searchDays; i++) {
            sum=0;
            for (int boxNr = 0; boxNr < boxCount; boxNr++) {
                sum+=stat.cardsForDay[boxNr][i];
            }
            dayFound = i;
            if (sum > 0) break;
        }
        return new int[]{sum, dayFound};
    }


    public void selectLessonAndLearn( Direction direction ) {
        if (currentStage != null) {
            currentStage.toFront();
            return;
        }

        SQLDatabase db = Database.getDatabase();
        if (db != null) {
            final LessonSelectionDialog sel = new LessonSelectionDialog();
            currentStage = sel;
            sel.showAndWait();
            Lesson lesson = sel.getSelectedLesson();
            CramMethod method = sel.getCramMethod();

            if (lesson != null)
                learn(lesson, method, direction);
        }
        currentStage = null;
    }

    /**
     * learn the given lesson
     * @param lesson
     */
    private void learn(Lesson lesson, CramMethod method, Direction direction)
    {
        Globals.setDirection( direction );
        if ( lesson != null )
        {
            lesson.load( true ); // direction is required for box errors

            if (!lesson.getCardCopy().isEmpty()) {
                CramLesson cram = new CramLesson(lesson, method);
                currentStage = cram;
                cram.showAndWait();
                Result res = cram.getResult();
                if (res.getAnwerCount() > 0) {
                    res.showResultDialog(lesson.getName());
                }
            }
        }
        Globals.setDirection( Direction.FRONT );

        if (lesson == Lesson.boxLesson)
            updateLeds();
    }


    private void about() {
        String revision = Version.getVersion();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(getClass().getResource("JCardBox.css").toExternalForm());
        dialogPane.getStyleClass().add("jcardbox");

        alert.setTitle("Über JCardBox");
        alert.setHeaderText("JCardBox Version "+ Globals.version +" " + revision +"\n" );
        alert.setContentText("Vokabeltrainer\n"
                +  "(c) Anke Visser, A.Visser@gmx.de\n"
                +  "License:\nGNU General Public License Version 3\n"
                +  "https://jcardbox.sourceforge.io/");
        alert.showAndWait();
    }



    private void updateLeds() {
        ObservableList<Language> list = Globals.getSelectedLanguages();
        ledLine.getChildren().clear();

        int rows = Globals.getSelectedLanguages().size();
        int cols=3;
        Label[] leds = new Label[ cols*rows ];
        int maxSize = 0;
        for (int i=0; i<rows; i++) {
            Label label = new Label( list.get(i).getLabel() );
            ledLine.getChildren().add( label );
            HBox hbox = new HBox();
            hbox.setAlignment( CENTER );
            ledLine.getChildren().add(hbox);
            for (int j=0; j<cols; j++) {
                Image icon = new Image(getClass().getResourceAsStream("/res/led_green.png"));
                ImageView image = new ImageView();
                image.setImage(icon);
                leds[j+i*cols] = new Label();
                hbox.getChildren().add(leds[j+i*cols]);

                final Tooltip tooltip = new Tooltip("");
                leds[j+i*cols].setTooltip(tooltip);
            }
        }

        Image green = new Image(getClass().getResourceAsStream("/res/led_green.png"));
        Image yellow = new Image(getClass().getResourceAsStream("/res/led_yellow.png"));
        Image red = new Image(getClass().getResourceAsStream("/res/led_red.png"));

        Language lang1 = Globals.getLanguage();
        int idx = 0;
        for (Language lang : list)
        {
            int firstCount = 0;
            Globals.setLanguage(lang);

            Direction dirs[] = {Direction.FRONT, Direction.BACK};
            for (Direction dir : dirs) {
                int count = Database.getDatabase().getBoxCount(dir);
                boolean started = Database.getDatabase().hasBoxLessonStarted();
                if (dir == Direction.FRONT) firstCount = count;
                // show green if less than 5 words to learn today, but not ifusl a lazy childs stops then
                if ( count == 0 || (!started && count < 5) )
                    leds[idx].setGraphic( new ImageView(green) );
                else if (count < 15)
                    leds[idx].setGraphic( new ImageView(yellow) );
                else
                    leds[idx].setGraphic( new ImageView(red) );
                String str = (dir == Direction.FRONT) ?
                        Globals.getLanguageLabel1() +"->"+ Globals.getLanguageLabel2() :
                        Globals.getLanguageLabel2() +"->"+ Globals.getLanguageLabel1();
                leds[idx].getTooltip().setText(str+": "+count);
                idx++;
            }
            Globals.setDirection(Direction.FRONT);

            { // check if errors from card box have been worked on
                if ( firstCount >= 5 )
                {
                    leds[idx].setGraphic( new ImageView(red) );
                    leds[idx].getTooltip().setText( "Noch "+firstCount+" Vokabeln im Kasten" );
                }
                else {
                    Lesson.boxLesson.load( false );
                    int errorCount = Lesson.boxLesson.getCards().size();
                    if (errorCount < 5) {
                        leds[idx].setGraphic( new ImageView(green) );
                        leds[idx].getTooltip().setText("Nur " + firstCount + " Fehler im Kasten");
                    } else {
                        boolean found = false;
                        ArrayList<LogEntry> logs = Database.getDatabase().readLog(1);
                        for (LogEntry entry : logs) {
                            if (entry.isBoxCramLesson()) {
                                System.out.println(entry.getMessage());
                                int res[] = entry.getBoxCramLessonErrors();
                                System.out.println("res "+res[0]+"  "+res[1]);
                                String message = "";
                                if ( res[0] > 0 ) {
                                    message = "Vokabeln teilweise gelernt aber noch " + res[0] + " nicht gelernt. ";
                                }
                                message += "Noch "+res[1]+" Vokabeln fehlerhaft";
                                if ( res[0] + res[1] > 10 )
                                    leds[idx].setGraphic( new ImageView(red) );
                                else if (res[0] + res[1] > 5)
                                    leds[idx].setGraphic( new ImageView(yellow) );
                                else
                                    leds[idx].setGraphic( new ImageView(green) );

                                leds[idx].getTooltip().setText(message);
                                found = true;
                            }
                        }
                        if (!found) {
                            leds[idx].setGraphic( new ImageView(red) );
                            leds[idx].getTooltip().setText(lang.getLabel() + ": noch " + errorCount + " Fehler im Kasten zu lernen");
                        }
                    }
                }
                idx++;
            }
        }
        if ( lang1 != Globals.getLanguage() )
            Globals.setLanguage(lang1);
        Globals.setDirection(Direction.FRONT);
    }

    public void saveDump( boolean incremental )
    {
        try {
            if (incremental)
                Database.getDatabase().saveIncrementalDump();
            else
                Database.getDatabase().saveDump();

            showInfoDialog("Datenbank - Sicherung", "Datenbanksicherung\n" , "Datenbank gesichert in:\\n\"+Globals.getDumpDir()");
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            DialogPane dialogPane = alert.getDialogPane();
            dialogPane.getStylesheets().add(getClass().getResource("JCardBox.css").toExternalForm());
            dialogPane.getStyleClass().add("jcardbox");
            alert.setTitle("Datenbank - Sicherung");
            alert.setHeaderText("Datenbanksicherung\n" );
            alert.setContentText("Sicherung konnte nicht erstellt werden.");
            alert.showAndWait();
        }
    }

    public void showInfoDialog( String title, String header, String content ) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(getClass().getResource("JCardBox.css").toExternalForm());
        dialogPane.getStyleClass().add("jcardbox");
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }

} // class JCardBox
