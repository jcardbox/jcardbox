package gui;
import data.*;

import javafx.collections.FXCollections;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.fxml.FXMLLoader;
import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class LessonSelectionDialog extends Stage implements Initializable
{
 	@FXML
	private Pane lessonSelection;
	@FXML
	private LessonSelection lessonSelectionController; // class+Controller
 	@FXML
	private Button ok;
	@FXML
	private Button cancel;
	@FXML
	private ComboBox<CramMethod> method;

    private Lesson lesson;

	public LessonSelectionDialog()
	{
		setTitle( "Auswahl der Lektion" );

		boolean multiple = false;

		// create new window and fill it
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("LessonSelectionDialog.fxml"));
		fxmlLoader.setController(this);
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
			setScene(new Scene(parent));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void initialize(URL url, ResourceBundle resourceBundle)
	{
		ok.setOnAction( e-> ok() );
		cancel.setOnAction( e-> {lesson = null; close();});
		method.setItems(FXCollections.observableArrayList(new CramMethods()));
		method.getSelectionModel().select(0);
	}

	public CramMethod getCramMethod() {
		return method.getSelectionModel().getSelectedItem();
	}

	public void ok()
	{
		lesson = lessonSelectionController.getSelectedLesson();
		close();
	}

	Lesson getSelectedLesson() {
        return lesson;
	}

	java.util.List<Lesson> getSelectedLessons() {
		if (lesson == null)
			return new java.util.ArrayList<Lesson>(); // cancelled
		else {
			return lessonSelectionController.getSelectedLessons();
		}
	}
}
