package gui;

import data.Database;
import data.Globals;
import data.SQLDatabase;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.stage.*;
import java.io.File;
import java.util.Optional;

public class Dump
{
    public static void loadDumpFile( Window parent ) {
        String dumpDir = Globals.getDumpDir();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Database dump", "*.zip", "*.ZIP") );
        File selectedFile = fileChooser.showOpenDialog( parent );
        if (selectedFile != null) {
            SQLDatabase.extractZip(selectedFile.getPath(), dumpDir);

            boolean error = false;
            for (String tablename : SQLDatabase.getBasicTables().keySet()) {
                File file = new File(dumpDir+File.separator+tablename+".csv");
                if (!file.exists()) {
                    error = true;
                    System.out.println("Dump file doesn't exist: "+tablename);
                    break;
                }
            }
            if (error) {
                showError( parent,"Fehler:","Unvollständige Sicherungsdatei" );
            } else {
                loadLastDump( parent );
            }
        }
    }

    public static void loadLastDump(Window parent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Löschen und Dump laden");
        alert.setHeaderText("Löschen und Dump laden");
        alert.setContentText("Alle vorhandenen Daten werden gelöscht\nSoll der Dump geladen werden?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            {
                try {
                    Database.getDatabase().loadDump();
                } catch (Exception e) {
                    showError(parent, "Fehler", "Fehler beim Öffnen der Datenbank");
                    e.printStackTrace();
                }
            }
        }
    }

    public static void saveDump(Window parent, boolean incremental) {
        try {
            if (incremental)
                Database.getDatabase().saveIncrementalDump();
            else
                Database.getDatabase().saveDump();
        } catch (Exception e) {
            showError( parent,"Fehler","Fehler beim Öffnen der Datenbank" );
            e.printStackTrace();
        }
    }

    private static void showError( Window parent, String title, String header ) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        DialogPane dialogPane = alert.getDialogPane();
        if (parent != null)
          dialogPane.getStylesheets().add( parent.getClass().getResource("JCardBox.css").toExternalForm());
        dialogPane.getStyleClass().add("jcardbox");

        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.showAndWait();
    }

}
