package gui;

import data.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by anke on 2/18/17.
 * Asks the user to insert the translation of a word into a text field and checks if the
 * correctlyAnswered is correct.
 * @see CardAnswerChoice
 */
public class CardAnswerText implements Initializable, CardAnswerInterface
{
    @FXML
    private Label answerLabel;
    @FXML
    private TextField answerField;
    @FXML
    private Button play;
    @FXML
    private Button ok;
    @FXML
    private Button cancel;

    private Card card;
    private AnswerListener listener;
    private boolean correctlyAnswered = false;
    private ExitStatus status ;

    @Override
    public void initialize(URL url, ResourceBundle ressourceBundle)
    {
        nextQuestion();
        answerField.setOnAction( event -> okSelected() );
        ok.setOnAction( event -> okSelected() );
        cancel.setOnAction( event -> notifyAnswered( false ) );
        play.setVisible(false);
        play.setOnAction( e-> AudioPlayer.playClip( card.getAudio()) );

        Globals.getLanguage().setInput( answerField );
    }
    public ExitStatus getStatus() {
        return status;
    }

    public boolean isCorrect() {
        return correctlyAnswered;
    }
    public void setQuestion( Card c )
    {
        nextQuestion();
        card = c;
    }
    public void setAnswerListener(AnswerListener q)
    {
        listener = q;
    }

    private void okSelected()
    {
        if ( status != ExitStatus.ANSWERED ) {
            checkAnswer();
            if ( status == ExitStatus.ANSWERED )
                listener.answerFinished();
        }
        else // answered and 2*ok pressed
        {
            notifyAnswered( true );
        }
     }

    /** notify listener, that the question has been answered and confirmed or aborted */
    private void notifyAnswered( boolean okSelected )
    {
        if ( status == ExitStatus.ANSWERED )
        {
            if ( okSelected )
                status = ExitStatus.ANSWERED_AND_CONFIRMED;
            else // cancelled after question has been answered
                status = ExitStatus.ANSWERED_AND_ABORTED;
        }
        else {
            status = ExitStatus.ABORTED;
        }
        listener.answerFinished(); // notify
    }

    public String getAnswer() {
        return answerField.getText().trim();
    }

    private void checkAnswer()
    {
        boolean answerIsSynonym = false; // correctlyAnswered is a synonym of the requested correctlyAnswered
        String text = answerField.getText().trim();
        correctlyAnswered = false;

        if (!text.isEmpty()) // ignore empty answers
        {
            if ( Globals.getLanguage().answerIsCorrect( text, card.getAnswer() ) )
            {
                correctlyAnswered = true;
            }
            else  { // check if correctlyAnswered is a synonym of the requested one
                for(String s : card.getSynonyms()) {
                    if ( Globals.getLanguage().answerIsCorrect( text, s ) )
                    {
                        answerIsSynonym = true;
                    }
                }
            }

            if (answerIsSynonym) { // request a synonym => not finished yet
                String diff="";
                String correct = card.getAnswer();
                int min = Math.min( correct.length(), text.length());
                for (int i=0; i<min; i++) {
                    char c = correct.charAt(i);
                    diff += c;
                    if ( c != text.charAt(i) ) break;
                }
                answerField.setText(diff);
                answerField.positionCaret(diff.length());
                answerLabel.setText("Gesucht wird ein Synonym");
            }
            else { // not a synonym
                status = ExitStatus.ANSWERED;

                //if ( ( Globals.getDirection() != Direction.FRONT ) && (card.getImage() != null) )
                // todo imageLabel.setIcon(new ImageIcon(card.getImage()));

                answerField.setEditable(false);
                if ( correctlyAnswered ) {
                    answerLabel.setTextFill(Color.web("#000000"));
                    answerLabel.setText("richtig!");
                }
                else {
                    answerLabel.setText(card.getAnswer());
                    answerLabel.setTextFill(Color.web("#ff0000"));
                }
                //Language.Gender gender = Globals.getLanguage().getGender(card.getAnswer());
                //todo Color background = Globals.getLanguage().getColor( gender );
                //if ( background != null) setBackgroundColor( background );

                if (card.getAudio() != null)
                {
                    AudioPlayer.playClip(card.getAudio());
                    play.setVisible( true );
                }
                else {
                    play.setVisible(false);
                }

            } // not a synonym
        } // not empty correctlyAnswered
    }

    private void nextQuestion() {
        status = ExitStatus.ABORTED; // initial status
        answerField.setText("");
        answerField.setEditable(true);
        answerLabel.setText("");
        answerLabel.setTextFill(Color.web("#000000"));
         play.setVisible(false);
       // if (backgroundChanged) {
        //     setBackgroundColor(defaultColor);
        // }
    }

}
