package gui;

import data.*;

import gui.dialogs.DefaultDialogs;
import gui.dialogs.ImportDialog;
import javafx.beans.property.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * creates a panel which allows the user to select a category and shows all available lessons
 */
public class LessonOverview implements Initializable
{
    // lesson selection tag
    @FXML
    private Pane lessonSelection;
    @FXML
    private LessonSelection lessonSelectionController; // class+Controller
    @FXML
    private TableView lessonView;
    @FXML
    private LessonView lessonViewController; // class+Controller
    @FXML
    private Pane newLesson;
    @FXML
    private NewLesson newLessonController; // class+Controller
    @FXML
    private Button back;
    @FXML
    MenuItem importCSV;
    @FXML
    MenuItem exportCSV;
    @FXML
    MenuItem print;
    @FXML
    MenuItem rename;
    @FXML
    MenuItem delete;

    private ReadOnlyObjectProperty<Lesson> selected;
    private Lesson newLessonEntry;

    public ReadOnlyObjectProperty<Lesson> getSelectedLessonProperty() {
        return selected;
    }
    public Button getCloseButton() {return back;}

    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        newLessonController.getLessonNameProperty().addListener( e-> newLessonCreated() );

        selected = lessonSelectionController.getSelectedLessonProperty();
        selected.addListener( (observable, oldLesson, newLesson) -> selectLesson( newLesson ) );

        newLessonEntry = new Lesson("Neu", lessonSelectionController.getCategory() );
        lessonSelectionController.addLesson( 0, newLessonEntry);

        importCSV.setOnAction( e-> importLesson() );
        exportCSV.setOnAction( e-> exportLesson() );
        rename.setOnAction( e-> renameLesson() );
        delete.setOnAction( e-> deleteLesson() );
        print.setOnAction( e->printLesson() );
    }

    private void newLessonCreated() {
        String lessonName = newLessonController.getLessonNameProperty().get();
        Lesson lesson = new Lesson( lessonName, lessonSelectionController.getCategory() );
        lessonSelectionController.addLesson( 1, lesson);
    }

    private void selectLesson( Lesson lesson ) {
        if ( lesson == newLessonEntry ) {
            lessonView.setVisible( false );
            newLesson.setVisible( true );
            newLessonController.setCategory( lessonSelectionController.getCategory() );
        }
        else {
            lessonView.setVisible( true );
            newLesson.setVisible( false );
            lessonViewController.setLesson(lesson);
        }
    }

    private void reload() {
        lessonSelectionController.reload();
        newLessonEntry = new Lesson("Neu", lessonSelectionController.getCategory() );
        lessonSelectionController.addLesson( 0, newLessonEntry);
    }

    private void exportLesson()
    {
        for (Lesson lesson: lessonSelectionController.getSelectedLessons() ) // todo multiple
        {
            exportLesson(lesson);
        }
    };

    /**
     * import lesson from CSV
     */
    private void exportLesson( Lesson lesson ) {
        if (lesson == null) return;
        File dumpDir = new File(Globals.getInstallationRoot() + File.separator + Globals.getLanguageIdentifier(), "exports");
        if (!dumpDir.exists()) dumpDir.mkdir();

        Window parent = lessonSelection.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Lektion exportieren");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Lektion", "*.lesson") );
        fileChooser.setInitialDirectory( dumpDir );
        fileChooser.setInitialFileName( lesson.getName().replace(" ","_") );
        File selectedFile = fileChooser.showSaveDialog( parent );
        if (selectedFile != null) {
            H2Database.saveLessonToCSV(lesson, selectedFile.getAbsolutePath() );
        }
    }

    private void importLesson() {
        File dumpDir = new File(Globals.getInstallationRoot() + File.separator + Globals.getLanguageIdentifier(), "exports");
        if (!dumpDir.exists()) dumpDir.mkdir();

        Window parent = lessonSelection.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Lektion importieren");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Lektion", "*.lesson", "*.csv") );
        fileChooser.setInitialDirectory( dumpDir );
        File selectedFile = fileChooser.showOpenDialog( parent );
        if (selectedFile != null) {
            if (selected != null) {
                ArrayList<Card> cards = H2Database.importLessonFromCSV(selectedFile.getAbsolutePath());
                String name = selectedFile.getName();
                name = name.replace(".lesson",""); name = name.replace(".csv","");
                new ImportDialog( cards, name ).showAndWait();
            }
            reload();
        }
    }

    private void renameLesson() {
        Lesson lesson = lessonSelectionController.getSelectedLesson();

        TextInputDialog dialog = new TextInputDialog( lesson.getName() );
        dialog.setTitle("Lektion umbenennen");
        dialog.setHeaderText("Lektion "+lesson.getName()+" umbenennen");
        dialog.setContentText("Neuer Name:");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            Lesson newLesson = new Lesson( result.get(), Globals.getDefaultCategory() );
            lesson.setName( newLesson.getName() );
            lesson.setCategory( newLesson.getCategory() );
            try {
                Database.getDatabase().renameLesson(lesson, newLesson);
                reload();
            } catch (Exception e) {
                DefaultDialogs.showExceptionDialog( "Fehler", "Lektion konnte nicht umbenannt werden", e);
            }
         }
    }

    private void deleteLesson() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add( this.getClass().getResource("JCardBox.css").toExternalForm());
        dialogPane.getStyleClass().add("jcardbox");

        alert.setTitle("Löschen bestätigen");
        alert.setHeaderText("Lektionen löschen?");
        alert.setContentText("Alle ausgewählten Lektionen löschen? \nDie enthaltenen Vokabeln bleiben erhalten.");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK)
        {
            for (Lesson lesson : lessonSelectionController.getSelectedLessons()) {
                Database.getDatabase().deleteLesson(lesson);
            }
            reload();
        }
    }

    private void printLesson() { //todo
        java.util.List<Lesson> lessons = lessonSelectionController.getSelectedLessons();
        if (lessons != null) {
            for (Lesson lesson : lessons) {
                lesson.load( false );
            }
            new CardPrinter(lessons, lessonSelection.getScene().getWindow() );
        }
    }

}