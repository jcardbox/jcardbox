; Script generated by the Inno Script Studio Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "JCardBox"
#define MyAppVersion "1.0"
#define MyAppPublisher "Anke Visser"
#define MyAppExeName "JCardBoxLaunch.exe"
#define MyAppDir "english"
[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{7E7D9B2B-721E-42E8-954B-8EA21307705F}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
Compression=lzma
SolidCompression=yes
OutputBaseFilename=JCardBox

[Languages]
Name: "german"; MessagesFile: "compiler:Languages\German.isl"
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "JCardBoxLaunch.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "JCardBox.jar"; DestDir: "{app}"; Flags: ignoreversion
Source: "flag-uk.ico"; DestDir: "{app}"; Flags: ignoreversion

; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\{#MyAppName}";         Filename: "{app}/{#MyAppExeName}"; WorkingDir: "{app}"; IconFilename: "{app}/flag-uk.ico" ;
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}/{#MyAppExeName}"; WorkingDir: "{app}"; IconFilename: "{app}/flag-uk.ico" ; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
