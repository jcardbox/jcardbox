## JCardBox - Vokabeltrainer nach dem Karteikartenprinzip

[Download](https://jcardbox.gitlab.io/download) und Benutzerdokumentation unter [https://jcardbox.gitlab.io/](https://jcardbox.gitlab.io/)

## Source
Mit [gradle](https://gradle.org/) kann der Source folgendermaßen übersetzt werden. Alternativ liegen 
Dateien für die IDE IntellJ Idea bei.

    git clone git@gitlab.com:jcardbox/jcardbox.git
    cd jcardbox/install/gradle
    gradle build