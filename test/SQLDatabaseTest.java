package data;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

/**
 * Created by anke on 8/16/15.
 */
public class SQLDatabaseTest {
    private String dirname="/tmp/a";

    //@Rule
    //public TemporaryFolder folder = new TemporaryFolder();
    
    private Card createCard(int start) {
        return createCard(start,"");
    }

    private Card createCard(int start, String postfix) {
        Card card = new Card();
        byte[] sound = new byte[1];
        sound[0] = (byte) (start+10);
        card.setFront("front" + start + " "+postfix);
        card.setBack("" + start + " " + postfix);
        card.setAudio(sound);
        return card;
    }

    private Lesson createLesson(int start) {
        Lesson lesson = new Lesson("name"+start,"category"+start);
        ArrayList<Card> cards = new ArrayList<Card>();
        Card card = createCard(start);
        cards.add(card);
        lesson.setCards(cards);
        return lesson;
    }

    private String fileToString(String filename) throws Exception {
        StringBuffer str = new StringBuffer();

        FileInputStream fi = new FileInputStream(filename);
        int c;
        while ((c = fi.read()) != -1) {
            str.append((char) c);
        }

        return str.toString();
    }

    void testCreateTables(SQLDatabase db) throws Exception {
        String value1 = "value";
        db.setPrefs("key", value1);
        String value2 = db.getPrefs("key");
        db.closeConnection();
        assertEquals(value1, value2, "Database created and values saved and loaded from preferences");
    }

    void addCard(SQLDatabase db) throws Exception {
        Card card = createCard(1);
        db.addCard(card);
        boolean exists = db.cardExists(card.getFront(), card.getBack());
        assertTrue(exists, "Add and retreive card");
        db.closeConnection();
    }

    void addSpecialCharacterCard(SQLDatabase db) throws Exception {
        Card card = createCard(1, "i'm ; g");
        db.addCard(card);
        boolean exists = db.cardExists(card.getFront(), card.getBack());
        assertTrue(exists, "Add and retreive card");

        int id=db.getID(card);
        assertTrue(id >= 0);

        db.closeConnection();
    }

    void testAddLesson(SQLDatabase db) throws Exception {
        Lesson lesson = createLesson(1);
        Card card = lesson.getCards().get(0);

        db.addLesson(lesson);

        boolean exists = db.cardExists(card.getFront(), card.getBack());
        assertTrue(exists, "Existing card is found");

        exists = db.cardExists("null","null");
        assertFalse(exists, "Non existing card is not found");

        db.closeConnection();
    }

    void testAddCardToLesson(SQLDatabase db) throws Exception {
        Lesson lesson = createLesson(1);
        db.addLesson(lesson);
        Card card = createCard(2);
        db.addCard(card);

        db.addCardToLesson(card.getDatabaseID(), lesson.getID());

        Lesson changedLesson = new Lesson(lesson.getName(), lesson.getCategory());
        db.loadLessonInfo(changedLesson);
        int newCardCount = db.loadLesson(changedLesson.getID()).size();

        assertTrue( newCardCount == 2, "Add card to lesson" ) ;

        db.closeConnection();
    }

    void testCreateDump( SQLDatabase db ) throws Exception {
        File dir = new File(dirname);
        File fa = new File(dir,"a");
        File fb = new File(dir,"b");
        fa.mkdir();
        fb.mkdir();

        db.setDumpDirectory(fa.getAbsolutePath());
        Lesson lesson = createLesson(1);
        db.addLesson(lesson);
        Card card = createCard(2);
        db.addCard(card);
        db.addCardToLesson(card.getDatabaseID(), lesson.getID());
        db.saveFullDump();

        // load dump and save to new directory
        db.deleteTables();
        db.loadFullDump();

        db.setDumpDirectory(fb.getAbsolutePath());
        db.saveFullDump();
        db.closeConnection();

        //p=Runtime.getRuntime().exec("cp "+fb.getAbsolutePath()+File.separator+"cards.csv /tmp/cards2.csv"); p.waitFor();

        String dat1 = fileToString(fa.getAbsolutePath()+File.separator+"cards.csv");
        String dat2 = fileToString(fb.getAbsolutePath()+File.separator+"cards.csv");

        assertEquals(dat1, dat2,"dump created and loaded");
    }

    void fillDB(SQLDatabase db) {
        Lesson lesson = createLesson(1);
        db.addLesson(lesson);
        Card card = createCard(2);
        db.addCard(card);
        db.addCardToLesson(card.getDatabaseID(), lesson.getID());
    }

    void exchangeDumps( SQLDatabase db1, SQLDatabase db2 ) throws Exception {
        File dir = new File(dirname);
        File fa = new File(dir,"a"); fa.mkdir();
        File fb = new File(dir,"b"); fb.mkdir();

        // insert cards and save full dump to fa
        fillDB( db1 );
        db1.setDumpDirectory(fa.getAbsolutePath());
        db1.saveFullDump();

        ArrayList<Card> cards = db1.loadLesson( 1 );
        for (Card card: cards) {System.err.println("ca "+card.getAudio()[0]);}

        // load dump from db1 and save to db2/fb
        db2.setDumpDirectory(fa.getAbsolutePath());
        db2.loadFullDump();
        db2.setDumpDirectory(fb.getAbsolutePath());
        db2.saveFullDump();

      cards = db2.loadLesson( 1 );
        for (Card card: cards) {System.err.println("ca "+card.getAudio()[0]);}

        try {
            String dat1 = fileToString(fa.getAbsolutePath() + File.separator + "audio_data.csv");
            String dat2 = fileToString(fb.getAbsolutePath() + File.separator + "audio_data.csv");

            assertEquals("exchange dumps", dat1, dat2); // fa on left of diff browser

            dat1 = fileToString(fa.getAbsolutePath() + File.separator + "image_data.csv");
            dat2 = fileToString(fb.getAbsolutePath() + File.separator + "image_data.csv");

            assertEquals("exchange dumps", dat1, dat2); // fa on left of diff browser
        } catch (Exception e) {
            fail("unexpected exception "+e.toString());
        }
    }

    void testCreateIncDump(SQLDatabase remoteDB) throws Exception {
        File dir = new File(dirname);
        File fa = new File(dir,"a");
        File fb = new File(dir,"b");
        fa.mkdir();
        fb.mkdir();

        Card middleCard;

        // create database with 3 cards and save dump
        SQLDatabase tmpDB = SQLDatabase.connect("jdbc:h2:mem:", "", "");
        tmpDB.addCard(createCard(1));
        tmpDB.addCard(middleCard = createCard(2));
        tmpDB.addCard(createCard(3));
        tmpDB.saveFullDump();
        tmpDB.closeConnection();

        // create copy of first db by loading dump of temp db
        // change 2nd card and add new card
        remoteDB.setDumpDirectory(fa.getAbsolutePath());
        remoteDB.loadFullDump();
        remoteDB.addCard(createCard(4));
        byte[] audio = middleCard.getAudio();
        audio[0] *= 2;
        remoteDB.changeCard(middleCard, middleCard.getFront() + "!", middleCard.getBack() + "!",
                middleCard.getImage(), audio);

        // --------- save dumps ---------------

        // save modified db in dump in fb
        remoteDB.setDumpDirectory(fb.getAbsolutePath());
        remoteDB.saveFullDump();

        remoteDB.setDumpDirectory(fa.getAbsolutePath());
        // update first dump (in fa) with incremental dump of 2nd db
        int modified1 = remoteDB.saveIncrementalDump();

        try {
            String dat1 = fileToString(fa.getAbsolutePath() + File.separator + "audio_data.csv");
            String dat2 = fileToString(fb.getAbsolutePath() + File.separator + "audio_data.csv");

            assertEquals(dat1, dat2, "incremental dump created: audioData.csv"); // fa (incremental dump) on left of diff browser
            assertEquals(modified1, 2, "number of modified or added carddata entries");

            // next dump should not change entries
            int modified2 = remoteDB.saveIncrementalDump();
            assertEquals( modified2, 0, "number of modified or added carddata entries");
            //Process p=Runtime.getRuntime().exec("cp -r "+fa.getAbsolutePath() + " /tmp/aa"); p.waitFor();
            //p=Runtime.getRuntime().exec("cp -r "+fb.getAbsolutePath()+  " /tmp/aa"); p.waitFor();

            assertEquals( dat1, dat2, "incremental dump created: cards.csv "); // fa (incremental dump) on left of diff browser
        } catch (Exception e) {
            fail("unexpected exception "+e.toString());
        }
    }

    private SQLDatabase getH2() throws Exception {
        return SQLDatabase.connect("jdbc:h2:mem:","","");
    }
    private SQLDatabase getMySQL() throws Exception {
        SQLDatabase db = SQLDatabase.connect("jdbc:mysql://localhost:3306/jannis", "root", "");
        db.deleteTableContents();
        return db;
    }

    @Test
    public void testCreateTables_H2() throws Exception {
        testCreateTables(getH2());
    }
    /*

    @Test
    public void testAddCard_H2() throws Exception {
        addCard(getH2());
    }
    @Test
    public void testAddCardSpecialCharacter_H2() throws Exception {
        addSpecialCharacterCard(getH2());
    }
    @Test
    public void testAddLesson_H2() throws Exception {
        testAddLesson(getH2());
    }
    @Test
    public void testAddCardToLesson_H2() throws Exception {
        testAddCardToLesson(getH2());
    }
    @Test
    public void testCreateDump_H2() throws Exception {
        testCreateDump(getH2());
    }
    @Test
    public void testCreateIncDump_H2() throws Exception {
        testCreateIncDump(getH2());
    }
    @Test
    public void testSpecialChars() throws Exception {
        SQLDatabase db = getH2();
        Card card = createCard(1,"a'aäöü€");
        db.addCard(card);
        db.mergeCard(card);
    }
    //todo @Test
    public void testExportImport() throws Exception {
        Lesson lesson=createLesson(1);
        Globals.setInstallationRoot("/tmp");
        H2Database.saveLessonToCSV(lesson, "/tmp/english/dump/a");
        ArrayList<Card> cards = H2Database.importLessonFromCSV("/tmp/english/dump/a.lesson");
        assertEquals( cards.size(), lesson.getCards().size(), "imported cards == exported cards");
        assertEquals( cards.get(0), lesson.getCards().get(0), "1st imported card == 1st exported card" );
    }
    */

/*
    @Test
    public void testCreateTables_MySQL() throws Exception {
        testCreateTables(getMySQL());
    }         
    @Test
    public void testAddCard_MySQL() throws Exception {
        addCard(getMySQL());
    }
    @Test
    public void testAddCardSpecialCharacter_MySQL() throws Exception {
        addSpecialCharacterCard(getMySQL());
    }
    @Test
    public void testAddLesson_MySQL() throws Exception {
        testAddLesson(getMySQL());
    }
    @Test
    public void testAddCardToLesson_MySQL() throws Exception {
        testAddCardToLesson(getMySQL());
    }
    @Test
    public void testCreateDump_MySQL() throws Exception {
        testCreateDump(getMySQL());
    }
    @Test
    public void exchangeDumps1() throws Exception {
        exchangeDumps(getMySQL(), getH2());
     }
    @Test
    public void exchangeDumps2() throws Exception {
        exchangeDumps(getH2(), getMySQL());
    }
    @Test
    public void testCreateIncDump_MySQL() throws Exception {
        testCreateIncDump(getMySQL());
    }
    
    */

    @Test
    void createSampleDB() throws Exception {
        try {
            SQLDatabase db = SQLDatabase.connect("jdbc:h2:mem:", "", "");
            File dumpDir = new File(dirname + "/test");
            dumpDir.mkdir();
            db.setDumpDirectory(dumpDir.getAbsolutePath());
            Lesson lesson = sampleLesson();
            db.addLesson(lesson);
            db.lessonToBox(lesson, Direction.FRONT);
            //Exception exception = assertThrows(Exception.class, () -> db.testChangeCardDates() );
            db.testChangeCardDates();
            db.saveFullDump();

            db = SQLDatabase.connect("jdbc:h2:/home/anke/JCardBox/database/CardBoxDB", "", "");
            String dump2="/home/anke/JCardBox/dump/";
            SQLDatabase.extractZip(dumpDir.getAbsolutePath()+"/jcardboxDump_2018_12_19.zip", dump2);
            db.setDumpDirectory(dump2);
            db.loadDump();
        } catch (Exception e) {
            fail("unexpected exception "+e.toString());
        }

        //db.saveIncrementalDump();
    }

    private Lesson sampleLesson() {
        Lesson lesson = new Lesson("lesson1","Klasse5");
        ArrayList<Card> cards = new ArrayList<Card>();
        for (int i=0; i<4; i++) {
            Card card = createCard(i);
            cards.add(card);
        }
        lesson.setCards(cards);
        return lesson;
    }


}